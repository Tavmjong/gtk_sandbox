
#ifndef CUSTOM_WIDGET_H
#define CUSTOM_WIDGET_H

#include <iostream>
#include <gtkmm.h>
#include <glibmm/extraclassinit.h>
#include <glibmm/ustring.h>
//#include <glibmm.h>

gboolean focus(GtkWidget* widget, GtkDirectionType direction);


using BaseObjectType = GtkDrawingArea;
using BaseClassType = GtkDrawingAreaClass;

void class_init_function(void* g_class, void* class_data)
{
  g_return_if_fail(GTK_IS_WIDGET_CLASS(g_class));

  const auto klass = static_cast<BaseClassType*>(g_class);
  const auto css_name = static_cast<Glib::ustring*>(class_data);
  std::cout << "class_init_function: " << css_name << std::endl;

  //  gtk_widget_class_set_css_name((GtkWidgetClass*)klass, css_name->c_str());

  const auto widget_klass = static_cast<GtkWidgetClass*>(g_class);
  widget_klass->focus = focus;
}

class MyExtraInit : public Glib::ExtraClassInit
{
 public:
  MyExtraInit(const Glib::ustring& css_name)
    : Glib::ExtraClassInit(class_init_function)
    {
      std::cout << "MyExtraInit::MyExtraInit" << std::endl;
    }
};

class CustomWidget :  public MyExtraInit, public Gtk::DrawingArea
{
 public:
 CustomWidget()
   : Glib::ObjectBase("CustomWidget")
    , MyExtraInit("custom-widget")
    , Gtk::DrawingArea()
  {
    std::cout << "CustomWidget::CustomWidget()" << std::endl;
    set_focusable(true);
    set_hexpand(true);
    set_vexpand(true);
    set_draw_func(sigc::mem_fun(*this, &CustomWidget::draw));
  }
  virtual ~CustomWidget() = default;

  void draw(Cairo::RefPtr<Cairo::Context> const &cr, int width, int height)
  {
    cr->set_source_rgb(1.0, 0.0, 0.0);

    cr->move_to(0.0, 0.0);
    cr->line_to(width, height);
    cr->stroke();

    cr->move_to(0.0, height);
    cr->line_to(width, 0.0);
    cr->stroke();

    switch (focus_state) {
    case UNFOCUSED:
      cr->set_source_rgb(0.5, 0.5, 0.5);
      break;
    case FOCUS1:
      cr->set_source_rgb(0.0, 1.0, 0.0);
      break;
    case FOCUS2:
      cr->set_source_rgb(0.0, 0.0, 1.0);
      break;
    }

    cr->move_to(1.0,       1.0);
    cr->line_to(width-1.0, 1.0);
    cr->line_to(width-1.0, height-1.0);
    cr->line_to(0.0,       height-1.0);
    cr->close_path();
    cr->stroke();
  }

  enum FocusState {
    UNFOCUSED,
    FOCUS1,
    FOCUS2
  };

  FocusState focus_state = UNFOCUSED;
};

gboolean focus(GtkWidget* widget, GtkDirectionType direction)
{
  std::cout << "\n" << gtk_widget_get_name(widget)
            << " has focus: " << std::boolalpha << (bool)gtk_widget_has_focus(widget)
            << " direction: " << (int)direction << std::endl;
  gtk_widget_queue_draw(widget);

  auto my_widget = Glib::wrap(widget);
  auto my_widget2 = dynamic_cast<CustomWidget*>(my_widget);
  if (!my_widget2) {
    std::cerr << " Failed to get C++ widget!" << std::endl;
    return false;
  }

  if (gtk_widget_has_focus(widget)) {
    // Already have focus so either switch focus or exit.
    if (direction == GTK_DIR_TAB_FORWARD ||
        direction == GTK_DIR_RIGHT       ||
        direction == GTK_DIR_DOWN) {
      if (my_widget2->focus_state == CustomWidget::FocusState::FOCUS1) {
        my_widget2->focus_state =  CustomWidget::FocusState::FOCUS2;
        return true;
      } else {
        my_widget2->focus_state =  CustomWidget::FocusState::UNFOCUSED;
        return false;
      }
    } else {
      if (my_widget2->focus_state == CustomWidget::FocusState::FOCUS2) {
        my_widget2->focus_state =  CustomWidget::FocusState::FOCUS1;
        return true;
      } else {
        my_widget2->focus_state =  CustomWidget::FocusState::UNFOCUSED;
        return false;
      }
    }
  } else {
    // Entered from somewhere else.
    if (direction == GTK_DIR_TAB_FORWARD ||
        direction == GTK_DIR_RIGHT       ||
        direction == GTK_DIR_DOWN) {
      my_widget2->focus_state = CustomWidget::FocusState::FOCUS1;
    } else {
      my_widget2->focus_state = CustomWidget::FocusState::FOCUS2;
    }
    gtk_widget_grab_focus(widget);
    return true;
  }
}

#endif

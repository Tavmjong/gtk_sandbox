
#include <iostream>
#include <ostream>
#include <string>

#include <gtkmm.h>

#include "custom-widget.h"

class Window: public Gtk::ApplicationWindow
{

public:
  Window()
  {
    set_title("Window");

    auto custom_widget1 = Gtk::make_managed<CustomWidget>();
    auto custom_widget2 = Gtk::make_managed<CustomWidget>();
    auto custom_widget3 = Gtk::make_managed<CustomWidget>();
    custom_widget1->set_name("Widget1");
    custom_widget2->set_name("Widget2");
    custom_widget3->set_name("Widget3");

    auto button1 = Gtk::make_managed<Gtk::Button>("Button1");
    auto button2 = Gtk::make_managed<Gtk::Button>("Button2");

    auto box = Gtk::make_managed<Gtk::Box>();
    box->append(*button1);
    box->append(*custom_widget1);
    box->append(*custom_widget2);
    box->append(*custom_widget3);
    box->append(*button2);

    set_child(*box);

    present();
  }
};

int main(int argc, char *argv[]) {

  auto application = Gtk::Application::create("org.tavmjong.template_mm4");
  return application->make_window_and_run<Window>(argc, argv);

  return 0;
}


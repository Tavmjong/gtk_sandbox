#ifndef INK_DIALOG_CONTAINER_H
#define INK_DIALOG_CONTAINER_H

/*
 * A widget that manages InkNotebook's and other widgets inside a
 * horizontal InkMultipane containing vertical InkMultipane's or other
 * widgets.
 *
 * This widget keeps a list of dialogs it contains for use in
 * callbacks. It manages dialog creation/deletion/moving.
 *
 * Copyright (C) 2018 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include <gtkmm.h>

#include <iostream>

class InkMultipaned;
class InkDialogNotebook;
class InkDialogBase;

class InkDialogContainer : public Gtk::Box
{
public:
  InkDialogContainer();
  virtual ~InkDialogContainer() {std::cout << "~InkDialogContainer" << std::endl;};

  InkMultipaned* get_columns();
  InkMultipaned* create_column();  // Probably should be private
  std::pair<InkDialogBase*, Gtk::Widget*> create_dialog_widgets(Glib::ustring value);
  void new_dialog(Glib::ustring value);

  bool has_dialog_of_type(InkDialogBase* dialog);
  void link_dialog(InkDialogBase* dialog);
  void unlink_dialog(InkDialogBase* dialog);

  void update_dialogs();

  void list_dialogs(); // Debug

private:

  InkMultipaned* columns;
  std::vector<Gtk::TargetEntry> target_entries; // What kind of object can be dropped.

  // Do to the way Gtk handles dragging between notebooks, one can
  // either allow multiple instances of the same dialog in a notebook
  // or restrict dialogs to docks tied to a particular document
  // window. (More explicitly, use one group name for all notebooks or
  // use a unique group name for each document window with related
  // floating docks.) For the moment we choose the former which
  // requires a multimap here as we use the dialog type name as a key.
  std::multimap<Glib::ustring, InkDialogBase*> dialogs;

  // Handlers
  void on_unmap();
  InkDialogNotebook* prepare_drop(const Glib::RefPtr<Gdk::DragContext> context);
  void prepend_drop(const Glib::RefPtr<Gdk::DragContext> context, InkMultipaned* column);
  void append_drop( const Glib::RefPtr<Gdk::DragContext> context, InkMultipaned* column);
  void column_empty(InkMultipaned* column);
};

#endif // INK_DIALOG_CONTAINER_H

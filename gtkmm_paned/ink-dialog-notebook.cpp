
/*
 * A wrapper for Gtk::Notebook.
 *
 * Copyright (C) 2018 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include "ink-dialog-notebook.h"
#include "ink-dialog-container.h"
#include "ink-dialog-base.h"
#include "ink-multipaned.h"
#include "ink-window-dock.h"
#include "ink-application.h"

#include <iostream>

// Doesn't work as you can't create multiple instances of a widget
// from one Gtk::Builder.
//bool InkDialogNotebook::_builder_built = false;
//Glib::RefPtr<Gtk::Builder> InkDialogNotebook::_builder;

InkDialogNotebook::InkDialogNotebook (InkDialogContainer* container)
  : Gtk::Box (Gtk::ORIENTATION_VERTICAL)
  , _container(container)
{
  set_name("InkDialogNotebook");
  set_vexpand();

  // Notebook with menu
  _action_menu.set_title("Dialogs");

  Gtk::MenuItem* new_menu_item = nullptr;

  // Close tab
  new_menu_item = Gtk::manage(new Gtk::MenuItem("Close Tab"));
  new_menu_item->signal_activate().connect(
     sigc::mem_fun(*this, &InkDialogNotebook::close_tab_callback));
  _action_menu.append(*new_menu_item);

  // Hide tab label
  new_menu_item = Gtk::manage(new Gtk::MenuItem("Hide Tab Label"));
  new_menu_item->signal_activate().connect(
     sigc::mem_fun(*this, &InkDialogNotebook::hide_tab_label_callback));
  _action_menu.append(*new_menu_item);

  // Show tab label
  new_menu_item = Gtk::manage(new Gtk::MenuItem("Show Tab Label"));
  new_menu_item->signal_activate().connect(
     sigc::mem_fun(*this, &InkDialogNotebook::show_tab_label_callback));
  _action_menu.append(*new_menu_item);

  // Hide all tab labels
  new_menu_item = Gtk::manage(new Gtk::MenuItem("Hide All Tab Labels"));
  new_menu_item->signal_activate().connect(
     sigc::mem_fun(*this, &InkDialogNotebook::hide_all_tab_labels_callback));
  _action_menu.append(*new_menu_item);

  // Show all tab labels
  new_menu_item = Gtk::manage(new Gtk::MenuItem("Show All Tab Labels"));
  new_menu_item->signal_activate().connect(
     sigc::mem_fun(*this, &InkDialogNotebook::show_all_tab_labels_callback));
  _action_menu.append(*new_menu_item);

  // Move to new window
  new_menu_item = Gtk::manage(new Gtk::MenuItem("Move Tab to New Window"));
  new_menu_item->signal_activate().connect(
     sigc::mem_fun(*this, &InkDialogNotebook::move_tab_callback));
  _action_menu.append(*new_menu_item);

  // Close notebook
  new_menu_item = Gtk::manage(new Gtk::MenuItem("Close Notebook"));
  new_menu_item->signal_activate().connect(
     sigc::mem_fun(*this, &InkDialogNotebook::close_notebook_callback));
  _action_menu.append(*new_menu_item);

  new_menu_item = Gtk::manage(new Gtk::SeparatorMenuItem());
  _action_menu.append(*new_menu_item);

  // Add Dockable Dialogs
  _builder = Gtk::Builder::create();
  try
    {
      _builder->add_from_file("dialogs.xml");
    }
  catch (const Glib::Error& error)
    {
      std::cerr << "InkDialogNotebook::InkDialogNotebook: dialog menu error:"
                << error.what() << std::endl;
    }

  Gtk::Menu* menu_dialogs = nullptr;
  _builder->get_widget("menu-dialogs", menu_dialogs);
  if (!menu_dialogs) {
    std::cerr << "InkDialogNotebook: dialog menu not found!" << std::endl;
  } else {
    Gtk::MenuItem* menu_item = Gtk::manage(new Gtk::MenuItem("Dockable Dialogs"));
    _action_menu.append(*menu_item);
    menu_item->set_submenu(*menu_dialogs);
  }

  _action_menu.show_all_children();

  // Setup notebook
  _notebook.set_group_name("InkscapeDialogGroup"); // Could be param.
  _notebook.popup_enable();  // Doesn't hurt.

  // Add notebook action menu
  _action_button.set_image_from_icon_name("pan-start-symbolic");
  _action_button.set_popup(_action_menu);
  _action_button.show();  // show_all() below doesn't show this.
  _notebook.set_action_widget(&_action_button, Gtk::PACK_END);

  //_notebook.signal_drag_begin().connect(  sigc::mem_fun(*this, &InkDialogNotebook::on_drag_begin));
  _notebook.signal_drag_end().connect(    sigc::mem_fun(*this, &InkDialogNotebook::on_drag_end));
  _notebook.signal_drag_failed().connect( sigc::mem_fun(*this, &InkDialogNotebook::on_drag_failed));
  // drag_data_received(), drag_motion(), drag_drop(): Not useful!

  _notebook.signal_page_added().connect(  sigc::mem_fun(*this, &InkDialogNotebook::on_page_added));
  _notebook.signal_page_removed().connect(sigc::mem_fun(*this, &InkDialogNotebook::on_page_removed));

  // Bottom bar
  _close_button.set_label("Close");
  _close_button.signal_button_release_event().connect(
     sigc::mem_fun(*this, &InkDialogNotebook::close_notebook_button_callback));

  _bar.set_orientation (Gtk::ORIENTATION_HORIZONTAL);
  _bar.add (_close_button);

  // Add all and show
  add (_notebook);
  add (_bar);

  show_all();
}


// Adds a widget as a new page. Fails since builder widgets are not reusable!
// We could load builder again and that might work....
void
InkDialogNotebook::add_page(Gtk::Widget& page, Glib::ustring label)
{
  page.set_vexpand();

  Gtk::Widget* widget = nullptr;
  Glib::ustring name = "NotebookTab" + label;
  _builder->get_widget(name, widget);
  if (!widget) {
    std::cerr << "InkDialogNotebook::add_page: Did not find tab widget!" << std::endl;
    widget = Gtk::manage(new Gtk::Label(label));
  }

  _notebook.append_page (page, *widget);
  _notebook.set_menu_label_text(page, label);
  _notebook.set_tab_reorderable (page);
  _notebook.set_tab_detachable (page);
  _notebook.show_all();
}


// Adds a widget as a new page with tab.
void
InkDialogNotebook::add_page(Gtk::Widget& page, Gtk::Widget& tab, Glib::ustring label)
{
  page.set_vexpand();

  _notebook.append_page (page, tab);
  _notebook.set_menu_label_text(page, label);
  _notebook.set_tab_reorderable (page);
  _notebook.set_tab_detachable (page);
  _notebook.show_all();

  // Switch notebook to new page.
  int page_number = _notebook.page_num(page);
  _notebook.set_current_page(page_number);
}


// Moves a page from a different notebook to this one.
void
InkDialogNotebook::move_page(Gtk::Widget& page)
{
  // Find old notebook
  Gtk::Notebook* old_notebook = dynamic_cast<Gtk::Notebook*>(page.get_parent());
  if (!old_notebook) {
    std::cerr << "InkDialogNotebook::move_page: page not in notebook!" << std::endl;
    return;
  }

  Gtk::Widget* tab = old_notebook->get_tab_label(page);

  tab->reference();
  page.reference();
  old_notebook->detach_tab(page);
  _notebook.append_page (page, *tab);
  tab->unreference();
  page.unreference();

  _notebook.set_tab_reorderable (page);
  _notebook.set_tab_detachable (page);
  _notebook.show_all();
}


// Signal handlers - Notebook

// Does nothing at the moment, could be used to change cursor.
void
InkDialogNotebook::on_drag_begin(const Glib::RefPtr<Gdk::DragContext> context)
{
  std::vector<std::string> targets = context->list_targets();
  for (auto target : targets) {
    std::cout << "   " << target << std::endl;
  }
}

// Closes the notebook if empty.
void
InkDialogNotebook::on_drag_end(const Glib::RefPtr<Gdk::DragContext> context)
{
  if (_notebook.get_n_pages() == 0) {
    close_notebook_callback();
  }
}


// A failed drag means that the page was not dropped on an existing notebook.
// Thus create a new window with notebook to move page to.
bool
InkDialogNotebook::on_drag_failed(const Glib::RefPtr<Gdk::DragContext> context, Gtk::DragResult result)
{
  Gtk::Widget* source = Gtk::Widget::drag_get_source_widget(context);

  // Find source notebook and page
  Gtk::Notebook* old_notebook = dynamic_cast<Gtk::Notebook*>(source);
  if (!old_notebook) {
    std::cerr << "InkDialogNotebook::on_drag_failed: notebook not found!" << std::endl;
    return false;
  }

  // Find page
  Gtk::Widget* page = old_notebook->get_nth_page( old_notebook->get_current_page() );
  if (!page) {
    std::cerr << "InkDialogNotebook::on_drag_failed: page not found!" << std::endl;
    return false;
  }

  // Find window
  Gtk::Window* window = dynamic_cast<Gtk::Window*>(get_toplevel());
  if (!window) {
    std::cerr << "InkDialogNotebook::on_drag_failed: Failed to get window!" << std::endl;
    return false;
  }

  // Find app
  Glib::RefPtr<Gtk::Application> app = window->get_application();
  if (!app) {
    std::cerr << "InkDialogNotebook::on_drag_failed: Failed to get application!" << std::endl;
    return false;
  }
  
  Glib::RefPtr<InkApplication> inkapp = Glib::RefPtr<InkApplication>::cast_dynamic(app);
  if (!app) {
    std::cerr << "InkDialogNotebook::on_drag_failed: Failed to get application!" << std::endl;
    return false;
  }

  // Move page to notebook in new dock.
  inkapp->create_window_dock(page);

  return true;
}


// Update dialog list on adding a page.
void
InkDialogNotebook::on_page_added(Gtk::Widget* page, int page_num)
{
  // Add to dialog list
  InkDialogBase* dialog = dynamic_cast<InkDialogBase*>(page);
  if (dialog) {
    _container->link_dialog(dialog);
  }
  _container->list_dialogs();
}


// Update dialog list on removing a page.
void
InkDialogNotebook::on_page_removed(Gtk::Widget* page, int page_num)
{
  // Remove from dialog list
  InkDialogBase* dialog = dynamic_cast<InkDialogBase*>(page);
  if (dialog) {
    _container->unlink_dialog(dialog);
  }

  // We cannot remove an empty notebook here as a user may drop the tab back onto this notebook.

  _container->list_dialogs();
}


// Signal handlers - Notebook menu (action)

void
InkDialogNotebook::close_tab_callback()
{
  _notebook.remove_page(_notebook.get_current_page() );
}


void
InkDialogNotebook::hide_tab_label_callback()
{
  // Find page and label.
  Gtk::Widget* page = _notebook.get_nth_page( _notebook.get_current_page() );
  Gtk::Widget* tab  = _notebook.get_tab_label(*page);

  Gtk::Box* box = dynamic_cast<Gtk::Box*>(tab);
  if (!box) return;

  std::vector<Gtk::Widget*> children = box->get_children();
  Gtk::Label* label = dynamic_cast<Gtk::Label*>(children[1]);
  if (label) {
    label->hide();
  }
}


void
InkDialogNotebook::show_tab_label_callback()
{
  // Find page and label.
  Gtk::Widget* page = _notebook.get_nth_page( _notebook.get_current_page() );
  Gtk::Widget* tab  = _notebook.get_tab_label(*page);

  Gtk::Box* box = dynamic_cast<Gtk::Box*>(tab);
  if (!box) return;

  std::vector<Gtk::Widget*> children = box->get_children();
  Gtk::Label* label = dynamic_cast<Gtk::Label*>(children[1]);
  if (label) {
    label->show();
  }
}


void
InkDialogNotebook::hide_all_tab_labels_callback()
{
  std::vector<Gtk::Widget*> pages = _notebook.get_children();

  for (auto page : pages) {
    Gtk::Widget* tab = _notebook.get_tab_label(*page);

    Gtk::Box* box = dynamic_cast<Gtk::Box*>(tab);
    if (!box) continue;
    
    std::vector<Gtk::Widget*> children = box->get_children();
    Gtk::Label* label = dynamic_cast<Gtk::Label*>(children[1]);
    if (label) {
      label->hide();
    }
  }
}


void
InkDialogNotebook::show_all_tab_labels_callback()
{
  std::vector<Gtk::Widget*> pages = _notebook.get_children();

  for (auto page : pages) {
    Gtk::Widget* tab = _notebook.get_tab_label(*page);

    Gtk::Box* box = dynamic_cast<Gtk::Box*>(tab);
    if (!box) continue;
    
    std::vector<Gtk::Widget*> children = box->get_children();
    Gtk::Label* label = dynamic_cast<Gtk::Label*>(children[1]);
    if (label) {
      label->show();
    }
  }
}


void
InkDialogNotebook::move_tab_callback()
{
  // Find page.
  Gtk::Widget* page = _notebook.get_nth_page( _notebook.get_current_page() );

  // Find window
  Gtk::Window* window = dynamic_cast<Gtk::Window*>(get_toplevel());
  if (!window) {
    std::cerr << "InkDialogNotebook::on_drag_failed: Failed to get window!" << std::endl;
    return;
  }

  // Find app
  Glib::RefPtr<Gtk::Application> app = window->get_application();
  if (!app) {
    std::cerr << "InkDialogNotebook::on_drag_failed: Failed to get application!" << std::endl;
    return;
  }
  
  Glib::RefPtr<InkApplication> inkapp = Glib::RefPtr<InkApplication>::cast_dynamic(app);
  if (!app) {
    std::cerr << "InkDialogNotebook::on_drag_failed: Failed to get application!" << std::endl;
    return;
  }

  // Move page to notebook in new dock.
  inkapp->create_window_dock(page);
}


void
InkDialogNotebook::action_activate_callback(Glib::ustring label)
{
  Gtk::Label* dialog = Gtk::manage(new Gtk::Label(label));
  add_page(*dialog, label + " Tab");
}


// Signal handlers - Other

void
InkDialogNotebook::close_notebook_callback()
{
  // We can't just delete this without updating the parent InkMultipaned.

  // Search for InkMultipaned
  InkMultipaned* multipaned = nullptr;

  Gtk::Widget* parent = get_parent();
  while (parent != nullptr) {
    multipaned = dynamic_cast<InkMultipaned*>(parent);
    if (multipaned) {
      break;
    }
    parent = parent->get_parent();
  }

  if (multipaned) {
    multipaned->remove(*this); // Deletes "this" too.
  } else {
    std::cerr << "InkDialogNotebook::close_notebook_callback2: Unexpected parent!" << std::endl;
    get_parent()->remove(*this);
    delete this;
  }
}


bool
InkDialogNotebook::close_notebook_button_callback(GdkEventButton* release_event)
{
  close_notebook_callback();

  return true;
}



#include "ink-application.h"

int main(int argc, char *argv[])
{
    auto application = InkApplication::create();

    return application->run(argc, argv);
}

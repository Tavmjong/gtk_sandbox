#ifndef INK_DOCUMENT_H
#define INK_DOCUMENT_H

#include <giomm/simpleactiongroup.h>

class InkDocument
{
public:
  InkDocument();
  virtual ~InkDocument() {}

  Glib::ustring get_color() { return _color; }
  void set_color(Glib::ustring& color) { _color = color; }

  Glib::RefPtr<Gio::SimpleActionGroup> get_action_group() { return _action_group; }

private:
  Glib::RefPtr<Gio::SimpleActionGroup> _action_group;
  Glib::RefPtr<Gio::SimpleAction> _bool_action;

  Glib::ustring _color;
  bool _bool;

  void on_bool();
};

#endif //INK_DOCUMENT_H

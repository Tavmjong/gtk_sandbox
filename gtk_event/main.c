
#include <stdio.h>
#include <gtk/gtk.h>

static gboolean
key_pressed (GtkEventController *controller,
             guint               keyval,
             guint               keycode,
             GdkModifierType     modifiers,
             GtkWidget          *widget)
{
  char ch = keyval < 256 ? keyval : '-';
  printf ("keyval %u (%c), keycode %u\n", keyval, ch, keycode);
  switch (keyval)
    {
      case GDK_KEY_Return:
      case GDK_KEY_KP_Enter:
        printf ("Return\n");
        break;

      default:
        break;
    }

  return GDK_EVENT_PROPAGATE;
}

static void
activate (GtkApplication* app, gpointer user_data)
{
  GtkWidget *window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Window");
  gtk_window_set_default_size (GTK_WINDOW (window), 200, 200);

#if GTK_CHECK_VERSION(4, 0, 0)
  GtkEventController* event_controller_key = gtk_event_controller_key_new();
  g_signal_connect (event_controller_key, "key-pressed", G_CALLBACK (key_pressed), window);
  gtk_widget_add_controller (window, event_controller_key);

  gtk_widget_show (window);
#else
  GtkEventController* event_controller_key = gtk_event_controller_key_new(window);
  g_signal_connect (event_controller_key, "key-pressed", G_CALLBACK (key_pressed), window);

  gtk_widget_show_all (window);
#endif
}


int main(int argc, char **argv)
{
  printf("Type in window!\n");

  GtkApplication *app = gtk_application_new ("org.tavmjong.controllers", G_APPLICATION_DEFAULT_FLAGS);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  int status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);
  

  return status;
}


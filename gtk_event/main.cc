
#include <iostream>
#include <gtkmm.h>


#if GTK_CHECK_VERSION(4, 0, 0)
static bool
key_pressed (guint               keyval,
             guint               keycode,
             Gdk::ModifierType   modifiers)
{
  char ch = keyval < 256 ? keyval : '-';
  printf ("keyval %u (%c), keycode %u\n", keyval, ch, keycode);
  switch (keyval)
    {
      case GDK_KEY_Return:
      case GDK_KEY_KP_Enter:
        printf ("Return\n");
        break;

      default:
        break;
    }

  return false;
}
#else
static gboolean
key_pressed (GtkEventController *controller,
             guint               keyval,
             guint               keycode,
             GdkModifierType     modifiers,
             GtkWidget          *widget)
{
  char ch = keyval < 256 ? keyval : '-';
  printf ("keyval %u (%c), keycode %u\n", keyval, ch, keycode);
  switch (keyval)
    {
      case GDK_KEY_Return:
      case GDK_KEY_KP_Enter:
        printf ("Return\n");
        break;

      default:
        break;
    }

  return GDK_EVENT_PROPAGATE;
}
#endif

class Window: public Gtk::Window
{
public:
#if GTK_CHECK_VERSION(4, 0, 0)
  Window()
#else
  Window(const Glib::RefPtr<Gtk::Application>& app)
#endif
  {
    set_title("Window");
    set_default_size(200, 200);

#if GTK_CHECK_VERSION(4, 0, 0)
    auto event_controller_key = Gtk::EventControllerKey::create();
    event_controller_key->signal_key_pressed().connect(sigc::ptr_fun(key_pressed), false);
    add_controller(event_controller_key);

    show();
#else
    GtkEventController* event_controller_key = gtk_event_controller_key_new(GTK_WIDGET(this->gobj()));
    g_signal_connect (event_controller_key, "key-pressed", G_CALLBACK (key_pressed), GTK_WIDGET(this->gobj()));

    show_all();
#endif
  }
};


int main(int argc, char **argv)
{
  printf("Type in window!\n");

#if GTK_CHECK_VERSION(4, 0, 0)
  auto application = Gtk::Application::create("org.tavmjong.controllers");
  return application->make_window_and_run<Window>(argc, argv);
#else
  auto application = Gtk::Application::create(argc, argv, "org.tavmjong.controllers");
  Window window(application);
  return application->run(window);
#endif

}


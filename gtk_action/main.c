
#include <gtk/gtk.h>

static void
activate (GtkApplication *app,
          gpointer user_data)
{
  GtkBuilder *builder;
  GMenuModel *menubar;

  GtkWidget *window;
  GtkWidget *grid;
  GtkWidget *menubar2;

  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Test Application - Simple");

  builder = gtk_builder_new_from_file ("test-application.ui");
  menubar = G_MENU_MODEL (gtk_builder_get_object (builder, "menu-bar"));
  gtk_application_set_menubar (GTK_APPLICATION (app), menubar);

  menubar2 = GTK_WIDGET (gtk_builder_get_object (builder, "menu-bar2"));

  grid = gtk_grid_new ();
  gtk_grid_attach (GTK_GRID (grid), menubar2, 0, 0, 1, 1);
  gtk_container_add (GTK_CONTAINER (window), GTK_WIDGET (grid));
  gtk_widget_show (GTK_WIDGET (grid));

  gtk_widget_show (window);

  g_object_unref (builder);
}

static void
test3_change_state (GSimpleAction *action,
                    GVariant      *state,
                    gpointer       user_data)
{
  const gchar *name;
  gint value;

  name = g_action_get_name (G_ACTION (action));
  value = g_variant_get_int32 (state);
  g_print ("Action: %s, state: %i\n", name, value);

  g_simple_action_set_state (action, state);
}

static GActionEntry app_entries[] = {
  { "test3", NULL, "i", "0", test3_change_state },
};

static void
startup (GApplication *app,
         gpointer user_data)
{
  g_action_map_add_action_entries (G_ACTION_MAP (app), app_entries, G_N_ELEMENTS (app_entries), app);
}

int main(int argc, char *argv[])
{

  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.tavmjong.testapplication", G_APPLICATION_FLAGS_NONE);

  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  g_signal_connect (app, "startup",  G_CALLBACK (startup),  NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
  

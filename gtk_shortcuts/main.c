

#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <gio/gio.h>
#include <glib-object.h>

static void
test1 (GSimpleAction* action, GVariant* parameter, gpointer data)
{
  printf ("Action1\n");

  GListStore *liststore = (GListStore*)data;
  GtkNamedAction* named_action = (GtkNamedAction*) gtk_named_action_new ("win.test2");
  GtkShortcutTrigger* trigger = gtk_shortcut_trigger_parse_string ("<Control>B");
  GtkShortcut* shortcut = gtk_shortcut_new (trigger, (GtkShortcutAction*) named_action);

  g_list_store_append (liststore, (GObject*) shortcut);
}

static void
test2 (GSimpleAction* action, GVariant* parameter, gpointer data)
{
  printf ("Action2\n");
}

static void
test3 (GSimpleAction* action, GVariant* parameter, gpointer data)
{
  printf ("Action3\n");
}

static void
toggle_handler (GSimpleAction* action, GVariant* parameter, gpointer data)
{
  GVariant* state = g_action_get_state ((GAction*) action);
  bool old_value = g_variant_get_boolean (state);
  //  bool new_value = g_variant_get_boolean (parameter);
  //  g_simple_action_set_state (action, parameter);
  g_simple_action_set_state (action, g_variant_new_boolean (!old_value));
  printf ("Toggle: %s -> %s\n", old_value ? "true" : "false", !old_value ? "true" : "false");
}

static void
radio_handler (GSimpleAction* action, GVariant* parameter, gpointer data)
{
  GVariant* state = g_action_get_state ((GAction*) action);
  gint32 old_value = g_variant_get_int32 (state);
  gint32 new_value = g_variant_get_int32 (parameter);
  g_simple_action_set_state (action, parameter);
  printf ("Radio: %i -> %i\n", old_value, new_value);
}

static void
activate (GtkApplication* app,
          gpointer        user_data)
{
  GListStore* liststore = g_list_store_new (gtk_shortcut_get_type ());

  // Create a new window.
  GtkWidget* window = gtk_application_window_new (app);
  gtk_window_set_title ((GtkWindow*)window, "Shortcut Test");
  gtk_window_set_default_size ((GtkWindow*) window, 300, 200);

  // Create actions.
  GSimpleAction* action1 = g_simple_action_new ("test1", NULL);
  GSimpleAction* action2 = g_simple_action_new ("test2", NULL);
  GSimpleAction* action3 = g_simple_action_new ("test3", NULL);

  GSimpleAction* toggle =
    g_simple_action_new_stateful ("toggle", NULL, g_variant_new_boolean(TRUE));

  GSimpleAction* radio =
    g_simple_action_new_stateful ("radio",  g_variant_type_new ("i"), g_variant_new_int32(0));

  g_signal_connect (action1, "activate", (GCallback) test1, (gpointer) liststore);
  g_signal_connect (action2, "activate", (GCallback) test2, NULL);
  g_signal_connect (action3, "activate", (GCallback) test3, NULL);
  g_signal_connect (toggle,  "activate", (GCallback) toggle_handler, NULL);
  g_signal_connect (radio,   "activate", (GCallback) radio_handler, NULL);

  g_action_map_add_action ((GActionMap*) window, (GAction*) action1);
  g_action_map_add_action ((GActionMap*) window, (GAction*) action2);
  g_action_map_add_action ((GActionMap*) window, (GAction*) action3);
  g_action_map_add_action ((GActionMap*) window, (GAction*) toggle);
  g_action_map_add_action ((GActionMap*) window, (GAction*) radio);

  // Create menu.
  GMenu* menu = g_menu_new();
  GMenu* submenu = g_menu_new();
  GMenuItem* menuitem1 = g_menu_item_new ("Test1",  "win.test1");
  GMenuItem* menuitem2 = g_menu_item_new ("Test2",  "win.test2");
  GMenuItem* menuitem3 = g_menu_item_new ("Test3",  "win.test3");
  GMenuItem* menuitem4 = g_menu_item_new ("Toggle", "win.toggle");
  GMenuItem* menuitem5 = g_menu_item_new ("Radio0", "win.radio(0)");
  GMenuItem* menuitem6 = g_menu_item_new ("Radio1", "win.radio(1)");
  GMenuItem* menuitem7 = g_menu_item_new ("Radio2", "win.radio(2)");

  g_menu_append_item (submenu, menuitem1);
  g_menu_append_item (submenu, menuitem2);
  g_menu_append_item (submenu, menuitem3);
  g_menu_append_item (submenu, menuitem4);
  g_menu_append_item (submenu, menuitem5);
  g_menu_append_item (submenu, menuitem6);
  g_menu_append_item (submenu, menuitem7);
  g_menu_append_submenu (menu, "Submenu", (GMenuModel*) submenu);

  // Use menu.
  GtkPopoverMenuBar* menu_bar = (GtkPopoverMenuBar*) gtk_popover_menu_bar_new_from_model ((GMenuModel*) menu);

  GtkMenuButton* menu_button = (GtkMenuButton*) gtk_menu_button_new();
  gtk_menu_button_set_menu_model (menu_button, (GMenuModel*) menu);

  GtkBox* box = (GtkBox*) gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
  gtk_box_append (box, (GtkWidget*) menu_bar);
  gtk_box_append (box, (GtkWidget*) menu_button);
  gtk_window_set_child ((GtkWindow*) window, (GtkWidget*) box);
  gtk_window_present ((GtkWindow*) window);

  /* Shortcuts */
  GtkNamedAction* named_action1 = (GtkNamedAction*) gtk_named_action_new ("win.test1");
  GtkNamedAction* named_action2 = (GtkNamedAction*) gtk_named_action_new ("win.test2");
  GtkNamedAction* named_action3 = (GtkNamedAction*) gtk_named_action_new ("win.test3");
  GtkNamedAction* named_action4 = (GtkNamedAction*) gtk_named_action_new ("win.toggle");
  GtkNamedAction* named_action5 = (GtkNamedAction*) gtk_named_action_new ("win.radio(0)");
  GtkNamedAction* named_action6 = (GtkNamedAction*) gtk_named_action_new ("win.radio(1)");
  GtkNamedAction* named_action7 = (GtkNamedAction*) gtk_named_action_new ("win.radio(2)");

  GtkShortcutTrigger* trigger1 = gtk_shortcut_trigger_parse_string ("<Control>A");
  GtkShortcutTrigger* trigger2 = gtk_shortcut_trigger_parse_string ("<Control>B");
  GtkShortcutTrigger* trigger3 = gtk_shortcut_trigger_parse_string ("<Control>C");
  GtkShortcutTrigger* trigger4 = gtk_shortcut_trigger_parse_string ("<Control>D");
  GtkShortcutTrigger* trigger5 = gtk_shortcut_trigger_parse_string ("<Control>E");
  GtkShortcutTrigger* trigger6 = gtk_shortcut_trigger_parse_string ("<Control>F");
  GtkShortcutTrigger* trigger7 = gtk_shortcut_trigger_parse_string ("<Control>G");

  GtkShortcut* shortcut1 = gtk_shortcut_new (trigger1, (GtkShortcutAction*) named_action1);
  GtkShortcut* shortcut2 = gtk_shortcut_new (trigger2, (GtkShortcutAction*) named_action2);
  GtkShortcut* shortcut3 = gtk_shortcut_new (trigger3, (GtkShortcutAction*) named_action3);
  GtkShortcut* shortcut4 = gtk_shortcut_new (trigger4, (GtkShortcutAction*) named_action4);
  GtkShortcut* shortcut5 = gtk_shortcut_new (trigger5, (GtkShortcutAction*) named_action5);
  GtkShortcut* shortcut6 = gtk_shortcut_new (trigger6, (GtkShortcutAction*) named_action6);
  GtkShortcut* shortcut7 = gtk_shortcut_new (trigger7, (GtkShortcutAction*) named_action7);

  g_list_store_append (liststore, (GObject*) shortcut1);
  // g_list_store_append (liststore, (GObject*) shortcut2);
  g_list_store_append (liststore, (GObject*) shortcut3);
  g_list_store_append (liststore, (GObject*) shortcut4);
  g_list_store_append (liststore, (GObject*) shortcut5);
  g_list_store_append (liststore, (GObject*) shortcut6);
  g_list_store_append (liststore, (GObject*) shortcut7);

  GtkShortcutController* shortcut_controller = (GtkShortcutController*) gtk_shortcut_controller_new_for_model ((GListModel*) liststore);
  gtk_event_controller_set_propagation_phase ((GtkEventController*) shortcut_controller, GTK_PHASE_BUBBLE);
  gtk_shortcut_controller_set_scope (shortcut_controller, GTK_SHORTCUT_SCOPE_GLOBAL);
  gtk_widget_add_controller (window, (GtkEventController*) shortcut_controller);

  /* g_object_unref (shortcut_controller); */
  g_object_unref (liststore);
  g_object_unref (shortcut1);
  g_object_unref (shortcut2);
  g_object_unref (shortcut3);
  g_object_unref (shortcut4);
  /* g_object_unref (named_action); */
  /* g_object_unref (trigger); */
  /* g_object_unref (window); */
}

int main (int argc, char ** argv) {
  GtkApplication* app = gtk_application_new ("org.tavmjong.example.shortcuts_c", G_APPLICATION_DEFAULT_FLAGS);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  gint result = g_application_run ((GApplication*) app, argc, argv);
  g_object_unref (app);

  return result;
}


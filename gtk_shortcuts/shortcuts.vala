
// valac --pkg gtk4 --pkg gio-2.0 shortcuts.vala

// To get shortcuts to work:
// valac --pkg gtk4 --pkg gio-2.0 -C shortcuts.vala
// Comment out G_TYPE_INSTANCE_GET_INTERFACE... line
// Add:
// gtk_widget_add_controller ((GtkWidget*) window, (GtkEventController*) shortcut_controller);
// gcc -o shortcutsc -g shortcuts.c `pkg-config gtk4 --cflags --libs`

int main (string[] argv) {

  var app = new Gtk.Application("org.tavmjong.example.shortcuts",
                                GLib.ApplicationFlags.DEFAULT_FLAGS);

  app.activate.connect (() => {
    // Create a new window.
    var window = new Gtk.ApplicationWindow (app);
	window.set_title ("Shortcut Test");
	window.set_default_size (300, 200);

    // Create actions.
    var action1 = new GLib.SimpleAction ("test1", null);
    var action2 = new GLib.SimpleAction ("test2", null);
    var action3 = new GLib.SimpleAction ("test3", null);
	var toggle  = new GLib.SimpleAction.stateful ("toggle", null, new Variant.boolean (false));
	var radio   = new GLib.SimpleAction.stateful ("radio",  new VariantType("i"), new Variant.int32 (0));
	
    action1.activate.connect (() => { print ("Action1\n"); });
    action2.activate.connect (() => { print ("Action2\n"); });
    action3.activate.connect (() => { print ("Action3\n"); });

	toggle.activate.connect (() => {
	  Variant state = toggle.get_state ();
	  bool b = state.get_boolean();
	  toggle.set_state (new Variant.boolean(!b));
	  print (@"Toggle: $b -> $(!b)\n");
	});

	radio.activate.connect ((variant) => {
	  int32 new_value = variant.get_int32();
	  Variant state = radio.get_state ();
	  int32 old_value = state.get_int32();
	  radio.set_state (variant);
	  print (@"Radio: $old_value -> $new_value\n");
	});

	window.add_action(action1);
    window.add_action(action2);
    window.add_action(action3);
	window.add_action(toggle);
	window.add_action(radio);

    // Create menu.
    var menu = new GLib.Menu ();
    var submenu = new GLib.Menu();  // Without submenu, menubar is unhappy!
    var menuitem1 = new GLib.MenuItem ("Test1",  "win.test1");
    var menuitem2 = new GLib.MenuItem ("Test2",  "win.test2");
    var menuitem3 = new GLib.MenuItem ("Test3",  "win.test3");
    var menuitem4 = new GLib.MenuItem ("Toggle", "win.toggle");
    var menuitem5 = new GLib.MenuItem ("Radio0", "win.radio(0)");
    var menuitem6 = new GLib.MenuItem ("Radio1", "win.radio(1)");
    var menuitem7 = new GLib.MenuItem ("Radio2", "win.radio(2)");

    submenu.append_item(menuitem1);
    submenu.append_item(menuitem2);
    submenu.append_item(menuitem3);
    submenu.append_item(menuitem4);
    submenu.append_item(menuitem5);
    submenu.append_item(menuitem6);
    submenu.append_item(menuitem7);
    menu.append_submenu("Submenu", submenu);

	// Use menu.
    var menubar = new Gtk.PopoverMenuBar.from_model(menu);

    var menubutton = new Gtk.MenuButton ();
    menubutton.set_menu_model(menu);

    var box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
    box.append(menubar);
    box.append(menubutton);
    window.set_child(box);
    window.present();

    // Create shortcuts.
    var shortcut_action1 = new Gtk.NamedAction("win.test1");
    var shortcut_action2 = new Gtk.NamedAction("win.test2");
    var shortcut_action3 = new Gtk.NamedAction("win.test3");
    var shortcut_action4 = new Gtk.NamedAction("win.toggle");
    var shortcut_action5 = new Gtk.NamedAction("win.radio(0)");
    var shortcut_action6 = new Gtk.NamedAction("win.radio(1)");
    var shortcut_action7 = new Gtk.NamedAction("win.radio(2)");

    var shortcut1 = new Gtk.Shortcut (Gtk.ShortcutTrigger.parse_string("<Control>A"), shortcut_action1);
    var shortcut2 = new Gtk.Shortcut (Gtk.ShortcutTrigger.parse_string("<Control>B"), shortcut_action2);
    var shortcut3 = new Gtk.Shortcut (Gtk.ShortcutTrigger.parse_string("<Control>C"), shortcut_action3);
    var shortcut4 = new Gtk.Shortcut (Gtk.ShortcutTrigger.parse_string("<Control>D"), shortcut_action4);
    var shortcut5 = new Gtk.Shortcut (Gtk.ShortcutTrigger.parse_string("<Control>E"), shortcut_action5);
    var shortcut6 = new Gtk.Shortcut (Gtk.ShortcutTrigger.parse_string("<Control>F"), shortcut_action6);
    var shortcut7 = new Gtk.Shortcut (Gtk.ShortcutTrigger.parse_string("<Control>G"), shortcut_action7);

    var liststore = new GLib.ListStore(typeof (Gtk.Shortcut));
    liststore.append(shortcut1);
    liststore.append(shortcut2);
    liststore.append(shortcut3);
    liststore.append(shortcut4);
    liststore.append(shortcut5);
    liststore.append(shortcut6);
    liststore.append(shortcut7);

    var shortcut_controller = new Gtk.ShortcutController.for_model(liststore);
    shortcut_controller.set_propagation_phase(Gtk.PropagationPhase.BUBBLE);
    shortcut_controller.set_scope(Gtk.ShortcutScope.GLOBAL);
    window.add_controller(shortcut_controller);
  });

  return app.run (argv);
}

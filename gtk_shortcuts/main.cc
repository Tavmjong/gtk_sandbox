
#include <iostream>

#include <giomm.h>
#include <glibmm.h>
#include <gtkmm.h>

class Window: public Gtk::ApplicationWindow
{

public:
  Window()
  {
    set_title("Window");
    set_title ("Shortcut Test");
    set_default_size (300, 200);

    // Create actions.
    add_action(              "test1",  [=]() {
      std::cout << "Test1" << std::endl;

      liststore->append(Gtk::Shortcut::create(Gtk::ShortcutTrigger::parse_string("<Control>B"), Gtk::NamedAction::create("win.test2")));

      // Add/remove controller to refresh menu shortcuts!
      remove_controller(shortcut_controller);
      add_controller(shortcut_controller);

      // Try adding shortcuts the old way.
      std::vector<Glib::ustring> accels = { "<Control>Z" };

      auto app = get_application();
      if (app) {
        app->set_accels_for_action("win.radio(0)", accels);
      } else {
        std::cout << "No app yet!" << std::endl;
      }
    });
    add_action(              "test2",  []() { std::cout << "Test2" << std::endl;});
    add_action(              "test3",  []() { std::cout << "Test3" << std::endl;});
    toggle_action = add_action_bool(         "toggle", [=]() {
      bool value = false;
      toggle_action->get_state(value);
      toggle_action->set_state(Glib::Variant<bool>::create(!value));
      std::cout << "Toggle: " << (value ? "true -> false" : "false -> true") << std::endl;
    }, true);
    radio_action = add_action_radio_integer("radio",  [=](int i) {
      int value = -1;
      radio_action->get_state(value);
      radio_action->set_state(Glib::Variant<int>::create(i));
      std::cout << "Radio: " << value << " -> " << i << std::endl;
    }, 0);

    // Create menu.
    auto menu = Gio::Menu::create();
    auto submenu = Gio::Menu::create();
    auto menuitem1 = Gio::MenuItem::create("Test1",  "win.test1");
    auto menuitem2 = Gio::MenuItem::create("Test2",  "win.test2");
    auto menuitem3 = Gio::MenuItem::create("Test3",  "win.test3");
    auto menuitem4 = Gio::MenuItem::create("Toggle", "win.toggle");
    auto menuitem5 = Gio::MenuItem::create("Radio0", "win.radio(0)");
    auto menuitem6 = Gio::MenuItem::create("Radio1", "win.radio(1)");
    auto menuitem7 = Gio::MenuItem::create("Radio2", "win.radio(2)");

    auto icon1 = Gio::ThemedIcon::create("document-new", true);
    auto icon2 = Gio::ThemedIcon::create("document-open", true);
    auto icon3 = Gio::ThemedIcon::create("document-save", true);
    menuitem1->set_icon(icon1);
    menuitem2->set_icon(icon2);
    menuitem3->set_icon(icon3);

    submenu->append_item(menuitem1);
    submenu->append_item(menuitem2);
    submenu->append_item(menuitem3);
    submenu->append_item(menuitem4);
    submenu->append_item(menuitem5);
    submenu->append_item(menuitem6);
    submenu->append_item(menuitem7);
    menu->append_submenu("Submenu", submenu);

    // Use menu.
    auto menubar = Gtk::make_managed<Gtk::PopoverMenuBar>(menu);

    auto menubutton = Gtk::make_managed<Gtk::MenuButton>();
    menubutton->set_menu_model(menu);

    auto box = Gtk::make_managed<Gtk::Box>(Gtk::Orientation::VERTICAL);
    box->append(*menubar);
    box->append(*menubutton);
    set_child(*box);
    present();

    // Create shortcuts.
    const std::vector<std::pair<std::string, std::string>> shortcut_data = {
      {"win.test1",        "<Control>A"},
    //{"win.test2",        "<Control>B"},
      {"win.test3",        "<Control>C"},
      {"win.toggle",       "<Control>D"},
      {"win.radio(0)",     "<Control>E"},
      {"win.radio::1",     "<Control>F"},
      {"win.radio('2')",   "<Control>G"},
    };

    liststore = Gio::ListStore<Gtk::Shortcut>::create();
    for (auto [a, b] : shortcut_data) {
      liststore->append(Gtk::Shortcut::create(Gtk::ShortcutTrigger::parse_string(b), Gtk::NamedAction::create(a)));
    }

    shortcut_controller = Gtk::ShortcutController::create(liststore);
    shortcut_controller->set_propagation_phase(Gtk::PropagationPhase::BUBBLE);
    add_controller(shortcut_controller);

    // Try adding shortcuts the old way.
    std::vector<Glib::ustring> accels = { "r", "s" };

    auto app = get_application();
    if (app) {
      app->set_accels_for_action("win.radio(1)", accels);
    } else {
      std::cout << "No app yet!" << std::endl;
    }
  }

  // Functions for action testing.
  void test1() { std::cout << "test1: adding new shortcut" << std::endl;
    liststore->append(Gtk::Shortcut::create(Gtk::ShortcutTrigger::parse_string("<Control>C"), Gtk::NamedAction::create("win.test3")));

    // Add/remove controller to refresh menu shortcuts!
    remove_controller(shortcut_controller);
    add_controller(shortcut_controller);

    // Try adding shortcuts the old way.
    std::vector<Glib::ustring> accels = { "r", "s" };

    auto app = get_application();
    if (app) {
      app->set_accels_for_action("win.radio(1)", accels);
    } else {
      std::cout << "No app yet!" << std::endl;
    }
  }

  Glib::RefPtr<Gio::SimpleAction> toggle_action;
  Glib::RefPtr<Gio::SimpleAction> radio_action;
  Glib::RefPtr<Gio::ListStore<Gtk::Shortcut>> liststore;
  Glib::RefPtr<Gtk::ShortcutController> shortcut_controller;
};


int main(int argc, char *argv[]) {

  auto application = Gtk::Application::create("org.tavmjong.shortcuts_cc");
  return application->make_window_and_run<Window>(argc, argv);
}




#include <iostream>
#include <vector>

#include <gtkmm.h>

void test5() {
  std::cout << "test5" << std::endl;
}

void test6(int value) {
  std::cout << "test6: " << value << std::endl;
}

void test7(std::string value) {
  std::cout << "test7: " << value << std::endl;
}

void test8(int value) {
  std::cout << "test8 int: " << value << std::endl;
}

void test8(double value) {
  std::cout << "test8 double: " << value << std::endl;
}

class Window: public Gtk::ApplicationWindow
{
public:
#if GTK_CHECK_VERSION(4, 0, 0)
  Window()
#else
  Window(const Glib::RefPtr<Gtk::Application>& app)
#endif
  {
    set_title("Window");
    set_default_size(200, 200);

    add_action( "test1",                  sigc::mem_fun(*this, &Window::on_test1));
    add_action( "test2",                  sigc::mem_fun(*this, &Window::on_test2));
#if GTK_CHECK_VERSION(4, 0, 0)
    add_action( "test3a", sigc::bind(sigc::mem_fun(*this, &Window::on_test3), 1  ));
    add_action( "test3b", sigc::bind(sigc::mem_fun(*this, &Window::on_test3), 1.5)); // Rounds down!
    add_action( "test3c", sigc::bind(sigc::mem_fun(*this, &Window::on_test3), 2  ));
    // add_action( "test4a", sigc::bind(sigc::mem_fun<void, int>(*this, &Window::on_test4), 1  ));
#else
    add_action( "test3a", sigc::bind<int>(sigc::mem_fun(*this, &Window::on_test3), 1  ));
    add_action( "test3b", sigc::bind<int>(sigc::mem_fun(*this, &Window::on_test3), 1.5)); // Rounds down!
    add_action( "test3c", sigc::bind     (sigc::mem_fun(*this, &Window::on_test3), 2  ));
    add_action( "test4a", sigc::bind     (sigc::mem_fun<int>(   *this, &Window::on_test4), 1  ));
    add_action( "test4b", sigc::bind     (sigc::mem_fun<double>(*this, &Window::on_test4), 1  ));
#endif

    add_action( "test5",  &test5);
    add_action( "test6a", sigc::bind(sigc::ptr_fun(&test6), 6));
    add_action( "test6b", sigc::bind(&test6, 6));
    add_action( "test7a", sigc::bind(&test7, "Got it!"));
    // add_action( "test7b", sigc::bind(&test7, 7));
#if GTK_CHECK_VERSION(4, 0, 0)
    add_action( "test8a", sigc::bind(sigc::ptr_fun<void, int>   (&test8), 8));   // Return comes first
    add_action( "test8b", sigc::bind(sigc::ptr_fun<void, double>(&test8), 8.5)); // Return comes first
    // add_action( "test8c", sigc::bind(sigc::ptr_fun<double>      (&test8), 8.5));
#else
    add_action( "test8a", sigc::bind(sigc::ptr_fun<int, void>   (&test8), 8));   // Return comes last
    add_action( "test8b", sigc::bind(sigc::ptr_fun<double, void>(&test8), 8.5)); // Return comes last
    add_action( "test8c", sigc::bind(sigc::ptr_fun<double>      (&test8), 8.5)); // Don't actually need return
#endif
    add_action( "test9a", [ ]() { std::cout << "test9a" << std::endl; });
    add_action( "test9b", [=]() { std::cout << "test9b" << std::endl; });

    add_action_radio_string("testA", sigc::mem_fun(*this, &Window::on_testA), "A");

    _gmenu = Gio::Menu::create();

    auto menuitem1  = Gio::MenuItem::create("Test1",  "win.test1");
    auto menuitem2  = Gio::MenuItem::create("Test2",  "win.test2");
    auto menuitem3a = Gio::MenuItem::create("Test3a", "win.test3a");
    auto menuitem3b = Gio::MenuItem::create("Test3b", "win.test3b");
    auto menuitem3c = Gio::MenuItem::create("Test3c", "win.test3c");
    auto menuitem5  = Gio::MenuItem::create("Test5",  "win.test5");
    auto menuitem6a = Gio::MenuItem::create("Test6a", "win.test6a");
    auto menuitem6b = Gio::MenuItem::create("Test6b", "win.test6b");
    auto menuitem7a = Gio::MenuItem::create("Test7a", "win.test7a");
    // auto menuitem7b = Gio::MenuItem::create("Test7b", "win.test7b");
    auto menuitem8a = Gio::MenuItem::create("Test8a", "win.test8a");
    auto menuitem8b = Gio::MenuItem::create("Test8b", "win.test8b");
    auto menuitem9a = Gio::MenuItem::create("Test9a", "win.test9a");
    auto menuitem9b = Gio::MenuItem::create("Test9b", "win.test9b");

    auto menuitemA  = Gio::MenuItem::create("TestA", "win.testA('A')");
    auto menuitemB  = Gio::MenuItem::create("TestB", "win.testA('B')");
    auto menuitemC  = Gio::MenuItem::create("TestC", "win.testA('C')");

    _gmenu->append_item(menuitem1);
    _gmenu->append_item(menuitem2);
    _gmenu->append_item(menuitem3a);
    _gmenu->append_item(menuitem3b);
    _gmenu->append_item(menuitem3c);
    _gmenu->append_item(menuitem5);
    _gmenu->append_item(menuitem6a);
    _gmenu->append_item(menuitem6b);
    _gmenu->append_item(menuitem7a);
    // _gmenu->append_item(menuitem7b);
    _gmenu->append_item(menuitem8a);
    _gmenu->append_item(menuitem8b);
    _gmenu->append_item(menuitem9a);
    _gmenu->append_item(menuitem9b);

    _gmenu->append_item(menuitemA);
    _gmenu->append_item(menuitemB);
    _gmenu->append_item(menuitemC);


#if  GTK_CHECK_VERSION(4, 0, 0)
    _popover = new Gtk::PopoverMenu(_gmenu);
    _popover->set_parent(*this);

    _box_outer.set_homogeneous();
    _box_outer.append(_box_inner);
    _box_outer.append(_entry);

    set_child(_box_outer);

    _entry.set_extra_menu(_gmenu);
#else
    // Need widget for popover!
    _popover = new Gtk::Popover(_box_outer, _gmenu);

    _entry.signal_populate_popup().connect(sigc::mem_fun(*this, &Window::on_my_populate_popup));

    _box_outer.set_homogeneous();
    _box_outer.pack_start(_box_inner);
    _box_outer.pack_start(_entry);

    add(_box_outer);
#endif


#if GTK_CHECK_VERSION(4, 0, 0)
    auto controller = Gtk::GestureClick::create();
    controller->set_button(0);
    controller->signal_pressed().connect(sigc::mem_fun(*this, &Window::on_my_pressed));
    add_controller(controller);
#else
    signal_button_press_event().connect(sigc::mem_fun(*this, &Window::on_my_button_press_event));

    show_all();
#endif
  }

  ~Window() {
    if (_popover) {
      delete _popover;
    }
  }

  Gtk::Box _box_outer;
  Gtk::Box _box_inner;
  Gtk::Entry _entry;
  Glib::RefPtr<Gio::Menu> _gmenu;
  Gtk::Popover* _popover = nullptr;

#if GTK_CHECK_VERSION(4, 0, 0)

  void popup(int x, int y) {

    std::cout << "popup4" << std::endl;
    _popover->set_pointing_to(Gdk::Rectangle(x, y, 1, 1));
    _popover->popup();
  }

#else

  void popup(GdkEventButton *event) {

    std::cout << "popup3" << std::endl;

    switch (event->button) {
    case 1:
      {
        // Popover with Gtk::Menu.
        auto menu = Gtk::manage(new Gtk::Menu(_gmenu));
        menu->attach_to_widget(*this);
        menu->popup_at_pointer((GdkEvent*)event);
      }
      break;
    case 3:
      {
        // Popover with Gtk::Popover
        _popover->set_pointing_to(Gdk::Rectangle(event->x, event->y, 1, 1));
        _popover->popup();
      }
      break;
    default:
      std::cout << "unhandled button " << event->button << std::endl;
    }
  }

  void handler(std::string value) {
    std::cout << "Value: " << value << std::endl;
  }

  void on_my_populate_popup(Gtk::Menu *menu) {
    for (auto iter : menu->get_children()) {
      menu->remove(*iter);
    }
    std::vector<std::string> values = { "1000%", "500%", "200%", "100%", "50%", "25%", "10%" };
    for (auto value: values) {
      auto item = Gtk::manage(new Gtk::MenuItem(value));
      item->signal_activate().connect(sigc::bind(sigc::mem_fun(*this, &Window::handler), value));
      menu->append(*item);
    }
    menu->show_all();
  }

#endif

  void on_test1() {
    std::cout << "on_test1" << std::endl;
  }

  void on_test2() {
    std::cout << "on_test2" << std::endl;
  }

  void on_test3(int value) {
    std::cout << "on_test3: " << value << std::endl;
  }

  void on_test4(int value) {
    std::cout << "on_test4 int: " << value << std::endl;
  }

  void on_test4(double value) {
    std::cout << "on_test4 double: " << value << std::endl;
  }

  void on_testA(Glib::ustring value) {
    std::cout << "on_testA: " << value << std::endl;
    auto action = lookup_action("testA");

    if (!action) {
      std::cerr << "on_testA: missing action!" << std::endl;
      return;
    }

#if GTK_CHECK_VERSION(4, 0, 0)
    auto saction = std::dynamic_pointer_cast<Gio::SimpleAction>(action);
#else
    auto saction = Glib::RefPtr<Gio::SimpleAction>::cast_dynamic(action);
#endif
    if (!saction) {
      std::cerr << "on_testA: not simple action!" << std::endl;
      return;
    }

    _popover->popdown(); // radio string actions keep popover open!

    // saction->change_state(value);
  }

#if GTK_CHECK_VERSION(4, 0, 0)
  void on_my_pressed(int n_press, double x, double y) {
    std::cout << "on_my_pressed: " << n_press << "  (" << x << "," << y << ")" << std::endl;
    popup((int)x, (int)y);
  }
#else
  bool on_my_button_press_event(GdkEventButton* event_button) {
    std::cout << "on_my_button_press" << std::endl;
    popup(event_button);
    return false;
  }
#endif

};


int main(int argc, char **argv)
{

#if GTK_CHECK_VERSION(4, 0, 0)
  auto application = Gtk::Application::create("org.tavmjong.popup_mm");
  return application->make_window_and_run<Window>(argc, argv);
#else
  auto application = Gtk::Application::create(argc, argv, "org.tavmjong.popup_mm");
  Window window(application);
  return application->run(window);
#endif

}


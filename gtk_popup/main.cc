

#include <iostream>
#include <vector>

#include <gtkmm.h>

class Window: public Gtk::ApplicationWindow
{
public:
#if GTK_CHECK_VERSION(4, 0, 0)
  Window()
#else
  Window(const Glib::RefPtr<Gtk::Application>& app)
#endif
  {
    set_title("Window");
    set_default_size(200, 200);

    add_action( "test1", sigc::mem_fun(*this, &Window::on_test1));
    add_action( "test2", sigc::mem_fun(*this, &Window::on_test2));
    add_action_radio_string("testA", sigc::mem_fun(*this, &Window::on_testA), "A");

    _gmenu = Gio::Menu::create();
    auto menuitem1 = Gio::MenuItem::create("Test1", "win.test1");
    auto menuitem2 = Gio::MenuItem::create("Test2", "win.test2");
    auto menuitemA = Gio::MenuItem::create("TestA", "win.testA('A')");
    auto menuitemB = Gio::MenuItem::create("TestB", "win.testA('B')");
    auto menuitemC = Gio::MenuItem::create("TestC", "win.testA('C')");

    _gmenu->append_item(menuitem1);
    _gmenu->append_item(menuitem2);
    _gmenu->append_item(menuitemA);
    _gmenu->append_item(menuitemB);
    _gmenu->append_item(menuitemC);


#if  GTK_CHECK_VERSION(4, 0, 0)
    _popover = new Gtk::PopoverMenu(_gmenu);
    _popover->set_parent(*this);

    _box_outer.set_homogeneous();
    _box_outer.append(_box_inner);
    _box_outer.append(_entry);

    set_child(_box_outer);

    _entry.set_extra_menu(_gmenu);
#else
    // Need widget for popover!
    _popover = new Gtk::Popover(_box_outer, _gmenu);

    _entry.signal_populate_popup().connect(sigc::mem_fun(*this, &Window::on_my_populate_popup));

    _box_outer.set_homogeneous();
    _box_outer.pack_start(_box_inner);
    _box_outer.pack_start(_entry);

    add(_box_outer);
#endif


#if GTK_CHECK_VERSION(4, 0, 0)
    auto controller = Gtk::GestureClick::create();
    controller->set_button(0);
    controller->signal_pressed().connect(sigc::mem_fun(*this, &Window::on_my_pressed));
    add_controller(controller);
#else
    signal_button_press_event().connect(sigc::mem_fun(*this, &Window::on_my_button_press_event));

    show_all();
#endif
  }

  ~Window() {
    if (_popover) {
      delete _popover;
    }
  }

  Gtk::Box _box_outer;
  Gtk::Box _box_inner;
  Gtk::Entry _entry;
  Glib::RefPtr<Gio::Menu> _gmenu;
  Gtk::Popover* _popover = nullptr;

#if GTK_CHECK_VERSION(4, 0, 0)

  void popup(int x, int y) {

    std::cout << "popup4" << std::endl;
    _popover->set_pointing_to(Gdk::Rectangle(x, y, 1, 1));
    _popover->popup();
  }

#else

  void popup(GdkEventButton *event) {

    std::cout << "popup3" << std::endl;

    switch (event->button) {
    case 1:
      {
        // Popover with Gtk::Menu.
        auto menu = Gtk::manage(new Gtk::Menu(_gmenu));
        menu->attach_to_widget(*this);
        menu->popup_at_pointer((GdkEvent*)event);
      }
      break;
    case 3:
      {
        // Popover with Gtk::Popover
        _popover->set_pointing_to(Gdk::Rectangle(event->x, event->y, 1, 1));
        _popover->popup();
      }
      break;
    default:
      std::cout << "unhandled button " << event->button << std::endl;
    }
  }

  void handler(std::string value) {
    std::cout << "Value: " << value << std::endl;
  }

  void on_my_populate_popup(Gtk::Menu *menu) {
    for (auto iter : menu->get_children()) {
      menu->remove(*iter);
    }
    std::vector<std::string> values = { "1000%", "500%", "200%", "100%", "50%", "25%", "10%" };
    for (auto value: values) {
      auto item = Gtk::manage(new Gtk::MenuItem(value));
      item->signal_activate().connect(sigc::bind(sigc::mem_fun(*this, &Window::handler), value));
      menu->append(*item);
    }
    menu->show_all();
  }

#endif

  void on_test1() {
    std::cout << "on_test1" << std::endl;
  }

  void on_test2() {
    std::cout << "on_test2" << std::endl;
  }

  void on_testA(Glib::ustring value) {
    std::cout << "on_testA: " << value << std::endl;
    auto action = lookup_action("testA");

    if (!action) {
      std::cerr << "on_testA: missing action!" << std::endl;
      return;
    }

    auto saction = Glib::RefPtr<Gio::SimpleAction>::cast_dynamic(action);
    if (!saction) {
      std::cerr << "on_testA: not simple action!" << std::endl;
      return;
    }

    _popover->popdown(); // radio string actions keep popover open!

    saction->change_state(value);
  }

#if GTK_CHECK_VERSION(4, 0, 0)
  void on_my_pressed(int n_press, double x, double y) {
    std::cout << "on_my_pressed: " << n_press << "  (" << x << "," << y << ")" << std::endl;
    popup((int)x, (int)y);
  }
#else
  bool on_my_button_press_event(GdkEventButton* event_button) {
    std::cout << "on_my_button_press" << std::endl;
    popup(event_button);
    return false;
  }
#endif

};


int main(int argc, char **argv)
{

#if GTK_CHECK_VERSION(4, 0, 0)
  auto application = Gtk::Application::create("org.tavmjong.popup_mm");
  return application->make_window_and_run<Window>(argc, argv);
#else
  auto application = Gtk::Application::create(argc, argv, "org.tavmjong.popup_mm");
  Window window(application);
  return application->run(window);
#endif

}


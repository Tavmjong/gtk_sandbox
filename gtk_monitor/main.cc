
//#define COLORD

#include <iostream>
#include <iomanip>
#include <gtkmm.h>

#ifdef COLORD
#include <colord.h>
#endif

class Window: public Gtk::Window
{
public:
#if GTK_CHECK_VERSION(4, 0, 0)
  Window()
#else
  Window(const Glib::RefPtr<Gtk::Application>& app)
#endif
  {
    set_title("Window");
    set_default_size(200, 200);

    signal_realize().connect(sigc::mem_fun(*this, &Window::on_my_realize));
    signal_map().connect(    sigc::mem_fun(*this, &Window::on_my_map    ));

    auto display = get_display(); // gdk display
    display->beep();
    // Display name will be of the form :0 for X11 and wayland-0 for Wayland.
    std::cout << "Display name: " << display->get_name() << std::endl;
    // char* display_env = getenv("DISPLAY");
    // std::cout << "Display envvar: " << (display_env ? display_env : "unknown") << std::endl;
#if GTK_CHECK_VERSION(4, 0, 0)
    std::cout << "Display is rgba: " << (display->is_rgba() ? "true" : "false") << std::endl;
#endif

#ifdef COLORD
    CdClient* client = cd_client_new();
    if(!client || !cd_client_connect_sync(client, NULL, NULL)) {
      std::cout << "Failed to find colord client!" << std::endl;
    } else {
      GPtrArray* devices = cd_client_get_devices_sync(client, NULL, NULL);
      std::cout << "colord: Number of devices: " << devices->len << std::endl;
      for (int i = 0; i < devices->len; ++i) {
        CdDevice* device = (CdDevice*)g_ptr_array_index(devices, i);
        if (device && cd_device_connect_sync(device, NULL, NULL)) {
          std::cout << "  Device: " << cd_device_get_id(device)
                    << "  Model: "  << cd_device_get_model(device)
                    << "  Vendor: " << cd_device_get_vendor(device)
                    << std::endl;
          CdProfile* profile = cd_device_get_default_profile(device);
          if (profile && cd_profile_connect_sync(profile, NULL, NULL)) {
            const gchar* title = cd_profile_get_title(profile);
            CdColorspace colorspace = cd_profile_get_colorspace(profile);
            std::cout << "  Profile: " << (title ? title : "no title")
                      << "  Colorspace: ";
            switch (colorspace) {
            case CD_COLORSPACE_UNKNOWN:  std::cout << "Unknown"; break;
            case CD_COLORSPACE_XYZ:      std::cout << "XYZ"    ; break;
            case CD_COLORSPACE_LAB:	 std::cout << "LAB"    ; break;
            case CD_COLORSPACE_LUV:	 std::cout << "LUV"    ; break;
            case CD_COLORSPACE_YCBCR:	 std::cout << "YCBRC"  ; break;
            case CD_COLORSPACE_YXY:	 std::cout << "YXY"    ; break;
            case CD_COLORSPACE_RGB:	 std::cout << "RGB"    ; break;
            case CD_COLORSPACE_GRAY:	 std::cout << "GRAY"   ; break;
            case CD_COLORSPACE_HSV:      std::cout << "HSV"    ; break;
            case CD_COLORSPACE_CMYK:	 std::cout << "CMYK"   ; break;
            case CD_COLORSPACE_CMY:	 std::cout << "CMY"    ; break;
            default: std::cout << "Unhandled colorspace enum value";
            }
            std::cout << std::endl;
          } else {
            std::cout << "Failed to get profile!" << std::endl;
          }
        } else {
          std::cout << "  Failed to get device!" << std::endl;
        }
      }
      //      CdDevice* cd_device = cd_client_find_device_by_property(cd_client, CD_DEVICE_METADATA_XRANDR_NAME,
      //                                                        monitor_name, NULL, NULL);
      //      cd_client_connect(cd_client, NULL, my_async_callback, nullptr);
    }
#endif

    auto button = Gtk::manage(new Gtk::Button("Update"));
    button->signal_clicked().connect(sigc::mem_fun(*this, &Window::on_my_clicked));

#if GTK_CHECK_VERSION(4, 0, 0)
    set_child(*button);
    show();

    auto monitors = display->get_monitors();
    std::cout << "\nNumber of monitors: " << monitors->get_n_items() << std::endl;
    for (int i = 0; i < monitors->get_n_items(); ++i) {
      auto object = monitors->get_object(i);
      auto monitor = std::dynamic_pointer_cast<Gdk::Monitor>(object);
      dump_monitor(monitor);
    }
#else
    add(*button);
    show_all();

    std::cout << "\nNumber of monitors: " << display->get_n_monitors() << std::endl;
    for (int i = 0; i < display->get_n_monitors(); ++i) {
      auto monitor = display->get_monitor(i); // Shoud be get_monitors() per documentation
      dump_monitor(monitor);
    }
#endif

    auto seats = display->list_seats();
    std::cout << "\nNumber of seats: " << seats.size() << std::endl;
    for (auto seat : seats) {
#if GTK_CHECK_VERSION(4, 0, 0)
      std::cout << "  Seat devices:" << std::endl;
      auto devices = seat->get_devices(Gdk::Seat::Capabilities::ALL);
      for (auto device : devices) {
        std::cout << "    Device: " << device->get_name() << std::endl;
        auto device_tool = device->get_device_tool();
        std::cout << "      Tool type: " << (uint)device_tool->get_type() << std::endl;
        // std::cout << "        unknown: " << (uint)Gdk::DeviceTool::Type::UNKNOWN << std::endl;
        // std::cout << "        lens   : " << (uint)Gdk::DeviceTool::Type::LENS << std::endl;
      }
      auto tools = seat->get_tools();
      std::cout << "  Seat tools: " << tools.size() << std::endl;
      for (auto tool : tools) {
        auto tool_type = tool->get_type();
        std::cout << "    Tool type: " << tool_type << std::endl;
      }
#endif
    }

    std::cout << "Initial monitor:" << std::endl;
    dump_monitor_signal();
  }

  void on_my_realize() {
    std::cout << "on_my_realize()" << std::endl;
    dump_monitor_signal();
  }

  void on_my_map() {
    std::cout << "on_my_map()" << std::endl;
    dump_monitor_signal();
  }

  void on_my_clicked() {
    std::cout << "on_my_clicked: Clicked" << std::endl;
    dump_monitor_signal();
  }

#if GTK_CHECK_VERSION(4, 0, 0)
  void on_my_enter_monitor(const Glib::RefPtr<Gdk::Monitor>& monitor) {
    std::cout << "on_my_enter_monitor" << std::endl;
    dump_monitor(monitor);
  }
#else
  void on_my_screen_changed(const Glib::RefPtr<Gdk::Screen>& previous_screen) {
    // Should never be called as 3.20 has only one screen!
    std::cout << "on_my_screen_changed" << std::endl;
  }
#endif

  void dump_monitor_signal() {
    auto display = get_display();

#if GTK_CHECK_VERSION(4, 0, 0)
    auto surface = get_surface();
    surface->signal_enter_monitor().connect(sigc::mem_fun(*this, &Window::on_my_enter_monitor));

    auto the_monitor = display->get_monitor_at_surface(surface);
#else
    signal_screen_changed().connect(sigc::mem_fun(*this, &Window::on_my_screen_changed)); // Shouldn't work (only one screen as of 3.20).

    auto the_monitor = display->get_monitor_at_window(get_window()); // Uses gdk window.
#endif
    dump_monitor(the_monitor);
  }

  void dump_monitor(Glib::RefPtr<Gdk::Monitor> monitor) {
    std::cout << "  Monitor: "
              << "  Manufacturer: " << std::setw(5)  << monitor->get_manufacturer()
              << "  Model: "        << std::setw(15) << monitor->get_model()
              << "  Width: "        << monitor->get_width_mm() << "mm"
              << "  Height: "       << monitor->get_height_mm() << "mm"
              << "  Refresh rate: " << monitor->get_refresh_rate() << "mHz"
#if GTK_CHECK_VERSION(4, 0, 0)
              << "  Connector: "    << monitor->get_connector()
#endif
#if GTK_CHECK_VERSION(4, 10, 0)
              << "  Description: "  << monitor->get_description()
#endif
              << std::endl;
  }
};


int main(int argc, char **argv)
{

#if GTK_CHECK_VERSION(4, 0, 0)
  auto application = Gtk::Application::create("org.tavmjong.monitors_mm");
  return application->make_window_and_run<Window>(argc, argv);
#else
  auto application = Gtk::Application::create(argc, argv, "org.tavmjong.montiors_mm");
  Window window(application);
  return application->run(window);
#endif

}


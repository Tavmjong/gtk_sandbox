
#include <gtk/gtk.h>

void
dump_monitor(GdkMonitor* monitor)
{
  const gchar* manufacturer = gdk_monitor_get_manufacturer(monitor);
  const gchar* model        = gdk_monitor_get_model       (monitor);
  const gint   width        = gdk_monitor_get_width_mm    (monitor);
  const gint   height       = gdk_monitor_get_height_mm   (monitor);
#if GTK_CHECK_VERSION(4, 0, 0)
  const gchar* connector    = gdk_monitor_get_connector   (monitor);
#endif

  printf ("  Monitor: Manufacturer: %15s  Model: %15s  Width: %d  Height: %d\n",
          manufacturer, model, width, height);
}

void
dump_monitor_signal (GtkWidget* widget)
{
  if (widget) {
    // printf("%s\n", gtk_widget_get_name(widget));
    GdkDisplay* display = gtk_widget_get_display(widget);
#if GTK_CHECK_VERSION(4, 0, 0)
    GdkSurface* surface = gtk_native_get_surface(GTK_NATIVE(widget));
    GdkMonitor* monitor = gdk_display_get_monitor_at_surface(display, surface);
#else
    GdkWindow* window = gtk_widget_get_window(widget);
    GdkMonitor* monitor = gdk_display_get_monitor_at_window(display, window);
#endif
    dump_monitor(monitor);
  } else {
    printf ("dump_monitor_signal: no widget!\n");
  }
}

static void
on_my_realize (GtkWidget* widget, gpointer user_data)
{
  printf ("on_my_realize\n");
  printf ("is_visible: %s\n", gtk_widget_is_visible(widget) ? "true" : "false");
  dump_monitor_signal(widget);
}

static void
on_my_map (GtkWidget* widget, gpointer user_data)
{
  printf ("on_my_map\n");
  printf ("is_visible: %s\n", gtk_widget_is_visible(widget) ? "true" : "false");
  dump_monitor_signal(widget);
}

#if GTK_CHECK_VERSION(4, 0, 0)
static void
on_my_enter_monitor(GdkSurface* surface, GdkMonitor* monitor, gpointer user_data)
{
  printf("on_my_enter_monitor (gtk4 only)\n");
  dump_monitor(monitor);
}
#endif

static void
on_my_clicked (GtkWidget* widget, gpointer user_data)
{
  printf("on_my_clicked\n");
  GtkWidget* window = GTK_WIDGET(user_data);
  dump_monitor_signal(window);
}

static void
activate (GtkApplication* app, gpointer user_data)
{
  GtkWidget *window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Window");
  gtk_window_set_default_size (GTK_WINDOW (window), 200, 200);

  g_signal_connect (window, "realize", G_CALLBACK (on_my_realize), NULL);
  g_signal_connect (window, "map",     G_CALLBACK (on_my_map),     NULL);

  GtkWidget* button = gtk_button_new_with_label("Update");
  g_signal_connect (button, "clicked", G_CALLBACK (on_my_clicked), window);

#if GTK_CHECK_VERSION(4, 0, 0)
  gtk_window_set_child(GTK_WINDOW(window), button);
  gtk_widget_show (window);
#else
  gtk_container_add(GTK_CONTAINER(window), button);
  gtk_widget_show_all (window);
#endif

#if GTK_CHECK_VERSION(4, 0, 0)
  GdkSurface* surface = gtk_native_get_surface(GTK_NATIVE(window));
  if (!surface) {
    printf ("FAILED TO GET SURFACE\n");
  }
  g_signal_connect (surface, "enter-monitor", G_CALLBACK (on_my_enter_monitor), NULL);
#endif

  GdkDisplay* display = gtk_widget_get_display(window);
  printf("Display: %s\n", gdk_display_get_name(display));

  printf ("Monitors:\n");
#if GTK_CHECK_VERSION(4, 0, 0)
  GListModel* monitors = gdk_display_get_monitors(display);
  gint n_monitors = g_list_model_get_n_items(monitors);
  for (gint i = 0; i < n_monitors; ++i) {
    GdkMonitor* monitor = g_list_model_get_item(monitors, i);
    dump_monitor(monitor);
  }
#else
  gint n_monitors = gdk_display_get_n_monitors(display);
  for (gint i = 0; i < n_monitors; ++i) {
    //printf ("%d ", i);
    GdkMonitor* monitor = gdk_display_get_monitor(display, i);
    dump_monitor(monitor);
  }
#endif
}


int main(int argc, char *argv[])
{

  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.tavmjong.monitors", G_APPLICATION_DEFAULT_FLAGS);

  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  //  g_signal_connect (app, "startup",  G_CALLBACK (startup),  NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}

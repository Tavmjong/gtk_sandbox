#ifndef TOOLITEM_MENU_H
#define TOOLITEM_MENU_H

#include <giomm/application.h>
#include <gtkmm.h>
#include <iostream>
#include <sstream>

#include "spinbutton-action.h"

// A Gtk::ToolItem with the ability to set the menu via a property.
class ToolItemMenu : public Gtk::ToolItem
{
 public:
  ToolItemMenu();
  ToolItemMenu(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refUI);
  virtual ~ToolItemMenu() {};

  Glib::PropertyProxy<Glib::ustring> property_menuitem_name() { return prop_menuitem_name.get_proxy(); }

 private:
  void on_realize() override;
  bool on_create_menu_proxy() override;

  // Properties
  Glib::Property<Glib::ustring> prop_menuitem_name;

  Gtk::MenuItem* menuitem_ui = nullptr;
};

// Dummy constructor to register type.
ToolItemMenu::ToolItemMenu()
  : Gtk::ToolItem()
  , Glib::ObjectBase("ToolItemMenu")
  , prop_menuitem_name(*this, "menuitem-name")
{
  std::cout << "ToolItemMenu::ToolItemMenu(): default constructor" << std::endl;
}

ToolItemMenu::ToolItemMenu(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refUI)
  : Gtk::ToolItem(cobject)
  , Glib::ObjectBase("ToolItemMenu")
  , prop_menuitem_name(*this, "menuitem-name")
{
  std::cout << "ToolItemMenu::ToolItemMenu(): UI constructor: " << prop_menuitem_name << std::endl;

  // Get a reference to a menu defined in the .ui file.
  Glib::ustring menuitem_name = prop_menuitem_name.get_value();

  if (menuitem_name != "") {
    refUI->get_widget(menuitem_name, menuitem_ui);

    if (menuitem_ui) {
      std::cout << "Found MenuItem from .ui file" << std::endl;
    }
  }
}

void ToolItemMenu::on_realize()
{
  std::cout << "ToolItemMenu::on_realize" << std::endl;
  Gtk::ToolItem::on_realize();

}

bool ToolItemMenu::on_create_menu_proxy()
{
  std::cout << "ToolItemMenu::on_create_menu_proxy(): " << prop_menuitem_name << std::endl;

  Gtk::MenuItem* menuitem = nullptr;

  // Get a menu defined in a child widget (only some widgets).
  if (menuitem_ui) {
    menuitem = menuitem_ui;
  } else {
    auto child = get_child();
    if (child) {
      auto spinbutton = dynamic_cast<SpinButtonAction*>(child);
      if (spinbutton) {
        std::cout << "Got spin button!" << std::endl;
        menuitem = spinbutton->get_menu();
      }
    }
  }

  set_proxy_menu_item("MyMenuItemId", *menuitem);

  return true;
}

#endif // TOOLITEM_MENU_H


#ifndef SIMPLE_WIDGETS_H
#define SIMPLE_WIDGETS_H

#include <gtkmm.h>
#include <iostream>

class SimpleButton : public Gtk::Button
{
 public:
  SimpleButton();
  SimpleButton(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refUI);
  virtual ~SimpleButton() {};

  void on_clicked() override;
};

SimpleButton::SimpleButton()
: Gtk::Button()
{
  std::cout << "SimpleButton::SimpleButton(): default constructor" << std::endl;
}

SimpleButton::SimpleButton(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refUI)
  : Gtk::Button(cobject)
    //  , Glib::ObjectBase("SimpleButton")
{
  std::cout << "SimpleButton::SimpleButton(): UI constructor" << std::endl;
}

void SimpleButton::on_clicked()
{
  std::cout << "SimpleButton::on_clicked: clicked!" << std::endl;
}

// --------------------------------------

void on_ustring_changed()
{
  std::cout << "  on_ustring_changed!" << std::endl;
}

class SimpleButton2 : public Gtk::Button
{
 public:
  SimpleButton2();
  SimpleButton2(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refUI);
  virtual ~SimpleButton2() {};

  Glib::PropertyProxy<Glib::ustring> property_ustring() { return prop_ustring.get_proxy(); }

  void on_clicked() override;

 private:
  Glib::Property<Glib::ustring> prop_ustring;
};

// For creating dummy object (needed to register type).
SimpleButton2::SimpleButton2()
  : Gtk::Button()
  , Glib::ObjectBase("SimpleButton2")
  , prop_ustring(*this, "simplebutton-ustring")
{
  std::cout << "SimpleButton2::SimpleButton2(): default constructor" << std::endl;
}

SimpleButton2::SimpleButton2(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refUI)
  : Gtk::Button(cobject)
  , Glib::ObjectBase("SimpleButton2")
  , prop_ustring(*this, "simplebutton-ustring")
{
  std::cout << "SimpleButton2::SimpleButton2(): UI constructor" << std::endl;

  property_ustring().signal_changed().connect(sigc::ptr_fun(&on_ustring_changed));
}

void SimpleButton2::on_clicked()
{
  std::cout << "SimpleButton2::on_clicked: clicked!" << std::endl;
}

#endif // SIMPLE_WIDGETS_H


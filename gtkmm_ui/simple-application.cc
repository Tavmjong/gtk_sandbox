

#include "simple-application.h"

#include <glibmm/i18n.h>
#include <gtk/gtk.h>

#include <fstream>
#include <iostream>
#include <iomanip>

#include "simple-widgets.h"
#include "spinbutton-action.h"

#if !GTK_CHECK_VERSION(4, 0, 0)
#include "toolitem-menu.h"
#endif

void on_value_changed(Gtk::SpinButton* spinbutton) {
  std::cout << "on_value_changed: " << spinbutton->get_value() << std::endl;
}

void on_changed(Gtk::ComboBox* combobox) {
  std::cout << "on_changed1: " << combobox->get_active_id() << std::endl;
}

class ColorColumns : public Gtk::TreeModel::ColumnRecord {
public:
  ColorColumns() {
    add (col_color);
    add (col_pixbuf);
  }
  Gtk::TreeModelColumn<Glib::ustring> col_color;
  Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf>> col_pixbuf;
};

// Convenience class and function to create a widget containing a color and label.
class MyDrawingArea : public Gtk::DrawingArea {
public:
  MyDrawingArea(const Glib::ustring& color)
    : Gtk::DrawingArea()
    , _color(color)
  {
    std::cout << "MyDrawinArea::MyDrawingArea: " << _color << std::endl;
#if GTK_CHECK_VERSION(4, 0, 0)
    set_draw_func(sigc::mem_fun(*this, &MyDrawingArea::on_draw));
#endif
  }
  virtual ~MyDrawingArea() {}

#if GTK_CHECK_VERSION(4, 0, 0)
  void on_draw(const Cairo::RefPtr<Cairo::Context>& cr, int, int) {
#else
  bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override {
#endif
    std::cout << "MyDrawingArea::on_draw: " << _color << std::endl;
    Gdk::RGBA rgba(_color);
    Gdk::Cairo::set_source_rgba(cr, rgba);
    cr->paint();

#if GTK_CHECK_VERSION(4, 0, 0)
#else
    return true;
#endif
  }

private:
  Glib::ustring _color;
};

Gtk::Button* create_dialog_button(const Glib::ustring& color) {
  auto drawing_area = Gtk::manage(new MyDrawingArea(color));
  drawing_area->set_size_request(20, 10);
  auto label = Gtk::manage(new Gtk::Label(color));
  auto grid = Gtk::manage(new Gtk::Grid());
  auto button = Gtk::manage(new Gtk::Button());
  grid->attach(*drawing_area, 0, 0, 1, 1);
  grid->attach(*label, 2, 0, 1, 1);
#if GTK_CHECK_VERSION(4, 0, 0)
  button->set_child(*grid);
#else
  button->add(*grid);
  button->show_all();
#endif
  return button;
}


// Convenience function to add cell renderers to Gtk::ListStore.
void list_store_append_color(Glib::RefPtr<Gtk::ListStore> list_store, Glib::ustring const &color) {
  auto row = *(list_store)->append();
  ColorColumns columns_gtk;
  row[columns_gtk.col_color]  = color;

#if GTK_CHECK_VERSION(4, 0, 0)
  auto surface = Cairo::ImageSurface::create(Cairo::Surface::Format::ARGB32, 10, 20);
#else
  auto surface = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32, 10, 20);
#endif
  auto cr = Cairo::Context::create(surface);
  Gdk::Cairo::set_source_rgba(cr, Gdk::RGBA(color));
  cr->paint();
  auto pixbuf = Gdk::Pixbuf::create(surface, 0, 0, 10, 20);
  row[columns_gtk.col_pixbuf] = pixbuf;
}

void on_treeview_row_activated(const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column, Glib::RefPtr<Gtk::ListStore> model) {
  auto iter = model->get_iter(path);
  if (iter) {
    Gtk::TreeModel::Row row = *iter;
    ColorColumns columns_gtk;
    std::cout << "on_treeview_row_activated: " << row[columns_gtk.col_color] << std::endl;
  }
}

#if GTK_CHECK_VERSION(4, 0, 0)
// Test Gio::ListStore

namespace {
  class MyColor : public Glib::Object  {

  public:
    static Glib::RefPtr<MyColor> create(const Glib::ustring& color)
    {
      return Glib::make_refptr_for_instance<MyColor>(new MyColor(color));
    }

    Glib::ustring get_color() const { return m_color; }

  protected:
    MyColor(const Glib::ustring& color)
      : Glib::ObjectBase(typeid(MyColor))
      , m_color(color)
    {
    }

  private:
    Glib::ustring m_color;
  };
} // anonymouse namespace

  ///zzzz
void setup_listitem_cb(const Glib::RefPtr<Gtk::ListItem>& list_item) {
  std::cout << "setup_listitem_cb" << std::endl;
  auto box   = Gtk::make_managed<Gtk::Box>(Gtk::Orientation::HORIZONTAL, 12);

  auto label = Gtk::make_managed<Gtk::Label>();
  label->set_halign(Gtk::Align::START);
  label->set_hexpand(true);
  box->append(*label);

  auto drawing_area = Gtk::make_managed<Gtk::DrawingArea>();
  drawing_area->set_content_width(10);
  drawing_area->set_content_height(10);
  box->append(*drawing_area);

  list_item->set_child(*box);
}

void on_drawing_area_draw(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height, Glib::ustring const &color) {
  std::cout << "on_drawing_area_draw: " << color << std::endl;
  Gdk::Cairo::set_source_rgba(cr, Gdk::RGBA(color));
  cr->paint();
}

void bind_listitem_cb(const Glib::RefPtr<Gtk::ListItem>& list_item) {
  auto item = list_item->get_item();
  if (auto column = std::dynamic_pointer_cast<MyColor>(item)) {

    if (auto label = dynamic_cast<Gtk::Label*>(list_item->get_child()->get_first_child())) {
      label->set_text(column->get_color());

      if (auto drawing_area = dynamic_cast<Gtk::DrawingArea*>(label->get_next_sibling())) {
        drawing_area->set_draw_func(sigc::bind(sigc::ptr_fun(on_drawing_area_draw), column->get_color()));
        std::cout << "bind_listitem_cb: " << column->get_color() << std::endl;
      } else {
        std::cout << "bind_listitem_cb: second child not DrawingArea!" << std::endl;
      }
    } else {
      std::cout << "bind_listitem_cb: first child not Label!" << std::endl;
    }

  } else {
    std::cout << "bind_listitem_cb: wrong type!" << std::endl;
  }
}

void activate_cb(Gtk::ListView* list, guint position) {
  if (auto model = std::dynamic_pointer_cast<Gio::ListModel>(list->get_model())) {
    auto item = model->get_object(position);
    if (auto column = std::dynamic_pointer_cast<MyColor>(item)) {
      std::cout << "activate_cb: " << column->get_color() << std::endl;
    } else {
      std::cout << "actibate:cb: wrong type!" << std::endl;
    }
  }
}

void activate_grid_cb(Gtk::GridView* grid_view, guint position) {
  if (auto model = std::dynamic_pointer_cast<Gio::ListModel>(grid_view->get_model())) {
    auto item = model->get_object(position);
    if (auto column = std::dynamic_pointer_cast<MyColor>(item)) {
      std::cout << "activate_grid_cb: " << column->get_color() << std::endl;
    } else {
      std::cout << "actibate_grid_cb: wrong type!" << std::endl;
    }
  }
}

#endif


#if GTK_CHECK_VERSION(4, 0, 0)
void dump_menu (std::shared_ptr<Gio::MenuModel> menu) {
#else
void dump_menu (Glib::RefPtr<Gio::MenuModel> menu) {
#endif

  static int indent = 0;
  ++indent;

  int n_items = menu->get_n_items();
  for (int i = 0; i < indent; ++i) std::cout << "  ";
  std::cout << "dump_menu: " << n_items << std::endl;

  for (int i = 0; i < n_items; ++i) {

    std::cout << std::endl;
    auto attribute_iter = menu->iterate_item_attributes(i);
    while (attribute_iter->next()) {
      for (int i = 0; i < indent + 1; ++i) std::cout << "  ";
      std::cout << attribute_iter->get_name() << ": " << attribute_iter->get_value().print() << std::endl;
    }

    // Recurse
    auto link_iter = menu->iterate_item_links(i);
    while (link_iter->next()) {
      for (int i = 0; i < indent + 1; ++i) std::cout << "  ";
      std::cout << "Link: " << link_iter->get_name() << std::endl;
      dump_menu (link_iter->get_value());
    }
  }

  --indent;
}

enum class UseIcons {
  always,
  as_requested,
  never
};

/*
 * Disable all or some menu icons.
 *
 * This is quite nasty:
 *
 * We must disable icons in the Gio::Menu as there is no way to pass
 * the needed information to the children of Gtk::PopoverMenu and no
 * way to set visibility via CSS.
 *
 * MenuItems are immutable and not copyable so you have to recreate
 * the menu tree. The format for accessing MenuItem data is not the
 * same as what you need to create a new MenuItem.
 */
#if GTK_CHECK_VERSION(4, 0, 0)
void copy_menu (std::shared_ptr<Gio::MenuModel> menu, std::shared_ptr<Gio::Menu> menu_copy,
                UseIcons useIcons = UseIcons::always) {
#else
void copy_menu (Glib::RefPtr<Gio::MenuModel> menu, Glib::RefPtr<Gio::Menu> menu_copy,
                UseIcons useIcons = UseIcons::always) {
#endif

  static int indent = 0;
  ++indent;

  int n_items = menu->get_n_items();
  for (int i = 0; i < indent; ++i) std::cout << "  ";
  std::cout << "copy_menu: " << n_items << "  use icons: " << (int)useIcons << std::endl;

  for (int i = 0; i < n_items; ++i) {

    Glib::ustring label;
    Glib::ustring action;
    Glib::ustring target;
    Glib::VariantBase icon;
    Glib::ustring use_icon;
    std::map<Glib::ustring, Glib::VariantBase> attributes;

    auto attribute_iter = menu->iterate_item_attributes(i);
    std::cout << std::endl;
    while (attribute_iter->next()) {
      for (int i = 0; i < indent + 1; ++i) std::cout << "  ";
      std::cout << attribute_iter->get_name() << ": " << attribute_iter->get_value().print() << std::endl;

      // Attributes we need to create MenuItem or set icon.
      if          (attribute_iter->get_name() == "label") {
        label    = attribute_iter->get_value().print();
        label.erase(0, 1);
        label.erase(label.size()-1, 1);
      } else if   (attribute_iter->get_name() == "action") {
        action  = attribute_iter->get_value().print();
        action.erase(0, 1);
        action.erase(action.size()-1, 1);
      } else if   (attribute_iter->get_name() == "target") {
        target  = attribute_iter->get_value().print();
      } else if   (attribute_iter->get_name() == "icon") {
        icon     = attribute_iter->get_value();
      } else if (attribute_iter->get_name() == "use-icon") {
        use_icon =  attribute_iter->get_value().print();
      } else {
        // All the remaining attributes.
        attributes[attribute_iter->get_name()] = attribute_iter->get_value();
      }
    }

    Glib::ustring detailed_action = action;
    if (target.size() > 0) {
      detailed_action += "(" + target + ")";
    }

    auto menu_item = Gio::MenuItem::create(label, detailed_action);
    if (icon &&
        (useIcons == UseIcons::always ||
         (useIcons == UseIcons::as_requested && use_icon.size() > 0))) {
      menu_item->set_attribute_value("icon", icon);
    }

    // Add remaining attributes
    for (auto const& [key, value] : attributes) {
      menu_item->set_attribute_value(key, value);
    }

    // Add submenus
    auto link_iter = menu->iterate_item_links(i);
    while (link_iter->next()) {
      auto submenu = Gio::Menu::create();
      if (link_iter->get_name() == "submenu") {
        menu_item->set_submenu(submenu);
      } else if (link_iter->get_name() == "section") {
        menu_item->set_section(submenu);
      } else {
        std::cout << "  Unknown link type: " << link_iter->get_name() << std::endl;
      }
      copy_menu (link_iter->get_value(), submenu, useIcons);
    }

    menu_copy->append_item(menu_item);
  }

  --indent;
}

#if GTK_CHECK_VERSION(4, 0, 0)

void
find_all_model_buttons(std::vector<Gtk::Widget*>& model_buttons, Gtk::Widget* widget)
{
  if (widget->get_name() == "GtkModelButton") {
    model_buttons.push_back(widget);
  }

  // Do we need this?
  auto box = dynamic_cast<Gtk::Box*>(widget);
  if (box) {
    box->set_spacing(0);
  }

  for (auto child = widget->get_first_child(); child != nullptr; child = child->get_next_sibling()) {
    find_all_model_buttons(model_buttons, child);
  }
}

void
SimpleApplication::fix_menuicons(Gtk::PopoverMenu *menu)
{
  std::cout << "fix_menuicons - GTK4" << std::endl;

  static Glib::RefPtr<Gtk::CssProvider> provider;
  if (!provider) {
    provider = Gtk::CssProvider::create();
    auto const display = Gdk::Display::get_default();
    Gtk::StyleContext::add_provider_for_display(display, provider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
  }

  if (menu) {
    std::vector<Gtk::Widget*> model_buttons;
    find_all_model_buttons(model_buttons, menu);
    std::cout << "   Found: " << model_buttons.size() << " Gtk::ModelButtons!" << std::endl;

    bool did_css = false;
    for (auto button : model_buttons) {

      auto allocation_button = button->get_allocation();
      std::cout << std::endl;
      std::cout << "Allocation " << std::setw(20) << "button "
                << "  x: "       << std::setw(5) << allocation_button.get_x()
                << "  y: "       << std::setw(5) << allocation_button.get_y()
                << "  width: "   << std::setw(5) << allocation_button.get_width()
                << "  height: "  << std::setw(5) << allocation_button.get_height()
                << "  Width: "   << std::setw(5) << button->get_width()
                << std::endl;

      // Count children, find image (icon).
      int i = 0;
      int total_width1 = 0;
      int total_width2 = 0;
      Gtk::Image* image = nullptr;
      for (auto child = button->get_first_child(); child != nullptr; child = child->get_next_sibling()) {
        ++i;
        auto image2 = dynamic_cast<Gtk::Image*>(child);
        if (image2) {
          image2->set_visible(true);
          image = image2;
        }

        auto allocation = child->get_allocation();
        std::cout << "Allocation " << std::setw(20) << child->get_name()
                  << "  x: "       << std::setw(5) << allocation.get_x()
                  << "  y: "       << std::setw(5) << allocation.get_y()
                  << "  width: "   << std::setw(5) << allocation.get_width()
                  << "  height: "  << std::setw(5) << allocation.get_height()
                  << "  Width: "   << std::setw(5) << child->get_width()
                  << std::endl;
        total_width1 += allocation.get_width();
        total_width2 += child->get_width();
      }
      std::cout << "  allocation total: " << total_width1
                << "  width total: " << total_width2 << std::endl;

      if (i == 4 && image && !did_css) {
        // Inside button, we have Gtk::Box + Gtk::Image + Gtk::Label + Gtk::Label.
        auto allocation_button = button->get_allocation();
        auto allocation_image = image->get_allocation();

        int shift = -allocation_image.get_x();
        if (button->get_direction() == Gtk::TextDirection::RTL) {
          shift += (allocation_button.get_width() - allocation_image.get_width());
        }

        static int current_shift = 0;
        if (std::abs(current_shift - shift) > 2) {
          // Only do this once per menu, and only if there is a large change.
          current_shift = shift;
          std::string css_str;
          if (button->get_direction() == Gtk::TextDirection::RTL) {
            css_str = "modelbutton image {margin-right:" + std::to_string(shift) + "px;}";
          } else {
            css_str = "modelbutton image {margin-left:" + std::to_string(shift) + "px;}";
          }
          std::cout << css_str << std::endl;
          provider->load_from_data(css_str);
          did_css = true;
        }
      }
    }
  }
}

void
SimpleApplication::fix_menuicons_recurse(Gtk::Widget *widget)
{
  std::cout << "fix_menuicons_recurse - GTK4" << std::endl;
  auto menu = dynamic_cast<Gtk::PopoverMenu*>(widget);
  if (menu) {
    std::cout << "  Found Gtk::PopoverMenu!!!" << std::endl;
    // Connect signal
    menu->signal_map().connect(sigc::bind(sigc::mem_fun(*this, &SimpleApplication::fix_menuicons), menu));
  } else {
    // Note: Gtk::PopoverMenuBar ->
    //         Gtk::PopoverMenuBarItem (not publically exposed!) ->
    //           Gtk::PopoverMenu
    for (auto child = widget->get_first_child(); child != nullptr; child = child->get_next_sibling()) {
      fix_menuicons_recurse(child);
    }
  }
}

#else

// Code from Inkscape to shift icons. Added setting icon visibility.
void
SimpleApplication::fix_menuicons(Gtk::MenuShell *menu)
{
  std::cout << "fix_menuicons" << std::endl;

  static Glib::RefPtr<Gtk::CssProvider> provider;
  if (!provider) {
    provider = Gtk::CssProvider::create();
    auto const screen = Gdk::Screen::get_default();
    Gtk::StyleContext::add_provider_for_screen(screen, provider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
  }

  // Calculate required shift. We need an example!
  // Search for Gtk::MenuItem -> Gtk::Box -> Gtk::Image
  for (auto child : menu->get_children()) {

    auto menuitem = dynamic_cast<Gtk::MenuItem *>(child);
    if (menuitem) {

      auto box = dynamic_cast<Gtk::Box *>(menuitem->get_child());
      if (box) {

        box->set_spacing(0); // Match ImageMenuItem

        auto children = box->get_children();
        if (children.size() == 2) { // Image + Label

          auto image = dynamic_cast<Gtk::Image *>(box->get_children()[0]);
          if (image) {

            image->set_visible(true);

            // OK, we have an example, do calculation.
            auto allocation_menuitem = menuitem->get_allocation();
            auto allocation_image = image->get_allocation();

            int shift = -allocation_image.get_x();
            if (menuitem->get_direction() == Gtk::TEXT_DIR_RTL) {
              shift += (allocation_menuitem.get_width() - allocation_image.get_width());
            }

            static int current_shift = 0;
            if (std::abs(current_shift - shift) > 2) {
              // Only do this once per menu, and only if there is a large change.
              current_shift = shift;
              std::string css_str;
              if (menuitem->get_direction() == Gtk::TEXT_DIR_RTL) {
                css_str = "menuitem box image {margin-right:" + std::to_string(shift) + "px;}";
              } else {
                css_str = "menuitem box image {margin-left:" + std::to_string(shift) + "px;}";
              }
              provider->load_from_data(css_str);
            }
          }
        }
      }
    }
  }
  // If we get here, it means there were no examples... but we don't care as there are no icons to shift anyway.
}

void
SimpleApplication::fix_menuicons_recurse(Gtk::MenuShell *menu)
{
  if (menu) {
    // Connect signal
    menu->signal_map().connect(sigc::bind(sigc::mem_fun(*this, &SimpleApplication::fix_menuicons), menu));
  }

  // Look for descendent menus.
  auto children = menu->get_children(); // Should be Gtk::MenuItem's
  for (auto child : children) {
    auto menuitem = dynamic_cast<Gtk::MenuItem *>(child);
    if (menuitem) {
      auto submenu = menuitem->get_submenu();
      if (submenu) {
        fix_menuicons_recurse(submenu);
      }
    }
  }
}

#endif

// ------------------------

SimpleApplication::SimpleApplication()
#if GTK_CHECK_VERSION(4, 0, 0)
  : Gtk::Application("org.tavmjong.simpleapplication_4")
#else
  : Gtk::Application("org.tavmjong.simpleapplication_3",
                     Gio::APPLICATION_HANDLES_OPEN |
                     Gio::APPLICATION_CAN_OVERRIDE_APP_ID)
#endif
{
  Glib::set_application_name("Simple Application");
}

Glib::RefPtr<SimpleApplication> SimpleApplication::create()
{
  return Glib::RefPtr<SimpleApplication>(new SimpleApplication());
}

void
SimpleApplication::on_startup()
{
  std::cout << "SimpleApplication::on_startup(): Entrance" << std::endl;
  Gtk::Application::on_startup();

  // Add actions
  Glib::VariantType Double(G_VARIANT_TYPE_DOUBLE);
  Glib::VariantType Int   (G_VARIANT_TYPE_INT32);

  m_test1   = add_action(              "test1", sigc::bind(sigc::mem_fun(*this, &SimpleApplication::on_test1), "app" ));
  m_test2   = add_action_bool(         "test2",            sigc::mem_fun(*this, &SimpleApplication::on_test2), false);
  m_test3   = add_action_radio_integer("test3",            sigc::mem_fun(*this, &SimpleApplication::on_test3), 0);
  m_test4   = add_action_radio_string( "test4",            sigc::mem_fun(*this, &SimpleApplication::on_test4), "Mango");
  m_test5   = add_action_with_parameter("test5", Double,   sigc::mem_fun(*this, &SimpleApplication::on_test5));
  m_test6   = add_action_with_parameter("test6", Int,      sigc::mem_fun(*this, &SimpleApplication::on_test6));

  m_icon    = add_action_with_parameter("icon_size", Int,  sigc::mem_fun(*this, &SimpleApplication::on_set_icon_size));

  // Glib::Variant<double> variant = Glib::Variant<double>::create(1);
  // auto action7 = Gio::SimpleAction::create( "test7", Double, variant);
  // m_test6  =   add_action(action7);

  m_reset   = add_action(              "reset",            sigc::mem_fun(*this, &SimpleApplication::reset) );
  m_disable = add_action_bool(         "disable",          sigc::mem_fun(*this, &SimpleApplication::disable));
  add_action(               "no_widget",     sigc::mem_fun(*this, &SimpleApplication::on_no_widget));

  // Accelerators defined in .ui files don't seem to work in Gtk4
  set_accel_for_action("app.test1", "<primary>1");
  set_accel_for_action("app.test2", "<primary>2");
  set_accel_for_action("app.test3(0)", "<primary>3");
  set_accel_for_action("app.test3(1)", "<alt>3");
  set_accel_for_action("app.test3(2)", "<primary><alt>3");
  set_accel_for_action("app.test4('Banana')", "<primary>4");
  set_accel_for_action("app.test4('Mango')",  "<alt>4");
  set_accel_for_action("app.test5(0.0)",   "<primary>5");
  set_accel_for_action("app.test5(100.0)", "<alt>5");

  // Custom widgets with proxies must be instantiated at least once before they can be used!
  auto tmp1 = new SimpleButton2();
  auto tmp2 = new SpinButtonAction();
#if GTK_CHECK_VERSION(4, 0, 0)
#else
  auto tmp3 = new ToolItemMenu();
#endif

  // Construct UI (see Gio::Menu to construct by hand)
  m_refBuilder = Gtk::Builder::create();

  try
    {
#if GTK_CHECK_VERSION(4, 0, 0)
      m_refBuilder->add_from_file("simple-application.gtk4.ui");
#else
      m_refBuilder->add_from_file("simple-application.ui");
#endif
    }
  catch (const Glib::Error& err)
    {
      std::cerr << "Building UI failed: " << err.what();
    }

  // Add menu to application.
  auto object = m_refBuilder->get_object("menu-bar");
#if GTK_CHECK_VERSION(4, 0, 0)
  auto menu = std::dynamic_pointer_cast<Gio::Menu>(object);
#else
  auto menu = Glib::RefPtr<Gio::Menu>::cast_dynamic(object);
#endif
  if (!menu) {
    std::cerr << "Menu not found" << std::endl;
  }

  // Add a new sub-menu programatically.
  auto submenu = Gio::Menu::create();
  auto section0 = Gio::Menu::create();
  auto section1 = Gio::Menu::create();
  auto section2 = Gio::Menu::create();

  // Menu items with icon.
  auto menu_itema = Gio::MenuItem::create("Test1", "app.test1");
  auto icona = Gio::ThemedIcon::create("document-new");
  menu_itema->set_icon(icona);

  auto menu_itemb = Gio::MenuItem::create("Test2", "app.test2");
  auto iconb = Gio::ThemedIcon::create("document-new");
  menu_itemb->set_icon(iconb);

  auto menu_item0 = Gio::MenuItem::create("0", "app.test3(0)");
  auto icon0 = Gio::ThemedIcon::create("format-justify-left");
  menu_item0->set_icon(icon0);
  menu_item0->set_attribute_value("use-icon", Glib::Variant<Glib::ustring>::create("True"));

  auto menu_item1 = Gio::MenuItem::create("1", "app.test3(1)");
  auto icon1 = Gio::ThemedIcon::create("format-justify-center");
  menu_item1->set_icon(icon1);
  menu_item1->set_attribute_value("use-icon", Glib::Variant<Glib::ustring>::create("True"));

  auto menu_item2 = Gio::MenuItem::create("2", "app.test3(2)");
  auto icon2 = Gio::ThemedIcon::create("format-justify-right");
  menu_item2->set_icon(icon2);
  menu_item2->set_attribute_value("use-icon", Glib::Variant<Glib::ustring>::create("True"));

  section0->append_item(menu_itema);
  section0->append_item(menu_itemb);
  section1->append_item(menu_item0);
  section1->append_item(menu_item1);
  section1->append_item(menu_item2);
  section2->append("Banana",  "app.test4('Banana')");
  section2->append("Mango",   "app.test4('Mango')");

  submenu->append_section(section0);
  submenu->append_section(section1);
  submenu->append_section(section2);

  menu->append_submenu("Programmed", submenu);

  std::cout << " Use icons?\n  0: always\n  1: only if tagged in .ui file\n  2: never\n  : ";
  int useicons = 0;
  std::cin >> useicons;

  if (useicons != 0) {
    std::cout << "removing some icons" << std::endl;
    // Remove all or some icons.
    auto menu_copy = Gio::Menu::create();
    copy_menu(menu, menu_copy, static_cast<UseIcons>(useicons));
    dump_menu(menu_copy);
    set_menubar( menu_copy );  // Per window menus
  } else {
    // Show all icons
    std::cout << "showing all icons" << std::endl;
    set_menubar(menu);
  }

  // Construct a "Dialog" menu
  std::vector<Glib::ustring> colors = {"red", "blue", "green", "purple"};
  int row = 0;

  auto menu_button_object = m_refBuilder->get_object("MenuButtonDialog");
#if GTK_CHECK_VERSION(4, 0, 0)
  auto menu_button_dialog = std::dynamic_pointer_cast<Gtk::MenuButton>(menu_button_object);
#else
  auto menu_button_dialog = Glib::RefPtr<Gtk::Button>::cast_dynamic(menu_button_object);
#endif

  if (!menu_button_dialog) {
    std::cerr << "Dialog menu button not found" << std::endl;
  } else {
    std::cout << "Dialog menu found!" << std::endl;
  }

#if GTK_CHECK_VERSION(4, 0, 0)
  auto grid_dialog_object = m_refBuilder->get_object("PopoverDialogGrid");
  auto grid_dialog = std::dynamic_pointer_cast<Gtk::Grid>(grid_dialog_object);
  if (!grid_dialog) {
    std::cerr << "Dialog grid not found" << std::endl;
  } else {
    std::cout << "Dialog grid found!" << std::endl;
  }

  auto grid_button1 = Gtk::manage(new Gtk::Button("Dialog1"));
  auto grid_button2 = Gtk::manage(new Gtk::Button("Dialog2"));
  auto grid_button3 = Gtk::manage(new Gtk::Button("Dialog3"));
  auto grid_button4 = Gtk::manage(new Gtk::Button("Dialog4"));
  auto grid_button5 = Gtk::manage(new Gtk::Button("Dialog5"));
  auto grid_separator = Gtk::manage(new Gtk::Separator());
  auto grid_label   = Gtk::manage(new Gtk::Label("Colors"));

  row = 0;

  grid_dialog->attach(*grid_button1, 0, row, 1, 1);
  grid_dialog->attach(*grid_button2, 1, row, 1, 1);
  row++;

  grid_dialog->attach(*grid_button3, 0, row, 2, 1);
  row++;

  grid_dialog->attach(*grid_button4, 0, row, 1, 1);
  grid_dialog->attach(*grid_button5, 1, row, 1, 1);
  row++;

  grid_dialog->attach(*grid_separator, 0, row, 2, 1);
  row++;

  grid_dialog->attach(*grid_label, 0, row, 2, 1);
  row++;

  int position = 0;
  for (auto color : colors) {
    auto color_button = create_dialog_button(color);
    grid_dialog->attach(*color_button, position%2, row);
    if (position%2 == 1) row++;
    position++;
  }

#else

  menu_button_dialog->signal_clicked().connect([=](){ m_menu_dialog.popup_at_widget(menu_button_dialog.get(), Gdk::GRAVITY_SOUTH, Gdk::GRAVITY_NORTH, nullptr); });
  menu_button_dialog->show();
  menu_button_dialog->set_can_focus(false);

  auto menu_itemd1 = Gtk::manage(new Gtk::MenuItem("Dialog1"));
  auto menu_itemd2 = Gtk::manage(new Gtk::MenuItem("Dialog2"));
  auto menu_itemd3 = Gtk::manage(new Gtk::MenuItem("Dialog3"));
  auto menu_itemd4 = Gtk::manage(new Gtk::MenuItem("Dialog4"));
  auto menu_itemd5 = Gtk::manage(new Gtk::MenuItem("Dialog5"));
  auto menu_itemds = Gtk::make_managed<Gtk::SeparatorMenuItem>();
  menu_itemds->set_label("Colors");
  menu_itemds->set_sensitive(false);

  menu_itemd1->signal_activate().connect(sigc::bind(sigc::mem_fun(*this, &SimpleApplication::on_test1), "app" ));

  m_menu_dialog.attach(*menu_itemd1, 0, 1, row, row+1);
  m_menu_dialog.attach(*menu_itemd2, 1, 2, row, row+1);
  row++;

  m_menu_dialog.attach(*menu_itemd3, 0, 2, row, row+1);
  row++;

  m_menu_dialog.attach(*menu_itemd4, 0, 1, row, row+1);
  m_menu_dialog.attach(*menu_itemd5, 1, 2, row, row+1);
  row++;

  m_menu_dialog.attach(*menu_itemds, 0, 2, row, row+1);
  row++;

  int position = 0;
  for (auto color : colors) {
    auto color_button = create_dialog_button(color);
    auto menu_item = Gtk::manage(new Gtk::MenuItem(*color_button));
    m_menu_dialog.attach(*menu_item, position%2, position%2+1, row, row+1);
    if (position%2 == 1) row++;
    position++;
  }

  m_menu_dialog.show_all_children();
#endif




#if GTK_CHECK_VERSION(4, 0, 0)
#else
#endif

  // Manually add a simple button
  auto object2 = m_refBuilder->get_object("ToolbarD");
#if GTK_CHECK_VERSION(4, 0, 0)
  auto toolbard = std::dynamic_pointer_cast<Gtk::Box>(object2);
#else
  auto toolbard = Glib::RefPtr<Gtk::Box>::cast_dynamic(object2);
#endif
  if (!toolbard) {
    std::cerr << "Toolbar D not found" << std::endl;
  } else {
    std::cout << "Toolbar D found!" << std::endl;
  }
  auto simple = Gtk::manage(new SimpleButton());
#if GTK_CHECK_VERSION(4, 0, 0)
  simple->set_label("ManualButton");
  simple->set_action_name("app.test1");
  if (toolbard) {
    toolbard->append(*simple);
  }
#else
  simple->add_label("ManualButton");
  gtk_actionable_set_action_name( GTK_ACTIONABLE(simple->gobj()), "app.test1");
  if (toolbard) {
    toolbard->add(*simple);
  }
#endif

  // Get simple button from .ui file
#if GTK_CHECK_VERSION(4, 0, 0)
  auto simple1 = Gtk::Builder::get_widget_derived<SimpleButton>(m_refBuilder, "SimpleButtonID");
#else
  SimpleButton* simple1 = nullptr;
  m_refBuilder->get_widget_derived("SimpleButtonID", simple1);
#endif

  if (simple1) {
    std::cout << "Simple button from UI file found!" << std::endl;
  } else {
    std::cerr << "Simple button from UI file not found!" << std::endl;
  }

#if GTK_CHECK_VERSION(4, 0, 0)
  auto spinbutton = m_refBuilder->get_widget<Gtk::SpinButton>("SpinButton");
#else
  Gtk::SpinButton* spinbutton = nullptr;
  m_refBuilder->get_widget("SpinButton", spinbutton);
#endif

  if (spinbutton) {
    spinbutton->signal_value_changed().connect(sigc::bind(sigc::ptr_fun(on_value_changed), spinbutton));
  }

  // Get combobox from .ui file
#if GTK_CHECK_VERSION(4, 0, 0)
  auto combobox1 = m_refBuilder->get_widget<Gtk::ComboBox>("ComboBox1");
#else
  Gtk::ComboBox* combobox1 = nullptr;
  m_refBuilder->get_widget("ComboBox1", combobox1);
#endif

  if (combobox1) {
    combobox1->signal_changed().connect(sigc::bind(sigc::ptr_fun(on_changed), combobox1));
  }

  // ===================  Gtk::ListStore vs. Gio::ListStore ======================

  const int STEP = 4; // Controls size of ListStore arrays, larger step -> smaller array.

  // Use classic Gtk::ListStore
#if GTK_CHECK_VERSION(4, 0, 0)
  auto scrolled_window0 = m_refBuilder->get_widget<Gtk::ScrolledWindow>("ScrolledWindow0");
#else
  Gtk::ScrolledWindow* scrolled_window0 = nullptr;
  m_refBuilder->get_widget("ScrolledWindow0", scrolled_window0);
#endif

  ColorColumns columns_gtk;
  auto list_store_gtk = Gtk::ListStore::create(columns_gtk);
  list_store_append_color(list_store_gtk, "Red");
  list_store_append_color(list_store_gtk, "Blue");
  list_store_append_color(list_store_gtk, "Green");
  list_store_append_color(list_store_gtk, "Purple");
  list_store_append_color(list_store_gtk, "Orange");
  list_store_append_color(list_store_gtk, "Aqua");
  list_store_append_color(list_store_gtk, "Violet");

  std::cout << "Before creating Gtk::ListStore." << std::endl;
  int count = 0;
  for (int i = 0; i < 0xFF; i+=STEP) {
    for (int j = 0; j < 0xFF; j+=STEP) {
      for (int k = 0; k < 0xFF; k+=STEP) {
        char hex[8];
        int color = (i << 16) + (j << 8) + k;
        snprintf(hex, sizeof hex, "#%06x", color);
        list_store_append_color(list_store_gtk, hex);
        count++;
      }
    }
  }
  std::cout << "After creating Gtk::ListStore: number of items: " << count << "." << std::endl;

  auto treeview = Gtk::manage(new Gtk::TreeView(list_store_gtk));
  treeview->set_headers_visible(false);
  treeview->set_vexpand();

  auto swatch_renderer = Gtk::manage(new Gtk::CellRendererPixbuf());

  treeview->append_column("Color", columns_gtk.col_color);
  treeview->append_column("Color2", *swatch_renderer);

  auto swatch_column = treeview->get_column(1);
  swatch_column->add_attribute(swatch_renderer->property_pixbuf(), columns_gtk.col_pixbuf);

  treeview->signal_row_activated().connect(sigc::bind(sigc::ptr_fun(on_treeview_row_activated), list_store_gtk));
  treeview->set_activate_on_single_click(true);

#if GTK_CHECK_VERSION(4, 0, 0)
  scrolled_window0->set_child(*treeview);
#else
  scrolled_window0->add(*treeview);
#endif

#if GTK_CHECK_VERSION(4, 0, 0)
  // Use Gio::ListStore
  auto list_store = Gio::ListStore<MyColor>::create();
  list_store->append(MyColor::create("Red"));
  list_store->append(MyColor::create("Blue"));
  list_store->append(MyColor::create("Green"));
  list_store->append(MyColor::create("Purple"));
  list_store->append(MyColor::create("Orange"));
  list_store->append(MyColor::create("Aqua"));
  list_store->append(MyColor::create("Violet"));

  std::cout << "Before creating Gio::ListStore" << std::endl;
  count = 0;
  for (int i = 0; i < 0xFF; i+=STEP) {
    for (int j = 0; j < 0xFF; j+=STEP) {
      for (int k = 0; k < 0xFF; k+=STEP) {
        char hex[8];
        int color = (i << 16) + (j << 8) + k;
        snprintf(hex, sizeof hex, "#%06x", color);
        list_store->append(MyColor::create(hex));
        count++;
      }
    }
  }
  std::cout << "After creating Gio::ListStorelist: number of items: " << count << "." << std::endl;

  auto factory = Gtk::SignalListItemFactory::create();
  factory->signal_setup().connect(sigc::ptr_fun(setup_listitem_cb));
  factory->signal_bind().connect(sigc::ptr_fun(bind_listitem_cb));

  auto dropdown1 = m_refBuilder->get_widget<Gtk::DropDown>("DropDown1");
  if (dropdown1) {
    dropdown1->set_model(list_store);
    dropdown1->set_factory(factory);
  }

  auto list = Gtk::make_managed<Gtk::ListView>(Gtk::SingleSelection::create(list_store), factory);
  list->set_single_click_activate(true);
  list->signal_activate().connect(sigc::bind<0>(sigc::ptr_fun(activate_cb), list));

  auto scrolled_window1 = m_refBuilder->get_widget<Gtk::ScrolledWindow>("ScrolledWindow1");
  if (scrolled_window1) {
    scrolled_window1->set_child(*list);
  }

  auto grid_view = Gtk::make_managed<Gtk::GridView>(Gtk::SingleSelection::create(list_store), factory);
  auto scrolled_window2 = m_refBuilder->get_widget<Gtk::ScrolledWindow>("ScrolledWindow2");
  if (scrolled_window2) {
    scrolled_window2->set_child(*grid_view);
  }

  // auto grid_view = m_refBuilder->get_widget<Gtk::GridView>("GridViewA");
  // if (grid_view) {
  //   grid_view->set_model(Gtk::SingleSelection::create(list_store));
  //   grid_view->set_factory(factory);
  // } else {
  //   std::cerr << "No Gtk::GridView!!" << std::endl;
  // }

  grid_view->set_min_columns(2);
  grid_view->set_max_columns(2);
  grid_view->set_single_click_activate(true);
  grid_view->signal_activate().connect(sigc::bind<0>(sigc::ptr_fun(activate_grid_cb), grid_view));

#endif

  // For custom widgets, we must call get_widget_derived or the callbacks don't work!
  // Extract the custom widgets out of the .ui file.
  std::fstream ui_file;
  ui_file.open("simple-application.ui", std::ios::in);
  if (ui_file.is_open()) {
    static auto regex = Glib::Regex::create("\\'gtkmm__CustomObject_(.*)\\'.*id=\\'(.*)\\'");
    Glib::MatchInfo match_info;
    std::string line;
    while (std::getline(ui_file, line)) {
      regex->match(line.c_str(), match_info); // c_str() needed for Gtk4
      if (match_info.matches()) {
	std::cout << "  match: " << match_info.fetch(1) << " : " << match_info.fetch(2) << std::endl;
	if (match_info.fetch(1) == "SpinButtonAction") {
#if GTK_CHECK_VERSION(4, 0, 0)
          auto spin_button_action = Gtk::Builder::get_widget_derived<SpinButtonAction>(m_refBuilder, match_info.fetch(2));
#else
	  SpinButtonAction* spin_button_action = nullptr;
	  m_refBuilder->get_widget_derived(match_info.fetch(2), spin_button_action);
#endif
	} else if (match_info.fetch(1) == "ToolItemMenu") {
#if !GTK_CHECK_VERSION(4, 0, 0)
	  ToolItemMenu* tool_item_menu = nullptr;
	  m_refBuilder->get_widget_derived(match_info.fetch(2), tool_item_menu);
#endif
	}
      }
    }
    ui_file.close();
  } else {
    std::cerr << "Coudn't read ui file 'simple-application.ui'!!" << std::endl;
  }

  std::cout << "DBus object path: " << get_dbus_object_path() << std::endl;
  std::cout << "SimpleApplication::on_startup(): Exit" << std::endl;
}

void
SimpleApplication::on_activate()
{
  std::cout << "SimpleApplication::on_activate(): Entrance" << std::endl;
  auto window = create_window();

  if (window) {

    // Add an extra window and make the main window' action group available to it!
    auto extra_window = new Gtk::Window();
    extra_window->set_title("Dialog Window");
    auto extra_window_button = new Gtk::Button("Dialog: Test 1");

    auto action_group = dynamic_cast<Gio::ActionGroup *>(window);
    if (action_group) {
      std::cout << "SimpleApplication::on_activate: Inserting group!" << std::endl;
      // Must use C API as C++ API takes a RefPtr which we can't get (easily).
      gtk_widget_insert_action_group(GTK_WIDGET(extra_window->gobj()),
                                     "extra",
                                     action_group->gobj());
    } else {
      std::cerr << "SimpleApplication::on_activate: no action group!" << std::endl;
    }

#if GTK_CHECK_VERSION(4, 0, 0)
    extra_window_button->set_action_name("extra.win_test1");
    extra_window->set_child(*extra_window_button);
#else
    // Can't access via C++ API, fixed in GTK4.
    gtk_actionable_set_action_name( GTK_ACTIONABLE(extra_window_button->gobj()), "extra.win_test1");
    extra_window->add(*extra_window_button);
    extra_window->show_all();
#endif

    extra_window->show();
    add_window(*extra_window);

#if GTK_CHECK_VERSION(4, 0, 0)
    std::cout << "**********************" << std::endl;
    // Only available in GTK4!
    extra_window->activate_action("extra.win_test1"); // Should work.
    extra_window->activate_action("app.test2");       // Should work.
    extra_window->activate_action("win.test1");       // Should not work.
    std::cout << "**********************" << std::endl;
#endif

    // Add callback to menus to adjust icons (visible, shift).
#if GTK_CHECK_VERSION(4, 0, 0)
    // All widgets can have children!
    for (auto child = window->get_first_child(); child != nullptr; child = child->get_next_sibling()) {
      auto menubar = dynamic_cast<Gtk::PopoverMenuBar*>(child);
      if (menubar) {
        fix_menuicons_recurse(menubar);
      }
    }


#else
    auto container = dynamic_cast<Gtk::Container*>(window);
    if (container) {
      auto children = container->get_children();
      for (auto child : children) {
        auto menubar = dynamic_cast<Gtk::MenuBar*>(child);
        if (menubar) {
          auto children2 = menubar->get_children();
          for (auto child2 : children2) {
            auto menuitem = dynamic_cast<Gtk::MenuItem *>(child2);
            if (menuitem) {
              auto submenu = menuitem->get_submenu();
              if (submenu) {
                fix_menuicons_recurse(submenu);
              }
            }
          }
        }
      }
    }
#endif

  } else {
    std::cout << "SimpleApplication::on_activate(): no window!" << std::endl;
  }
  std::cout << "SimpleApplication::on_activate(): Exit" << std::endl;
}

void SimpleApplication::on_test1(std::string tag)
{
  std::cout << "on_test1: " << tag << std::endl;
}

void SimpleApplication::on_test2()
{
  bool active = false;
  m_test2->get_state(active);
  active = !active;
  m_test2->change_state(active);

  if (active) {
    std::cout << "on_test2: active" << std::endl;
  } else {
    std::cout << "on_test2: not active" << std::endl;
  }
}

void SimpleApplication::on_test3( int value )
{
  static bool block = false;
  if (block) return;
  block = true;

  m_test3->change_state( value );

  block = false;

  std::cout << "on_test3: " << value << std::endl;
}

void SimpleApplication::on_test4( Glib::ustring value )
{
  static bool block = false;
  if (block) return;
  block = true;

  m_test4->change_state( value );

  block = false;

  std::cout << "on_test4: " << value << std::endl;
}

void SimpleApplication::on_test5( Glib::VariantBase value )
{
  // No state
  Glib::Variant<double> d = Glib::VariantBase::cast_dynamic<Glib::Variant<double> > (value);
  std::cout << "on_test5: " << d.get() << std::endl;
}

void SimpleApplication::on_test5_wrap()
{
  std::cout << "on_test5_wrap" << std::endl;
  double value = m_adjustment0->get_value();
  // Do some validiation...
  activate_action("test5", Glib::Variant<double>::create(value) );
}


void SimpleApplication::on_test6( Glib::VariantBase value )
{
  // No state
  Glib::Variant<gint32> i = Glib::VariantBase::cast_dynamic<Glib::Variant<gint32> > (value);
  std::cout << "on_test6: " << i.get() << std::endl;
}

// Reset to default values
void SimpleApplication::reset()
{
  std::cout << "SimpleApplication::reset(): Entrance" << std::endl;

  // We can directly change boolean values.
  m_test2->change_state(false);

  // We cannot directly change radio buttons as they trigger changes
  // the other buttons in the group which leads to loops. Activating
  // the action prevents this as the callback blocks additonal
  // callbacks.
  on_test3 (0);
  on_test4 ("Banana");
  //m_test3->change_state(2);
  std::cout << "SimpleApplication::reset(): Exit" << std::endl;
}

void SimpleApplication::on_set_icon_size( Glib::VariantBase value )
{
  // No state
  Glib::Variant<gint32> i = Glib::VariantBase::cast_dynamic<Glib::Variant<gint32> > (value);
  std::cout << "on_set_icon_size: " << i.get() << std::endl;

  static Glib::RefPtr<Gtk::CssProvider> provider;
  if (!provider) {
    provider = Gtk::CssProvider::create();
#if GTK_CHECK_VERSION(4, 0, 0)
    auto const display = Gdk::Display::get_default();
    Gtk::StyleContext::add_provider_for_display(display, provider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
#else
    auto const screen = Gdk::Screen::get_default();
    Gtk::StyleContext::add_provider_for_screen(screen, provider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
#endif
  }

#if GTK_CHECK_VERSION(4, 0, 0)
  std::string css_str = ".ToolbarIcon, .ToolbarIcon image {-gtk-icon-size:" + std::to_string(i.get()) + "px;}";
#else
  // GTK3 does not support -gtk-icon-size. This just shows that CSS classes work.
  std::string css_str = "image {margin-right:" + std::to_string(i.get()) + "px;}";
#endif

  provider->load_from_data(css_str);
}

// Reset to default values
void SimpleApplication::disable()
{
  std::cout << "SimpleApplication::disable(): Entrance" << std::endl;

  bool active = false;
  m_disable->get_state(active);

  active = !active;

  m_disable->change_state(active);

  if (active) {
    std::cout << "disable: active" << std::endl;
  } else {
    std::cout << "disable: not active" << std::endl;
  }

  m_test1->set_enabled(active);
  m_test3->set_enabled(active);
}

void SimpleApplication::on_no_widget()
{
  std::cout << "SimpleApplication:on_no_widget" << std::endl;
}

Gtk::ApplicationWindow*
SimpleApplication::create_window()
{
  std::cout << "SimpleApplication::create_window()" << std::endl;

#if 0
  /* ----------------------------------- */

  // The menubar isn't loaded with this code in GTK3!
#if GTK_CHECK_VERSION(4, 0, 0)
  auto application_window = m_refBuilder->get_widget<Gtk::ApplicationWindow>("ApplicationWindow");
  application_window->can_focus(true);
#else
  Gtk::ApplicationWindow* application_window = nullptr;
  m_refBuilder->get_widget("ApplicationWindow", application_window);
#endif

  if (!application_window) {
    std::cerr << "Application window not found!" << std::endl;
    return;
  }

  /* ----------------------------------- */
#else
  /* ----------------------------------- */

  auto application_window = new Gtk::ApplicationWindow;
  application_window->add_action(    "win_test1", sigc::bind(sigc::mem_fun(*this, &SimpleApplication::on_test1), "win" ));

  auto document_action_group = Gio::SimpleActionGroup::create();
  document_action_group->add_action( "doc_test1", sigc::bind(sigc::mem_fun(*this, &SimpleApplication::on_test1), "doc" ));

  application_window->insert_action_group("doc", document_action_group);

  // Export document action group to DBus
  auto connection = get_dbus_connection();
  Glib::ustring path = get_dbus_object_path();
  if (connection) {
    std::cout << "  Got DBus connection: (window) " << path << std::endl;
    auto document_path = get_dbus_object_path() + "/document/1";
    connection->export_action_group(document_path, document_action_group);
  } else {
    std::cout << "  Did not get DBus connection: " << path << std::endl;
  }


#if GTK_CHECK_VERSION(4, 0, 0)
  auto main_box = m_refBuilder->get_widget<Gtk::Box>("MainBox");
#else
  Gtk::Box* main_box = nullptr;
  m_refBuilder->get_widget("MainBox", main_box);
#endif

  if (!main_box) {
    std::cerr << "Application main box not found!" << std::endl;
    return nullptr;
  }
  main_box->unparent();

#if GTK_CHECK_VERSION(4, 0, 0)
  application_window->set_child(*main_box);
#else
  // Gtk::ApplicationWindow is derived from Gtk::Bin so it can contain only one child.
  application_window->add(*main_box);
#endif

  /* ----------------------------------- */
#endif

  application_window->set_show_menubar(true); // Needed by GTK4.
  application_window->set_title("Simple Application");

  Glib::VariantType Double(G_VARIANT_TYPE_DOUBLE);
  application_window->add_action_with_parameter("test5", Double, sigc::mem_fun(*this, &SimpleApplication::on_test5));

  // NOTE: This will not add a reference to the shared_ptr. Must use another way to keep it from self-destruction!
  add_window( *application_window );  // Application runs till window is closed.
  application_window->signal_hide().connect(
    sigc::bind(sigc::mem_fun(*this, &SimpleApplication::on_window_hide), application_window)
  );

  // Styling --------------------------------------------------

  auto provider = Gtk::CssProvider::create();
  try {
    provider->load_from_path("simple-application.css");
  }
  catch (...) {
  }

#if GTK_CHECK_VERSION(4, 0, 0)
  auto const display = Gdk::Display::get_default();
  Gtk::StyleContext::add_provider_for_display (display, provider, GTK_STYLE_PROVIDER_PRIORITY_USER);
#else
  auto const screen = Gdk::Screen::get_default();
  Gtk::StyleContext::add_provider_for_screen (screen, provider, GTK_STYLE_PROVIDER_PRIORITY_USER);
#endif

  set_tooltip_recurse(application_window);

  std::vector<Glib::ustring> action_descriptions = list_action_descriptions();
  for (auto action_description : action_descriptions) {
    std::cout << "Action description: " << action_description;
    std::vector<Glib::ustring> accels = get_accels_for_action(action_description);
    for (auto accel : accels) {
      std::cout << "  " << accel;
    }
    std::cout << std::endl;
  }

#if GTK_CHECK_VERSION(4, 0, 0)
  application_window->present();
#else
  application_window->show_all();
#endif

  std::cout << "SimpleApplication::create_window(): Exit" << std::endl;
  return application_window;
}

void
SimpleApplication::on_window_hide(Gtk::Window* window)
{
  std::cout << "SimpleApplication::on_window_hide()" << std::endl;
  delete window;
}

void
SimpleApplication::set_tooltip_recurse(Gtk::Widget* widget)
{
  static int indent = 0;
  indent++;

  Glib::ustring action_description;
  Glib::ustring action_accelerator;

  // Until GTK4 we need to fallback to GTK+
  if (GTK_IS_ACTIONABLE(widget->gobj())) {

    GtkActionable* actionable = GTK_ACTIONABLE(widget->gobj());
    const gchar* name = gtk_actionable_get_action_name (actionable);
    if (name) {
      action_description = name;
      GVariant* variant = gtk_actionable_get_action_target_value(actionable);
      if (variant) {

        if (       g_variant_is_of_type(variant, G_VARIANT_TYPE_INT32)) {
          const int integer = g_variant_get_int32 (variant);
          action_description += "(";
          action_description += std::to_string (integer);
          action_description += ")";

        } else if (g_variant_is_of_type(variant, G_VARIANT_TYPE_DOUBLE)) {
          const int Double = g_variant_get_double (variant);
          action_description += "(";
          action_description += std::to_string (Double);
          action_description += ")";

        } else if (g_variant_is_of_type(variant, G_VARIANT_TYPE_STRING)) {
          const gchar* string  = g_variant_get_string (variant, nullptr);
          action_description += "::";
          action_description += string;

        } else {
          std::cerr << "SimpleApplication::gui_recurse: unhandled variant type: "
                    << g_variant_get_type_string (variant) << ": "
                    << g_variant_print (variant, true) << std::endl;
        }
      }

      std::vector<Glib::ustring> accels = get_accels_for_action(action_description);
      if (!accels.empty()) {
        action_accelerator = accels[0];

        Glib::ustring tooltip = widget->get_tooltip_text();
        // To do, strip off any old accelerator info. Format like AccelLabel.
        tooltip += " (" + action_accelerator + ")";
        widget->set_tooltip_text( tooltip );
      } else {
        std::cout << "No accelerator for: " << action_description << std::endl;
      }
    }
  }

  // Glib::Value<std::string> string;
  // string.init(G_TYPE_STRING); // Must init!!!!
  // button->get_property_value("action_name", string);
  // std::string action_name(string.get());
  // std::cout << " action_name: "   << action_name << std::endl;

  for (auto i = 0; i < indent; ++i) {
    std::cout << "  ";
  }

  std::cout << std::setw(30) << std::left << widget->get_name();

  for (int i = 8 - indent; i > 0; --i) {
    std::cout << "  ";
  }

  std::cout << "    " << std::setw(20) << std::left << action_description
            << "    " << std::setw(10) << std::left << action_accelerator
            << "    " << widget->get_tooltip_text() << std::endl;


#if GTK_CHECK_VERSION(4, 0, 0)
  // All widgets can have children!
  for (auto child = widget->get_first_child(); child != nullptr; child = child->get_next_sibling()) {
    set_tooltip_recurse (child);
  }
#else
  auto container = dynamic_cast<Gtk::Container*>(widget);
  if (container) {
    auto children = container->get_children();
    for (auto child: children) {
      set_tooltip_recurse (child);
    }
  }
#endif

  indent--;
}

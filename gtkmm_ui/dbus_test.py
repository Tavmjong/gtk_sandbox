#!/usr/bin/env python

#  Start after 'simple-application3' or 'simple-application4' is running.
print ("DBus test")

import gi
gi.require_version("Gio", "2.0")
from gi.repository import Gio, GLib

try:
    bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)
except BaseException:
    print("No Gio bus")
    exit()
print ("Got Gio bus")

proxy = Gio.DBusProxy.new_sync(bus, Gio.DBusProxyFlags.NONE, None,
                               'org.freedesktop.DBus',
                               '/org/freedesktop/DBus',
                               'org.freedesktop.DBus', None)
names_list = proxy.call_sync('ListNames', None, Gio.DBusCallFlags.NO_AUTO_START,
                            500, None)
# print(names_list)
# names_list is a GVariant, must unpack
names = names_list.unpack()[0]

# names is a tuple
for name in names:
    if ('org.tavmjong.simpleapplication' in name):
        print ("Found: " + name)
        break

appGroupName = name.replace(".", "/")
appGroupName = "/" + appGroupName
print ("appGroupName: " + appGroupName)

appGroup = Gio.DBusActionGroup.get(
    bus,
    name,
    appGroupName)

docGroupName = appGroupName + "/document/1"
docGroup = Gio.DBusActionGroup.get(
    bus,
    name,
    docGroupName)

winGroupName = appGroupName + "/window/1"
winGroup = Gio.DBusActionGroup.get(
    bus,
    name,
    winGroupName)

# Extracting information from 'appGroup' seems to be broken!
actions = appGroup.list_actions()
# actions is a list
print (actions)
for action in actions:
    print (action)

if appGroup.has_action('test1'):
    print ("Has action 'test1'")
else:
    print ("Does not have action 'test1'")

if appGroup.has_action('testA'):
    print ("Has action 'testA'")
else:
    print ("Does not have action 'testA'")

type = appGroup.get_action_parameter_type('test3')
print (type)

# Activate actions. This does work!
print ("Testing 'app' action group")
appGroup.activate_action('test1', None)
appGroup.activate_action('test2', None)
appGroup.activate_action('test3', GLib.Variant.new_int32(1))
appGroup.activate_action('test4', GLib.Variant.new_string('Mango'))
appGroup.activate_action('test4', GLib.Variant.new_string('Banana'))
appGroup.activate_action('test5', GLib.Variant.new_double(1.23))
appGroup.activate_action('test6', GLib.Variant.new_int32(23))
appGroup.activate_action('test1', None)

print ("Testing 'win' action group")
winGroup.activate_action('win_test1', None)

print ("Testing 'doc' action group")
docGroup.activate_action('doc_test1', None)

# Monitor bus in a loop:
def onNameAppeared(connection, name, name_owner):
    print ("onNameAppeared: " + name + " " + name_owner)

def onNameVanished(connection, name):
    print ("onNameVanished: " + name)

busWatchId = Gio.bus_watch_name (
    Gio.BusType.SESSION,
    name,
    Gio.BusNameWatcherFlags.NONE,
    onNameAppeared,
    onNameVanished)

loop = GLib.MainLoop.new(None, False)
loop.run()

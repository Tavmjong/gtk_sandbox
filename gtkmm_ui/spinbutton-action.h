#ifndef SPINBUTTON_ACTION_H
#define SPINBUTTON_ACTION_H

#include <giomm/application.h>
#include <gtkmm.h>
#include <iostream>
#include <sstream>

void on_action_name_changed()
{
  std::cout << "  on_action_name_changed!" << std::endl;
}

// class SpinButtonAction : public Gtk::SpinButton, public Gtk::Actionable doesn't work
// as builder gets confused as to what is the BaseObjectType

// A spinbutton with an action. Action can be either an integer or double type.
// Set action name via "action-name" property.
class SpinButtonAction : public Gtk::SpinButton
{
  using parent_type = Gtk::SpinButton;

 public:

  SpinButtonAction();
  SpinButtonAction(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refUI);
  virtual ~SpinButtonAction() {};
#if !GTK_CHECK_VERSION(4, 0, 0)
  Gtk::MenuItem* get_menu();
#endif
  Glib::PropertyProxy<Glib::ustring> property_action_name()    { return prop_action_name.get_proxy(); }
  Glib::PropertyProxy<Glib::ustring> property_custom_values()  { return prop_custom_values.get_proxy(); }
  Glib::PropertyProxy<Glib::ustring> property_defocus_widget() { return prop_defocus_widget.get_proxy(); }

 private:

  std::string type_string;
  // Glib::VariantType type;
  double saved_value = 0;

  Glib::RefPtr<Gio::Menu> create_menu();
  void defocus();

  void on_realize() override;
#if GTK_CHECK_VERSION(4, 0, 0)
  int on_input(double& new_value);
  bool on_output();
  void on_value_changed();
  void on_button_pressed_event(int n_press, double x, double y, Gtk::Text* text);
  bool on_key_pressed_event(guint keyval, guint keycode, Gdk::ModifierType state); // "pressed" vs GTK3 "press"
  void on_enter();
  void on_changed() override;
#else
  int on_input(double* new_value) override;
  bool on_output() override;
  void on_value_changed() override;
  //  bool on_button_press_event(GdkEventButton *button_event) override;
  void on_populate_popup(Gtk::Menu* menu) override;
  bool on_focus_in_event(GdkEventFocus* event) override;
  bool on_key_press_event(GdkEventKey* event) override;
#endif

  void on_action_value_changed(const Glib::VariantBase& parameter);

  Glib::RefPtr<Gio::Action> action;
  Glib::RefPtr<Gio::Application> application;
  Gtk::ApplicationWindow* window = nullptr;
  Gtk::Widget* defocus_widget = nullptr;

  // Properties
  Glib::Property<Glib::ustring> prop_action_name;
  Glib::Property<Glib::ustring> prop_menu_name; // Used for proxy menu.
  Glib::Property<Glib::ustring> prop_custom_values;
  Glib::Property<Glib::ustring> prop_defocus_widget;
};

Gtk::Widget* find_widget_by_name_recurse(Gtk::Widget* widget, const Glib::ustring& name)
{
  if (widget && widget->get_name() == name) {
    std::cout << "Found widget" << std::endl;
    return widget;
  }

#if GTK_CHECK_VERSION(4, 0, 0)

  for (auto child = widget->get_first_child(); child != nullptr; child = child->get_next_sibling()) {
    auto temp = find_widget_by_name_recurse(child, name);
    if (temp) {
      return temp;
    }
  }

#else

  auto widget_bin = dynamic_cast<Gtk::Bin *>(widget);
  if (widget_bin) {
    std::cout << "  Bin: " << widget_bin->get_name() << std::endl;
    auto child = widget_bin->get_child();
    if (child) {
      return find_widget_by_name_recurse(child, name);
    }
    return nullptr;
  }

  auto widget_container = dynamic_cast<Gtk::Container *>(widget);
  if (widget_container) {
    std::cout << "  Container: " << widget_container->get_name() << std::endl;
    auto children = widget_container->get_children();
    for (auto child : children) {
      auto temp = find_widget_by_name_recurse(child, name);
      if (temp) {
        return temp;
      }
    }
  }

#endif

  return nullptr;
}

// Dummy constructor to register type.
SpinButtonAction::SpinButtonAction()
  : Gtk::SpinButton()
  , Glib::ObjectBase("SpinButtonAction")
  , prop_action_name(*this, "action-name")
  , prop_menu_name(*this, "menu-name")
  , prop_custom_values(*this, "custom-values")
  , prop_defocus_widget(*this, "defocus-widget")
{
  std::cout << "SpinButtonAction::SpinButtonAction(): default constructor" << std::endl;
}

SpinButtonAction::SpinButtonAction(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refUI)
  : Gtk::SpinButton(cobject)
  , Glib::ObjectBase("SpinButtonAction")
  , prop_action_name(*this, "action-name")
  , prop_menu_name(*this, "menu-name")
  , prop_custom_values(*this, "custom-values")
  , prop_defocus_widget(*this, "defocus-widget")
{
  std::cout << "SpinButtonAction::SpinButtonAction(): UI constructor: "
            << prop_action_name << ": " << prop_custom_values
            << "  defocus_widget: " << prop_defocus_widget
            << std::endl;
  application = Gtk::Application::get_default();

  if (!application) {
    std::cerr << "SpinButtonAction: no application!" << std::endl;
    return;
  }
}

#if !GTK_CHECK_VERSION(4, 0, 0)
// Create menu for GTK3 ToolItemMenu
Gtk::MenuItem*
SpinButtonAction::get_menu()
{
  Glib::ustring name = prop_menu_name.get_value();
  if (name == "") {
    name = prop_action_name.get_value();
  }
  if (name == "") {
    name = "SpinButton";
  }
  auto menu_item = Gtk::manage(new Gtk::MenuItem(name));
  auto menu = Gtk::manage(new Gtk::Menu(create_menu()));
  menu_item->set_submenu(*menu);
  return menu_item;
}
#endif

void
SpinButtonAction::defocus()
{
  std::cout << "SpinButtonAction::defocus: " << prop_defocus_widget << std::endl;

  if (!defocus_widget && prop_defocus_widget.get_value() != "") {
    defocus_widget = find_widget_by_name_recurse(window, prop_defocus_widget.get_value());
  }

  if (defocus_widget) {
    defocus_widget->grab_focus();
  }
}

// Used in creating menu
Glib::ustring round_to_digits(double value, int digits)
{
  std::stringstream stream;
  stream << std::fixed << std::setprecision(digits) << value;
  return stream.str();
}

// Used in creating menu
Glib::ustring round_to_digits(const Glib::ustring& value, int digits)
{
  return round_to_digits(std::stod(value), digits);
}

Glib::RefPtr<Gio::Menu>
SpinButtonAction::create_menu()
{
  Glib::ustring full_action_name = prop_action_name.get_value();
  Glib::ustring custom_values    = prop_custom_values.get_value();

  // Determine number of digits after decimal place.
  int digits = property_digits();
  if (type_string == "d" && digits < 1) {
    digits = 1; // Must have decimal place for "double" action to work!
  }
  if (type_string == "i") {
    digits = 0; // Must not have decimal places for "int" action to work!
  }

  // Create map of labels and values.
  std::map<double, Glib::ustring> entries;

  // Add values derived from adjustment (are these really needed in GTK4?)
  auto adjustment = get_adjustment();
  auto value          = adjustment->get_value();
  auto lower_value    = adjustment->get_lower();
  auto upper_value    = adjustment->get_upper();
  auto page_increment = adjustment->get_page_increment();

  entries[value]       = round_to_digits (value, digits);
  entries[lower_value] = round_to_digits (lower_value, digits);
  entries[upper_value] = round_to_digits (upper_value, digits);
  auto page_up   = value + page_increment;
  auto page_down = value - page_increment;
  if (page_up < upper_value) {
    entries[page_up] = round_to_digits (page_up, digits);
  }
  if (page_down > lower_value) {
    entries[page_down] = round_to_digits (page_down, digits);
  }

  // Add custom values (last so they can override values from adjustment).
  auto tokens = Glib::Regex::split_simple("\\s*;\\s*", custom_values);
  for (auto token: tokens) {
    Glib::ustring label;
    double value = 0;
    std::vector<Glib::ustring> parts = Glib::Regex::split_simple("\\s*:\\s*", token);
    if (parts.size() > 0) {
      value = std::stod(parts[0]);
      if (parts.size() == 1) {
        label = round_to_digits(parts[0], digits);
      }
      if (parts.size() == 2) {
        label = parts[1];
      }
    } else {
      std::cerr << "SpinButtonAction::create_menu(): no action value given!" << std::endl;
    }
    entries[value] = label;
  }

  // Create actual menu
  auto gmenu = Gio::Menu::create();
  for (auto const& [value, label] : entries) {

    // Construct detailed action name
    Glib::ustring action = full_action_name + "(" + round_to_digits (value, digits) + ")";

    auto gitem = Gio::MenuItem::create(label, action);
    gmenu->append_item(gitem);
  }

  return gmenu;
}

void SpinButtonAction::on_realize()
{
  std::cout << "SpinButtonAction::on_realize" << std::endl;
  Gtk::SpinButton::on_realize();


  // This whole section won't be necessary in Gtk4 where one can
  // activate any action in this widget or an ancester widget by using
  // on_activate() with the full action name.

  window = dynamic_cast<Gtk::ApplicationWindow*>(get_ancestor(GTK_TYPE_APPLICATION_WINDOW));
  if (!window) {
    std::cerr << "SpinButtonAction::on_realize: No application window!" << std::endl;
    return;
  }

  // Get a reference to the action
  Glib::ustring full_action_name = prop_action_name.get_value();
  std::vector<Glib::ustring> tokens = Glib::Regex::split_simple("\\.", full_action_name);

  if (tokens.size() != 2) {
    std::cerr << "SpinButtonAction:on_realize: invalid full action name: " << full_action_name << std::endl;
    return;
  }

  Glib::ustring map_name = tokens[0];
  Glib::ustring action_name = tokens[1];
  if (map_name == "app") {
    if (!application->has_action(action_name)) {
      std::cerr << "SpinButtonAction:on_realize: no app action with name: " << full_action_name << std::endl;
      return;
    }

    action = application->lookup_action(action_name);

  } else if (map_name == "win") {
    if (!window->has_action(action_name)) {
      std::cerr << "SpinButtonAction:on_realize: no win action with name: " << full_action_name << std::endl;
      return;
    }

    action = window->lookup_action(action_name);

  } else {
    std::cerr << "SpinButtonAction:on_realize: invalid map name: " << map_name << std::endl;
  }

  if (!action) {
    // Should never happen!
    std::cerr << "SpinButtonAction:on_realize: no action with name: " << full_action_name << std::endl;
  }

  // Doesn't seem to be a way to test this using the C++ binding without Glib-CRITICAL errors.
  const  GVariantType* gtype = g_action_get_parameter_type(action->gobj());
  if (!gtype) {
    std::cerr << "SpinButtonAction:on_value_changed: action without type: " << std::endl;
    return;
  }
  type_string = action->get_parameter_type().get_string();

#if GTK_CHECK_VERSION(4, 0, 0)
  auto saction = std::dynamic_pointer_cast<Gio::SimpleAction>(action);
#else
  auto saction = Glib::RefPtr<Gio::SimpleAction>::cast_dynamic(action);
#endif
  saction->signal_activate().connect(sigc::mem_fun(*this, &SpinButtonAction::on_action_value_changed));

#if GTK_CHECK_VERSION(4, 0, 0)
  // In GTK3 we override functions, in GTK4 default functions don't exist.
  signal_input().connect(        sigc::mem_fun(*this, &SpinButtonAction::on_input), true);  // bool -> after
  signal_output().connect(       sigc::mem_fun(*this, &SpinButtonAction::on_output), true);
  signal_value_changed().connect(sigc::mem_fun(*this, &SpinButtonAction::on_value_changed));

  // GTK: Add callback to created and add custom menu when using right button to click on text part of SpinButton.
  // In GTK4 SpinButton has three children: Gtk::Text + 2 x Gtk::Button
  for (auto child = get_first_child(); child != nullptr; child = child->get_next_sibling()) {

    auto text = dynamic_cast<Gtk::Text*>(child);
    if (text) {

      // Need to use controllers in GTK4.
      auto gesture = Gtk::GestureClick::create();
      gesture->set_name("CustomMenu");
      gesture->set_button(3); // 3: Right button
      gesture->signal_pressed().connect(sigc::bind(sigc::mem_fun(*this, &SpinButtonAction::on_button_pressed_event), text));
      text->add_controller(gesture);

      auto key = Gtk::EventControllerKey::create();
      key->signal_key_pressed().connect(sigc::mem_fun(*this, &SpinButtonAction::on_key_pressed_event), false); // Befor default handler.
      text->add_controller(key);

      auto focus = Gtk::EventControllerFocus::create();
      focus->signal_enter().connect(sigc::mem_fun(*this, &SpinButtonAction::on_enter));
      text->add_controller(focus);

      break;
    }
  }
#endif
}

#if GTK_CHECK_VERSION(4, 0, 0)

void SpinButtonAction::on_button_pressed_event(int n_press, double x, double y, Gtk::Text *text)
{
  std::cout << "on_press()" << std::endl;
  auto gmenu = Gio::Menu::create();
  gmenu->append_section(create_menu());
  text->set_extra_menu(gmenu);
}

#else

// Current way Inkscape handles SpinButton context menu.
// bool SpinButtonAction::on_button_press_event(GdkEventButton *button_event)
// {
//   std::cout << "on_button_press_event()" << std::endl;
//   if (gdk_event_triggers_context_menu(reinterpret_cast<const GdkEvent *>(button_event)) &&
//       button_event->type == GDK_BUTTON_PRESS) {

//     auto gmenu = create_menu();
//     auto menu = Gtk::manage(new Gtk::Menu(gmenu));

//     menu->attach_to_widget(*this);
//     menu->show_all();
//     menu->popup_at_pointer(reinterpret_cast<const GdkEvent*>(button_event));

//     return true;
//   }
//   return Gtk::SpinButton::on_button_press_event(button_event);
// }

void SpinButtonAction::on_populate_popup(Gtk::Menu* menu)
{
  std::cout << "on_populate_popup()" << std::endl;
  auto separator = Gtk::manage(new Gtk::SeparatorMenuItem());
  menu->append(*separator);

  auto gmenu = create_menu();
  auto extra_menu = Gtk::manage(new Gtk::Menu(gmenu));

  // Items shown but not enabled.
  // for (auto iter : extra_menu->get_children()) {
  //   std::cout << "child: " << iter->get_name() << std::endl;
  //   iter->reparent(*menu);
  // }

  auto item = Gtk::manage(new Gtk::MenuItem("Values"));
  item->set_submenu(*extra_menu);
  menu->append(*item);

  menu->show_all();
}
#endif

#if GTK_CHECK_VERSION(4, 0, 0)
bool SpinButtonAction::on_key_pressed_event(guint keyval, guint keycode, Gdk::ModifierType state)
{
  std::cout << "SpinButtonAction::on_key_pressed_event: " << keyval << std::endl;
  switch (keyval) {
  case GDK_KEY_Escape: // Defocus
    std::cout << "ESC" << std::endl;
    set_value(saved_value);
    defocus();
    return true;
    break;
  case GDK_KEY_Return:
    std::cout << "Return" << std::endl;
    defocus();
    break;
  case GDK_KEY_z:
  case GDK_KEY_Z:
    if ((state & Gdk::ModifierType::CONTROL_MASK) == Gdk::ModifierType::CONTROL_MASK) {
      std::cout << "Ctrl-Z" << std::endl;
      set_value(saved_value);
      return true;
    }
    break;
  default:
    break;
  }

  return false;
}

#else

bool SpinButtonAction::on_key_press_event(GdkEventKey* event)
{
  std::cout << "SpinButtonAction::on_key_press_event: " << event->keyval << std::endl;
  switch (event->keyval) {
  case GDK_KEY_Escape: // Defocus
    std::cout << "ESC" << std::endl;
    set_value(saved_value);
    defocus();
    return true;
    break;
  case GDK_KEY_Return:
    std::cout << "Return" << std::endl;
    defocus();
    break;
  case GDK_KEY_z:
  case GDK_KEY_Z:
    if (event->state & GDK_CONTROL_MASK) {
      std::cout << "Ctrl-Z" << std::endl;
      set_value(saved_value);
      return true;
    }
    break;
  default:
    break;
  }

  return parent_type::on_key_press_event(event); // We've overridden parent class's function.
}
#endif

#if GTK_CHECK_VERSION(4, 0, 0)

void SpinButtonAction::on_enter()
{
  saved_value = get_adjustment()->get_value();
}

#else

bool SpinButtonAction::on_focus_in_event(GdkEventFocus* event)
{
  saved_value = get_adjustment()->get_value();
  return parent_type::on_focus_in_event(event); // We've overridden parent class's function.
}

#endif

#if GTK_CHECK_VERSION(4, 0, 0)
void SpinButtonAction::on_changed()
{
  std::cout << "  on_changed" << std::endl;
}

int SpinButtonAction::on_input(double& new_value)
{
  std::cout << "SpinButtonAction::on_input: " << get_value() << " " << get_text() << std::endl;
  new_value = std::stod(get_text());
  return true;
}
#else
int SpinButtonAction::on_input(double* new_value)
{
  std::cout << "SpinButtonAction::on_input: " << get_value() << " " << get_text() << std::endl;
  *new_value = std::stod(get_text());
  return true;
}
#endif

bool SpinButtonAction::on_output()
{
  // Play with output for testing.
  std::cout << "SpinButtonAction::on_output! " << get_value() << std::endl;
  std::stringstream stream;
  stream << std::fixed << std::setprecision(property_digits()) << get_value();
  Glib::ustring text = stream.str() + "°";
  set_text(text);
  return true;
}

void SpinButtonAction::on_value_changed()
{
  std::cout << "SpinButtonAction::on_value_changed! " << get_value() << std::endl;

#if GTK_CHECK_VERSION(4, 0, 0)
  // Gtk4 will allow action to be activated in this widget or any ancestor widget.
  // But we need to determine the variant type any way... this just shows that it works.
  activate_action("app.test1");
#endif

  if (!action) {
    std::cerr << "SpinButtonAction::on_value_changed: missing action!" << std::endl;
    return;
  }

  if (type_string == "d") {

    // Double
    Glib::Variant<double> d = Glib::Variant<double>::create(get_value()); 
    action->activate(d);

  } else if (type_string == "i") {

    // Integer
    Glib::Variant<int> i = Glib::Variant<int>::create(get_value()); 
    action->activate(i);

  } else {
    std::cerr << "SpinButtonAction:on_value_changed: unhandled type: " << type_string << std::endl;
  }
}

// Update the SpinButton if something other than a SpinButton calls the action (e.g. via a normal Button).
// (Spin buttons that share the same adjustment update automatically.)
void SpinButtonAction::on_action_value_changed(const Glib::VariantBase& parameter)
{
  std::cout << "SpinButtonAction::on_action_value_changed!" << std::endl;

  if (type_string == "d") {

    // Double
    Glib::Variant<double> d = Glib::VariantBase::cast_dynamic<Glib::Variant<double> >(parameter);
    std::cout << "set value: Before" << std::endl;
    set_value(d.get());
    std::cout << "set value: After" << std::endl;


  } else if (type_string == "i") {

    // Integer
    Glib::Variant<int> i = Glib::VariantBase::cast_dynamic<Glib::Variant<int> >(parameter);
    set_value(i.get());

  } else {
    std::cerr << "SpinButtonAction:on_value_changed: unhandled type: " << type_string << std::endl;
  }
}

#endif // SIMPLE_WIDGETS_H


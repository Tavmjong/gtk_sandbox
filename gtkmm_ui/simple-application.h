#ifndef SIMPLE_APPLICATION_H
#define SIMPLE_APPLICATION_H

#include <gtkmm.h>
#include <iostream>


class SimpleApplication : public Gtk::Application
{
 protected:
  SimpleApplication();

 public:
  static Glib::RefPtr<SimpleApplication> create();

 protected:
  void on_startup()  override;
  void on_activate() override;

  void on_test1(std::string tag);
  void on_test2();
  void on_test3( int value );
  void on_test4( Glib::ustring value );
  void on_test5( Glib::VariantBase value );
  void on_test5_wrap();
  void on_test6( Glib::VariantBase value );
  void on_set_icon_size( Glib::VariantBase value );

  void reset();
  void disable();

  void on_no_widget();


private:
  Gtk::ApplicationWindow* create_window();
  void on_window_hide(Gtk::Window* window);
  void set_tooltip_recurse(Gtk::Widget* widget);

#if GTK_CHECK_VERSION(4, 0, 0)
  void fix_menuicons(Gtk::PopoverMenu *menu);
  void fix_menuicons_recurse(Gtk::Widget *widget);
#else
  void fix_menuicons(Gtk::MenuShell *menu);
  void fix_menuicons_recurse(Gtk::MenuShell *menu);
#endif

  Glib::RefPtr<Gtk::Builder> m_refBuilder;

  Glib::RefPtr<Gio::SimpleAction> m_test1;
  Glib::RefPtr<Gio::SimpleAction> m_test2;
  Glib::RefPtr<Gio::SimpleAction> m_test3;
  Glib::RefPtr<Gio::SimpleAction> m_test4;
  Glib::RefPtr<Gio::SimpleAction> m_test5;
  Glib::RefPtr<Gio::SimpleAction> m_test6;
  Glib::RefPtr<Gio::SimpleAction> m_icon;

  Glib::RefPtr<Gtk::Adjustment> m_adjustment0;
  Glib::RefPtr<Gio::SimpleAction> m_reset;
  Glib::RefPtr<Gio::SimpleAction> m_disable;

  Glib::RefPtr<Gtk::ComboBox> m_combobox1;

#if GTK_CHECK_VERSION(4, 0, 0)
#else
  Gtk::Menu m_menu_dialog;
#endif
};

#endif // SIMPLE_APPLICATION_H


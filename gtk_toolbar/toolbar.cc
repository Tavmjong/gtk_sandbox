
#include "toolbar.h"

#include <iostream>
#include <algorithm>

void dump_widget(Gtk::Widget &widget, int level = 0) {
  for (int i = 0; i < level; ++i) {
    std::cout << "  ";
  }
  std::cout << widget.get_name() << std::endl;
#if GTK_CHECK_VERSION(4, 0, 0)
  for (auto child = widget.get_first_child(); child != nullptr; child = child->get_next_sibling()) {
    dump_widget(*child, level+1);
  }
#else
  if (auto container = dynamic_cast<Gtk::Container*>(&widget)) {
    for (auto child : container->get_children()) {
      dump_widget(*child, level+1);
    }
  }
#endif
}

Gtk::Orientation
toolbar_orthogonal (Gtk::Orientation orientation) {
#if GTK_CHECK_VERSION(4, 0, 0)
  if (orientation == Gtk::Orientation::HORIZONTAL) {
    return Gtk::Orientation::VERTICAL;
  } else {
    return Gtk::Orientation::HORIZONTAL;
  }
#else
  if (orientation == Gtk::ORIENTATION_HORIZONTAL) {
    return Gtk::ORIENTATION_VERTICAL;
  } else {
    return Gtk::ORIENTATION_HORIZONTAL;
  }
#endif
}

// A box which has two states, either of:
// - my_button is hidden,  my_box is child of ToolItem.
// - my_button is visible, my_box is child of my_popover.
ToolItem::ToolItem()
  : Glib::ObjectBase("ToolItem")
  , prop_priority(*this, "priority", 0)
  , prop_icon_name(*this, "icon-name", "")
{
  set_name("ToolItem");
  my_popover.set_name("ToolItemPopover");
  my_button.set_name("ToolItemButton");
  my_box.set_name("ToolItemBox");

  my_button.set_popover(my_popover);
  my_button.set_label("V");
  my_button.set_visible(false);

#if GTK_CHECK_VERSION(4, 0, 0)
  my_button.set_parent(*this);
  my_box.set_parent(*this);
#else
  my_button.set_no_show_all();
  add(my_button);
  add(my_box);
  my_popover.set_modal(false);
  show_all();
#endif

#if GTK_CHECK_VERSION(4, 0, 0)
  // For managed widgets.
  signal_destroy().connect(sigc::mem_fun(*this, &ToolItem::on_container_destroy));
#endif
}

ToolItem::ToolItem(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refbuilder)
  : Glib::ObjectBase("ToolItem")
#if GTK_CHECK_VERSION(4, 0, 0)
  , Gtk::Widget(cobject)
#else
  , Gtk::Box(cobject)
#endif
  , prop_priority(*this, "priority", 0)
  , prop_icon_name(*this, "icon-name", "")
{
  set_name("ToolItem-Builder");
  my_popover.set_name("ToolItemPopover-Builder");
  my_button.set_name("ToolItemButton-Builder");
  my_box.set_name("ToolItemBox-Builder");

  my_button.set_popover(my_popover);
  my_button.set_label("V");
  my_button.set_visible(false);

  // Call after set_label().
  if (!prop_icon_name.get_value().empty()) {
    set_icon_name(prop_icon_name.get_value());
  }

#if GTK_CHECK_VERSION(4, 0, 0)
  // Move child widgets to box.
  while (Gtk::Widget* child = get_first_child()) {
    g_object_ref(G_OBJECT(child->gobj()));
    child->unparent();
    my_box.append(*child);
    g_object_unref(G_OBJECT(child->gobj()));
  }
  my_button.set_parent(*this);
  my_box.set_parent(*this);
#else
  auto children = get_children();
  for (auto child : children) {
    child->reparent(my_box);
  }
  my_button.set_no_show_all();
  add(my_button);
  add(my_box);
  my_popover.set_modal(false);
  show_all();
#endif

  set_priority(get_property_priority());

#if GTK_CHECK_VERSION(4, 0, 0)
  // For managed widgets.
  signal_destroy().connect(sigc::mem_fun(*this, &ToolItem::on_container_destroy));
#endif
}

#if GTK_CHECK_VERSION(4, 0, 0)
ToolItem::~ToolItem()
{
  // If managed, C object is already destructed.
  if (!gobj()) return;

  on_container_destroy();
}

void
ToolItem::on_container_destroy()
{
  while (Widget* child = get_first_child()) {
    child->unparent();
  }
}
#else
#endif

void
ToolItem::add_to_box(Gtk::Widget& widget)
{
#if GTK_CHECK_VERSION(4, 0, 0)
    my_box.append(widget);
#else
    my_box.add(widget);
#endif
}

void
ToolItem::set_use_popover(bool use_popover)
{
  if (use_popover) {
    if (!is_popover) {
#if GTK_CHECK_VERSION(4, 0, 0)
      my_box.unparent();
      my_popover.set_child(my_box); // Must be balanced with unset_child!
      my_box.set_visible(true);
#else
      my_box.reparent(my_popover);
#endif
      my_button.set_visible(true);
      is_popover = true;
    }
  } else {
    if (is_popover) {
#if GTK_CHECK_VERSION(4, 0, 0)
      my_popover.unset_child();
      my_box.set_parent(*this);
#else
      my_box.reparent(*this);
#endif
      my_button.set_visible(false);
      is_popover = false;
    }
  }
}

void
ToolItem::set_icon_name(Glib::ustring name) {
  icon_name = name;
#if GTK_CHECK_VERSION(4, 0, 0)
    my_button.set_icon_name(icon_name);
#else
    my_button.set_image_from_icon_name(icon_name);
    my_button.set_label("");
#endif
}

void
ToolItem::set_orientation(Gtk::Orientation new_orientation)
{
  orientation = new_orientation;
  my_box.set_orientation(orientation);
#if GTK_CHECK_VERSION(4, 0, 0)
  minimum_height = 0;
  minimum_width = 0;
#else
  // Not needed.
#endif

  // Remeasure!
  my_box.queue_resize();
  queue_resize();
}

#if GTK_CHECK_VERSION(4, 0, 0)
void
ToolItem::measure_vfunc(Gtk::Orientation orientation, int for_size,
                        int &minimum, int &natural,
                        int &minimum_baseline, int &natural_baseline) const
{
  Gtk::Requisition req_minimum;
  Gtk::Requisition req_natural;
  Gtk::Requisition req_not_used;

  if (orientation == get_orientation()) {
    my_button.get_preferred_size(req_minimum, req_not_used);
    my_box.get_preferred_size(req_not_used, req_natural);
    if (orientation == Gtk::Orientation::HORIZONTAL) {
      minimum = req_minimum.get_width();
      natural = req_natural.get_width();
      if (minimum == 0) {
        minimum = minimum_width;
      }
    } else {
      minimum = req_minimum.get_height();
      natural = req_natural.get_height();
      if (minimum == 0) {
        minimum = minimum_height;
      }
    }
  } else {
    for (auto child = get_first_child(); child; child = child->get_next_sibling()) {
      if (!child->is_visible()) continue;
      child->get_preferred_size(req_minimum, req_natural);
      if (orientation == Gtk::Orientation::HORIZONTAL) {
        minimum = std::max(minimum, req_minimum.get_width());
        natural = std::max(natural, req_natural.get_width());
      } else {
        minimum = std::max(minimum, req_minimum.get_height());
        natural = std::max(natural, req_natural.get_height());
      }
    }
  }

  // Sanity:
  if (minimum > natural) {
    std::cerr << "ToolItem::measure_vfunc: minimum > natural!"
              << " minimum: " << minimum
              << " natural: " << natural << std::endl;
    minimum = natural;
  }
}

void
ToolItem::size_allocate_vfunc(int width, int height, int baseline)
{
  int current_x = 0;
  int current_y = 0;
  for (auto child = get_first_child(); child; child = child->get_next_sibling()) {
    if (!child->is_visible()) continue;

    // Save button dimension when visible.
    if (dynamic_cast<Gtk::MenuButton*>(child)) {
      int minimum = 0;
      int natural = 0;
      int minimum_baseline = 0;
      int natural_baseline = 0;
      child->measure(get_orientation(), -1, minimum, natural, minimum_baseline, natural_baseline);
      if (get_orientation() == Gtk::Orientation::HORIZONTAL) {
        minimum_width = minimum;
      } else {
        minimum_height = minimum;
      }
    }

    auto child_allocation = child->get_allocation();

    Gtk::Requisition req_minimum;
    Gtk::Requisition req_natural;
    child->get_preferred_size(req_minimum, req_natural);

    child_allocation.set_x(current_x);
    child_allocation.set_y(current_y);
    child_allocation.set_x(current_x);
    child_allocation.set_y(current_y);
    if (get_orientation() == Gtk::Orientation::HORIZONTAL) {
      child_allocation.set_width(req_natural.get_width());
      child_allocation.set_height(height);
      current_x += req_natural.get_width();
    } else {
      child_allocation.set_height(req_natural.get_height());
      child_allocation.set_width(width);
      current_y += req_natural.get_height();
    }

    child->size_allocate(child_allocation, -1);
  }
}

#else

void
ToolItem::get_preferred_width_vfunc(int &minimum_width, int &natural_width) const
{
  int minimum_width_button = 0;
  int natural_width_button = 0;
  int minimum_width_box = 0;
  int natural_width_box = 0;

  my_button.get_preferred_width(minimum_width_button, natural_width_button);
  my_box.get_preferred_width(   minimum_width_box,    natural_width_box);
  if (get_orientation() == Gtk::ORIENTATION_HORIZONTAL) {
    // Minimum width is minimum width of button.
    // Natural width is natural width of box.
    minimum_width = minimum_width_button;
    natural_width = natural_width_box;
  } else {
    // REVISIT
    minimum_width = std::max(minimum_width_button, minimum_width_box);
    natural_width = std::max(natural_width_button, natural_width_box);
  }
}

void
ToolItem::get_preferred_height_vfunc(int &minimum_height, int &natural_height) const
{
  int minimum_height_button = 0;
  int natural_height_button = 0;
  int minimum_height_box = 0;
  int natural_height_box = 0;

  my_button.get_preferred_height(minimum_height_button, natural_height_button);
  my_box.get_preferred_height(   minimum_height_box,    natural_height_box);
  if (get_orientation() == Gtk::ORIENTATION_VERTICAL) {
    // Minimum height is minimum height of button.
    // Natural height is natural height of box.
    minimum_height = minimum_height_button;
    natural_height = natural_height_box;
  } else {
    // REVISIT
    minimum_height = std::max(minimum_height_button, minimum_height_box);
    natural_height = std::max(natural_height_button, natural_height_box);
  }
}          
#endif

void
ToolItem::on_map()
{
  // Track parents orientation.
  auto parent = dynamic_cast<Toolbar*>(get_parent());
  if (parent) {
    set_orientation(parent->get_orientation());
  } else {
    std::cerr << "ToolItem::on_map: parent is not Toolbar" << std::endl;
  }
  Gtk::Widget::on_map();
}

Toolbar::Toolbar(Gtk::Orientation orientation)
  : Glib::ObjectBase("Toolbar")
#if GTK_CHECK_VERSION(4, 0, 0)
  , prop_orientation(*this, "orientation", Gtk::Orientation::HORIZONTAL)
#else
  , prop_orientation(*this, "orientation", Gtk::ORIENTATION_HORIZONTAL)
#endif
{
  set_name("Toolbar");
  set_hexpand(true);
  set_orientation(orientation);

#if GTK_CHECK_VERSION(4, 0, 0)
  // For managed widgets.
  signal_destroy().connect(sigc::mem_fun(*this, &Toolbar::on_container_destroy));
#endif
}

Toolbar::Toolbar(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refbuilder)
#if GTK_CHECK_VERSION(4, 0, 0)
  : Gtk::Widget(cobject)
#else
  : Gtk::Box(cobject)
#endif
  , Glib::ObjectBase("Toolbar")
#if GTK_CHECK_VERSION(4, 0, 0)
  , prop_orientation(*this, "orientation", Gtk::Orientation::HORIZONTAL)
#else
  , prop_orientation(*this, "orientation", Gtk::ORIENTATION_HORIZONTAL)
#endif
{
  set_name("Toolbar-Builder");
  set_hexpand(true);
  set_orientation(get_property_orientation());

#if GTK_CHECK_VERSION(4, 0, 0)
  // For managed widgets.
  signal_destroy().connect(sigc::mem_fun(*this, &Toolbar::on_container_destroy));
#endif
}

#if GTK_CHECK_VERSION(4, 0, 0)
Toolbar::~Toolbar()
{
  // If managed, C object is already destructed.
  if (!gobj()) return;

  on_container_destroy();
}

void
Toolbar::on_container_destroy()
{
  while (Widget* child = get_first_child()) {
    child->unparent();
  }
}

void
Toolbar::append(Gtk::Widget& widget)
{
  widget.set_parent(*this);
}
#else
#endif

void
Toolbar::set_orientation(Gtk::Orientation orient) {
  orientation = orient;
#if GTK_CHECK_VERSION(4, 0, 0)
  for(auto child = get_first_child(); child; child = child->get_next_sibling()) {
    if (auto toolitem = dynamic_cast<ToolItem*>(child)) {
      toolitem->set_orientation(orientation);
    }
    else if (auto orientable = dynamic_cast<Gtk::Orientable*>(child)) {
      orientable->set_orientation(orient);
    }
  }
#else
  for(auto child : get_children()) {
    if (auto toolitem = dynamic_cast<ToolItem*>(child)) {
      toolitem->set_orientation(orientation);
      std::cout << "Set orientation: " << toolitem->get_name() << std::endl;
    }
    else if (auto orientable = dynamic_cast<Gtk::Orientable*>(child)) {
      orientable->set_orientation(orient);
      std::cout << "Set orientation: " << child->get_name() << std::endl;
    } else {
      std::cout << "Not set orientation: " << child->get_name() << std::endl;
    }
  }
#endif

  // Remeasure!
  queue_resize();
}


#if GTK_CHECK_VERSION(4, 0, 0)
void
Toolbar::measure_vfunc(Gtk::Orientation orientation, int for_size,
                       int &minimum, int &natural,
                       int &minimum_baseline, int &natural_baseline) const
{
  int minimum_size = 0;
  int natural_size = 0;

  for(auto child = get_first_child(); child; child = child->get_next_sibling()) {
    int child_minimum = 0;
    int child_natural = 0;
    int child_baseline_minimum = 0;
    int child_baseline_natural = 0;

    child->measure(orientation, for_size,
                   child_minimum, child_natural,
                   child_baseline_minimum, child_baseline_natural);

    if (orientation == get_orientation()) {
      minimum_size += child_minimum;
      natural_size += child_natural;
    } else {
      minimum_size = std::max(minimum_size, child_minimum);
      natural_size = std::max(natural_size, child_natural);
    }
  }
  minimum = minimum_size;
  natural = natural_size;
}

void
Toolbar::size_allocate_vfunc(int width, int height, int baseline)
{
  // Sum up required space; make list of toolitem children.
  int req_minimum_sum = 0;
  int req_natural_sum = 0;
  std::vector<ToolItem*> toolitems;

  for(auto child = get_first_child(); child; child = child->get_next_sibling()) {

    if (!child->is_visible()) continue;

    Gtk::Requisition req_minimum;
    Gtk::Requisition req_natural;
    child->get_preferred_size(req_minimum, req_natural);

    if (get_orientation() == Gtk::Orientation::HORIZONTAL) {
      req_minimum_sum += req_minimum.get_width();
      req_natural_sum += req_natural.get_width();
    } else {
      req_minimum_sum += req_minimum.get_height();
      req_natural_sum += req_natural.get_height();
    }

    auto toolitem = dynamic_cast<ToolItem*>(const_cast<Gtk::Widget*>(child));
    if (toolitem) {
      toolitems.push_back(toolitem);
    }
  }

  // Sort toolitems by priority.
  std::sort(toolitems.begin(), toolitems.end(), [](ToolItem* const& a, ToolItem* const& b)
  {
    return a->get_priority() < b->get_priority();
  });

  // How much space do we need?
  int deficit = 0;
  if (get_orientation() == Gtk::Orientation::HORIZONTAL) {
    deficit = req_natural_sum - width;
  } else {
    deficit = req_natural_sum - height;
  }

  // Enable popovers until we fit allocated space.
  for (auto toolitem : toolitems) {
    if (deficit <= 0) {
      toolitem->set_use_popover(false);
    } else {
      toolitem->set_use_popover(true);
      Gtk::Requisition req_minimum;
      Gtk::Requisition req_natural;
      toolitem->get_preferred_size(req_minimum, req_natural);
      if (get_orientation() == Gtk::Orientation::HORIZONTAL) {
        deficit -= (req_natural.get_width() - req_minimum.get_width());
      } else {
        deficit -= (req_natural.get_height() - req_minimum.get_height());
      }
    }
  }

  // Place children.
  int current_x = 0;
  int current_y = 0;
  for(auto child = get_first_child(); child; child = child->get_next_sibling()) {

    if (!child->is_visible()) continue;

    auto child_allocation = child->get_allocation();
    child_allocation.set_x(current_x);
    child_allocation.set_y(current_y);

    Gtk::Requisition req_minimum;
    Gtk::Requisition req_natural;
    child->get_preferred_size(req_minimum, req_natural);

    auto toolitem = dynamic_cast<ToolItem*>(child);

    if (get_orientation() == Gtk::Orientation::HORIZONTAL) {
      child_allocation.set_height(height);

      if (toolitem && toolitem->get_is_popover()) {
        child_allocation.set_width(req_minimum.get_width());
        current_x += req_minimum.get_width();
      } else {
        child_allocation.set_width(req_natural.get_width());
        current_x += req_natural.get_width();
      }

    } else {
      child_allocation.set_width(width);

      if (toolitem && toolitem->get_is_popover()) {
        child_allocation.set_height(req_minimum.get_height());
        current_y += req_minimum.get_height();
      } else {
        child_allocation.set_height(req_natural.get_height());
        current_y += req_natural.get_height();
      }
    }

    child->size_allocate(child_allocation, -1);
  }
}

#else  

void
Toolbar::get_preferred_width_vfunc(int &minimum_width, int &natural_width) const
{
  int minimum_size = 0;
  int natural_size = 0;

  for (auto child : get_children()) {
    int child_minimum = 0;
    int child_natural = 0;
    child->get_preferred_width(child_minimum, child_natural);
    if (get_orientation() == Gtk::ORIENTATION_HORIZONTAL) {
      minimum_size += child_minimum;
      natural_size += child_natural;
    } else {
      minimum_size = std::max(minimum_size, child_minimum);
      natural_size = std::max(natural_size, child_natural);
    }
  }

  minimum_width = minimum_size;
  natural_width = natural_size;
}          

void
Toolbar::get_preferred_height_vfunc(int &minimum_height, int &natural_height) const
{
  int minimum_size = 0;
  int natural_size = 0;

  for (auto child : get_children()) {
    int child_minimum = 0;
    int child_natural = 0;
    child->get_preferred_height(child_minimum, child_natural);
    if (get_orientation() == Gtk::ORIENTATION_VERTICAL) {
      minimum_size += child_minimum;
      natural_size += child_natural;
    } else {
      minimum_size = std::max(minimum_size, child_minimum);
      natural_size = std::max(natural_size, child_natural);
    }
  }

  minimum_height = minimum_size;
  natural_height = natural_size;
}

void
Toolbar::on_size_allocate(Gtk::Allocation& allocation)
{
  // Sum up required space; make list of toolitem children.
  int req_minimum_sum = 0;
  int req_natural_sum = 0;
  std::vector<ToolItem*> toolitems;

  for (auto child : get_children()) {

    if (!child->is_visible()) continue;

    Gtk::Requisition req_minimum;
    Gtk::Requisition req_natural;
    child->get_preferred_size(req_minimum, req_natural);

    if (get_orientation() == Gtk::ORIENTATION_HORIZONTAL) {
      req_minimum_sum += req_minimum.width;
      req_natural_sum += req_natural.width;
    } else {
      req_minimum_sum += req_minimum.height;
      req_natural_sum += req_natural.height;
    }

    auto toolitem = dynamic_cast<ToolItem*>(child);
    if (toolitem) {
      toolitems.push_back(toolitem);
    }
  }

  // Sort toolitems by priority.
  std::sort(toolitems.begin(), toolitems.end(), [](ToolItem* const& a, ToolItem* const& b)
  {
    return a->get_priority() < b->get_priority();
  });

  // How much space do we need?
  int deficit = 0;
  if (get_orientation() == Gtk::ORIENTATION_HORIZONTAL) {
    deficit = req_natural_sum - allocation.get_width();
  } else {
    deficit = req_natural_sum - allocation.get_height();
  }

  // Enable popovers until we fit allocated space.
  for (auto toolitem : toolitems) {
    if (deficit <= 0) {
      toolitem->set_use_popover(false);
    } else {
      toolitem->set_use_popover(true);
      Gtk::Requisition req_minimum;
      Gtk::Requisition req_natural;
      toolitem->get_preferred_size(req_minimum, req_natural);
      if (get_orientation() == Gtk::ORIENTATION_HORIZONTAL) {
        deficit -= (req_natural.width - req_minimum.width);
      } else {
        deficit -= (req_natural.height - req_minimum.height);
      }
    }
  }

  // Place children.
  int current_x = allocation.get_x(); // Start with parent geometry.
  int current_y = allocation.get_y();
  for (auto child : get_children()) {

    if (!child->is_visible()) continue;

    auto child_allocation = child->get_allocation();
    child_allocation.set_x(current_x);
    child_allocation.set_y(current_y);

    Gtk::Requisition req_minimum;
    Gtk::Requisition req_natural;
    child->get_preferred_size(req_minimum, req_natural);

    auto toolitem = dynamic_cast<ToolItem*>(child);

    if (get_orientation() == Gtk::ORIENTATION_HORIZONTAL) {
      child_allocation.set_height(allocation.get_height());

      if (toolitem && toolitem->get_is_popover()) {
        child_allocation.set_width(req_minimum.width);
        current_x += req_minimum.width;
      } else {
        child_allocation.set_width(req_natural.width);
        current_x += req_natural.width;
      }

    } else {
      child_allocation.set_width(allocation.get_width());

      if (toolitem && toolitem->get_is_popover()) {
        child_allocation.set_height(req_minimum.height);
        current_y += req_minimum.height;
      } else {
        child_allocation.set_height(req_natural.height);
        current_y += req_natural.height;
      }
    }

    child->size_allocate(child_allocation);
  }
}

#endif

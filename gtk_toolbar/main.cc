

#include <fstream>
#include <iostream>
#include <gtkmm.h>
#include "toolbar.h"

#if GTK_CHECK_VERSION(4, 0, 0)
static Gtk::Orientation ORIENTATION            = Gtk::Orientation::VERTICAL;
#else
static Gtk::Orientation ORIENTATION            = Gtk::ORIENTATION_VERTICAL;
#endif

Gtk::Orientation orthogonal(Gtk::Orientation orientation) {
#if GTK_CHECK_VERSION(4, 0, 0)
  return (orientation == Gtk::Orientation::HORIZONTAL)
    ? Gtk::Orientation::VERTICAL
    : Gtk::Orientation::HORIZONTAL;
#else
  return (orientation == Gtk::ORIENTATION_HORIZONTAL)
    ? Gtk::ORIENTATION_VERTICAL
    : Gtk::ORIENTATION_HORIZONTAL;
#endif
}

void change_orientation(Gtk::Box* box) {
  // Box orientation is orthoganal to toolbar orientations.
  auto new_orientation = box->get_orientation();

#if GTK_CHECK_VERSION(4, 0, 0)
  if (box->get_orientation() == Gtk::Orientation::HORIZONTAL) {
    box->set_orientation(Gtk::Orientation::VERTICAL);
  } else {
    box->set_orientation(Gtk::Orientation::HORIZONTAL);
  }
#else
  if (box->get_orientation() == Gtk::ORIENTATION_HORIZONTAL) {
    box->set_orientation(Gtk::ORIENTATION_VERTICAL);
  } else {
    box->set_orientation(Gtk::ORIENTATION_HORIZONTAL);
  }
#endif

#if GTK_CHECK_VERSION(4, 0, 0)
  for (auto child = box->get_first_child(); child != nullptr; child = child->get_next_sibling()) {
#else
  for (auto child : box->get_children()) {
#endif
    if (auto toolbar = dynamic_cast<Toolbar*>(child)) {
      toolbar->set_orientation(new_orientation);
    }
    else if (auto orientable = dynamic_cast<Gtk::Orientable*>(child)) {
      orientable->set_orientation(new_orientation);
    }
  }
}

void change_visibility(Gtk::Widget* widget) {
  widget->set_visible(!widget->get_visible());
}

Glib::RefPtr<Gtk::Builder> create_builder(const char* path)
{
  std::cout << "create_builder: " << path << std::endl;
  // For custom widgets, we must call get_widget_derived or only the base class is initialized.
  // We must do this before any ancestor in the builder file is initialized.

  // Extract the custom widgets out of the .ui file.
  std::fstream ui_file;
  ui_file.open(path, std::ios::in);

  if (!ui_file.is_open()) {
    std::cerr << "Coudn't read ui file '" << path << "'!!" << std::endl;
    // throw exception
  }

  auto builder = Gtk::Builder::create_from_file(path);

  static auto regex = Glib::Regex::create("\"gtkmm__CustomObject_(.*)\".*id=\"(.*)\"");
  Glib::MatchInfo match_info;
  std::string line;
  while (std::getline(ui_file, line)) {
    regex->match(line.c_str(), match_info); // c_str() needed for Gtk4
    if (match_info.matches()) {
      if (match_info.fetch(1) == "ToolItem") {
#if GTK_CHECK_VERSION(4, 0, 0)
        auto toolitem = Gtk::Builder::get_widget_derived<ToolItem>(builder, match_info.fetch(2));
#else
        ToolItem* toolitem= nullptr;
        builder->get_widget_derived(match_info.fetch(2), toolitem);
#endif
      }
    }
  }
  ui_file.close();

  return builder;
}

class Window: public Gtk::ApplicationWindow
{
public:
#if GTK_CHECK_VERSION(4, 0, 0)
  Window()
#else
  Window(const Glib::RefPtr<Gtk::Application>& app)
#endif
  {
    set_title("Window");

    // Register types
    static_cast<void>(Toolbar());
    static_cast<void>(ToolItem());

    auto toolbar = Gtk::make_managed<Toolbar>(ORIENTATION);

    for (int i = 0; i < 4; ++i) {

      // A collapsing widget.
      std::string label_text = " A wide widget: " + std::to_string(i);
      auto label = Gtk::make_managed<Gtk::Label>(label_text);

      auto button = Gtk::make_managed<Gtk::Button>();
#if GTK_CHECK_VERSION(4, 0, 0)
      button->set_icon_name("go-home");
#else
      button->set_image_from_icon_name("go-home");
#endif

      auto toolitem = Gtk::make_managed<ToolItem>();
      toolitem->set_name(label_text);
      toolitem->set_priority((i+2)%4);
      toolitem->set_icon_name("go-down");
      toolitem->add_to_box(*label);
      toolitem->add_to_box(*button);

#if GTK_CHECK_VERSION(4, 0, 0)
      toolbar->append(*toolitem);
#else
      toolbar->add(*toolitem);
#endif

      // A non-collapsing widget.
      label_text = "A fixed widget: " + std::to_string(i);
      label = Gtk::make_managed<Gtk::Label>(label_text);

#if GTK_CHECK_VERSION(4, 0, 0)
      toolbar->append(*label);
#else
      toolbar->add(*label);
#endif
    }

#if GTK_CHECK_VERSION(4, 0, 0)
    auto builder = create_builder("toolbar4.ui");
    auto toolbar2 = Gtk::Builder::get_widget_derived<Toolbar>(builder, "MyToolbar");

    auto buttonA = Gtk::make_managed<Gtk::Button>("Change Orientation");
    auto buttonB = Gtk::make_managed<Gtk::Button>("Toggle Toolbar 1");
    auto buttonC = Gtk::make_managed<Gtk::Button>("Toggle Toolbar 2");
    auto button_box = Gtk::make_managed<Gtk::Box>(ORIENTATION);
    button_box->append(*buttonA);
    button_box->append(*buttonB);
    button_box->append(*buttonC);

    auto box = Gtk::make_managed<Gtk::Box>(orthogonal(ORIENTATION));

    buttonA->signal_clicked().connect(sigc::bind(sigc::ptr_fun(change_orientation), box));
    buttonB->signal_clicked().connect(sigc::bind(sigc::ptr_fun(change_visibility), toolbar));
    buttonC->signal_clicked().connect(sigc::bind(sigc::ptr_fun(change_visibility), toolbar2));

    box->append(*button_box);
    box->append(*toolbar);
    box->append(*toolbar2);
    set_child(*box);

#else
    auto builder = create_builder("toolbar3.ui");
    Toolbar* toolbar2 = nullptr;
    builder->get_widget_derived("MyToolbar", toolbar2);

    auto buttonA = Gtk::make_managed<Gtk::Button>("Change Orientation");
    auto buttonB = Gtk::make_managed<Gtk::Button>("Toggle Toolbar 1");
    auto buttonC = Gtk::make_managed<Gtk::Button>("Toggle Toolbar 2");
    auto button_box = Gtk::make_managed<Gtk::Box>(ORIENTATION);
    button_box->add(*buttonA);
    button_box->add(*buttonB);
    button_box->add(*buttonC);

    auto box = Gtk::make_managed<Gtk::Box>(orthogonal(ORIENTATION));

    buttonA->signal_clicked().connect(sigc::bind(sigc::ptr_fun(change_orientation), box));
    buttonB->signal_clicked().connect(sigc::bind(sigc::ptr_fun(change_visibility), toolbar));
    buttonC->signal_clicked().connect(sigc::bind(sigc::ptr_fun(change_visibility), toolbar2));

    box->add(*button_box);
    box->add(*toolbar);
    box->add(*toolbar2);

    add(*box);

    show_all();
#endif
  }
};

int main(int argc, char **argv)
{

#if GTK_CHECK_VERSION(4, 0, 0)
  auto application = Gtk::Application::create("org.tavmjong.custom_mm4");
  return application->make_window_and_run<Window>(argc, argv);
#else
  auto application = Gtk::Application::create(argc, argv, "org.tavmjong.custom_mm3");
  Window window(application);
  return application->run(window);
#endif

}

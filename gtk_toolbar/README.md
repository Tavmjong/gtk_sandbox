
# A test of a toolbar that adapts to its width.

`g++ *.cc -o toolbar3  `pkg-config gtkmm-3.0  --cflags --libs` `

`g++ *.cc -o toolbar4  `pkg-config gtkmm-4.0  --cflags --libs` `

## Implements:

- Creating toolbar programatically.
- Creating toolbar from builder file.
    - Priority can be set by property.
    - MenuButton icon can be set by property.
    - Orientation of Toolbar can be set by property.
    - Children of ToolItems are automatically moved to a Box.
- Can set Toolbar orientation programmatically or in builder file (can change on fly).

## Strategy:

- A toolbar that contains toolitems which contain widgets that move between the
  toolbar and popovers, and normal widgets (that won't be moved to popovers).
- Toolitems contain three widgets:
    - A Box, where the widgets that move between the toolbar and popover are placed.
    - A MenuButton
    - A Popover
    Either the Box or the MenuButton are shown in the Toolbar. The Box is moved between
    the Toolbar and the Popover.
- The toolitem measure function returns the button width (height) for the "minimum"
  value and the toolbar width (height) for the "normal" value. The button width (height)
  is cached as with Gtk4, the button's width is reported as zero when it is hidden.

## To do:

- Maybe handle case where ToolItem box is smaller than MenuButton (in this case,
  there is no advantage to wrapping in a ToolItem so maybe just leave it).
- Might be some optimizations.

# Conclusions (so far):
## LayoutManager
- Must derive from LayoutManager:
    - Cannot override BoxLayout as virtual functions disabled.
    - The "widget" in `allocate_vfunc` is a const, so one cannot, without using `const_cast`,
      set the allocations of the children (pbs has filed bug report).
    - Deriving from Gtk::Orientable (`class ToolbarLayout : public Gtk::Orientable, public LayoutManger`)
      gives this error: `gtk_orientable_get_orientation: assertion 'GTK_IS_ORIENTABLE (orientable)' failed`
      Daniel shared a link that might lead to how to fix this: https://stackoverflow.com/a/5195408

## Toolbar
- Deriving the toolbar from Gtk::Box runs into problems:
    - Overriding virtual functions is disabled (can't customize `measure_vfunc` for example).
    - Replacing BoxLayout by a custom LayoutManager can work but ff one tries to do something
      like set the spacing, it results in the error:
     `gtk_box_layout_get_spacing: assertion 'GTK_IS_BOX_LAYOUT (box_layout)' fails.`
- Deriving from Gtk::Widget is better:
    - One can override the `measure` and `allocate` functions (and thus don't need a custom LayoutManager).
    - One has to handle orientation on one's own (or solve the use of Gtk::Orientable as listed above).

## Toolitem
- Better to derive from Gtk::Widget, then one can customize the measure function to return
    - the button width for the 'minimum'
    - the box with for 'normal'
    - The widths can be cached (for example, the button width will be reported
      by the button measure function as '0' when hidden, but we need the width when shown).

## Builder
- Getting a derived container builds the child widget's 'C' objects which prevents later calls to
  `get_widget_derived()` for the child widgets from being able to add the 'C++' functionality.
  This can be gotten around by searching the builder file for derived widgets and calling
  `get_widget_derived()` on the children before calling `get_widget_derived()` on the container.
  An ugly hack but it works.

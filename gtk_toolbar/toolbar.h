
#include <gtkmm.h>

// A box which has two states, either of:
// - my_box is visible child of ToolItem. my_button is hidden.
// - my_button is a visible child of ToolItem, my_box is child of my_popover.
// Orientation is taken from parent Toolbar.
#if GTK_CHECK_VERSION(4, 0, 0)
class ToolItem : public Gtk::Widget
#else
class ToolItem : public Gtk::Box
#endif
{
public:
  ToolItem();
  ToolItem(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refbuilder);
#if GTK_CHECK_VERSION(4, 0, 0)
  ~ToolItem() override;
  void on_container_destroy();
#else
#endif

  void add_to_box(Gtk::Widget& widget);
  bool const get_is_popover() { return is_popover; }
  void set_use_popover(bool use_popover);

  // Getters and setters.
  void set_orientation(Gtk::Orientation orient); // Should only be called by parent Toolbar.
  Gtk::Orientation get_orientation() const { return orientation; }
  void set_priority(int i) { priority = i; }
  int const get_priority() { return priority; }
  void set_icon_name(Glib::ustring icon_name);
  Glib::ustring get_icon_name() { return icon_name; }

  // Custom properties
  Glib::PropertyProxy<int> get_property_priority() { return prop_priority.get_proxy(); }
  Glib::PropertyProxy<Glib::ustring> get_property_icon_name() { return prop_icon_name.get_proxy(); }

  // Signal handlers.
  void on_map() override;

protected:
#if GTK_CHECK_VERSION(4, 0, 0)
  void measure_vfunc(Gtk::Orientation orientation, int for_size,
                     int &minimum, int &natural,
                     int &minimum_baseline, int &natural_baseline) const override;
  void size_allocate_vfunc(int width, int height, int baseline) override;
#else
  void get_preferred_width_vfunc( int &minimum_width,  int &natural_width)  const override;
  void get_preferred_height_vfunc(int &minimum_height, int &natural_height) const override;
#endif

private:
  Gtk::MenuButton my_button;
  Gtk::Popover my_popover;     // Popover, holds my_box if is_popover is true.
  Gtk::Box my_box;             // Contains widgets

  // State
  bool is_popover = false;

  // Custom properties
  int priority = 0;
  Glib::ustring icon_name;

  Glib::Property<int>           prop_priority;
  Glib::Property<Glib::ustring> prop_icon_name;

#if GTK_CHECK_VERSION(4, 0, 0)
  Gtk::Orientation orientation = Gtk::Orientation::HORIZONTAL;
  int minimum_width = 0;
  int minimum_height = 0;
#else
  Gtk::Orientation orientation = Gtk::ORIENTATION_HORIZONTAL;
#endif
};


// Keeps a list of ToolItems, changes state based on mininmum widths.
#if GTK_CHECK_VERSION(4, 0, 0)
class Toolbar : public Gtk::Widget
#else
class Toolbar : public Gtk::Box
#endif
{
public:
#if GTK_CHECK_VERSION(4, 0, 0)
  Toolbar(Gtk::Orientation orientation = Gtk::Orientation::HORIZONTAL);
  Toolbar(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refbuilder);
  ~Toolbar() override;
  void on_container_destroy();
  void append(Gtk::Widget& widget);

#else
  Toolbar(Gtk::Orientation orientation = Gtk::ORIENTATION_HORIZONTAL);
  Toolbar(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refbuilder);
#endif

  void set_orientation(Gtk::Orientation orient);
  Gtk::Orientation get_orientation() const { return orientation; }

  // Custom properties
  Glib::PropertyProxy<Gtk::Orientation> get_property_orientation() { return prop_orientation.get_proxy(); }

protected:
#if GTK_CHECK_VERSION(4, 0, 0)
  void measure_vfunc(Gtk::Orientation orientation, int for_size,
                     int &minimum, int &natural,
                     int &minimum_baseline, int &natural_baseline) const override;
  void size_allocate_vfunc(int width, int height, int baseline) override;
#else  
  void get_preferred_width_vfunc( int &minimum_width,  int &natural_width)  const override;
  void get_preferred_height_vfunc(int &minimum_height, int &natural_height) const override;
  void on_size_allocate(Gtk::Allocation& allocation) override;
#endif


private:
#if GTK_CHECK_VERSION(4, 0, 0)
  Gtk::Orientation orientation = Gtk::Orientation::HORIZONTAL;
#else
  Gtk::Orientation orientation = Gtk::ORIENTATION_HORIZONTAL;
#endif
  Glib::Property<Gtk::Orientation> prop_orientation;
};
  

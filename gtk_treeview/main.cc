
#include <iostream>
#include <ostream>
#include <string>
#include <vector>

#include <gtkmm.h>

class MyColumns : public Gtk::TreeModel::ColumnRecord {
public:
  MyColumns() {
    add (col_name);
  }
  Gtk::TreeModelColumn<Glib::ustring> col_name;
};

class MyTreeStore : public Gtk::TreeStore
{
  bool row_draggable_vfunc(const Gtk::TreeModel::Path &path) const override
  {
    std::cout << "MyTreeStore::row_draggable" << std::endl;
    return true;
  }

  bool row_drop_possible_vfunc (const Gtk::TreeModel::Path &dest_path, const Glib::ValueBase &value) const override {
    std::cout << "MyTreeStore::row_drop_possible" << std::endl;
    return true;
  }
};

class Window: public Gtk::ApplicationWindow
{

public:
  Window()
  {
    set_title("TreeView Drag and Drop Example");

    /* STYLING */

    auto provider = Gtk::CssProvider::create();
    try {
      provider->load_from_path("treeview.css");
    }
    catch (...) {
    }
    auto const display = Gdk::Display::get_default();
    Gtk::StyleContext::add_provider_for_display(display, provider, GTK_STYLE_PROVIDER_PRIORITY_USER);

    static const std::vector<std::string> names{ "one", "two", "three", "four", "five"};
    static const std::vector<std::string> names2{ "A", "B", "C", "D", "E"};

    /* TREE STORE */

    MyColumns columns;
    auto tree_store = Gtk::ListStore::create(columns);
    tree_store->signal_row_deleted( ).connect([](auto)       { std::cout << "Row deleted"  << std::endl; });
    tree_store->signal_row_inserted().connect([](auto, auto) { std::cout << "Row inserted" << std::endl; });
    for (auto name : names) {
      auto row = *(tree_store)->append();
      row[columns.col_name] = name;
    }

    auto my_tree_store = MyTreeStore::create(columns);
    my_tree_store->signal_row_deleted( ).connect([](auto)       { std::cout << "Row deleted"  << std::endl; });
    my_tree_store->signal_row_inserted().connect([](auto, auto) { std::cout << "Row inserted" << std::endl; });
    for (auto name : names2) {
      auto row = *(my_tree_store)->append();
      row[columns.col_name] = name;
    }

    /* TREEVIEWS */

    auto treeview1 = Gtk::manage(new Gtk::TreeView(tree_store));
    treeview1->set_headers_visible(false);
    treeview1->set_vexpand(true);
    treeview1->append_column("Name", columns.col_name);

    auto treeview2 = Gtk::manage(new Gtk::TreeView(tree_store));
    treeview2->set_headers_visible(false);
    treeview2->set_vexpand(true);
    treeview2->append_column("Name", columns.col_name);

    auto treeview3 = Gtk::manage(new Gtk::TreeView(tree_store));
    treeview3->set_headers_visible(false);
    treeview3->set_vexpand(true);
    treeview3->append_column("Name", columns.col_name);

    auto treeview4 = Gtk::manage(new Gtk::TreeView(my_tree_store));
    treeview4->set_headers_visible(false);
    treeview4->set_vexpand(true);
    treeview4->append_column("Name", columns.col_name);

    /* DRAG AND DROP - REORDERABLE */
    treeview1->set_reorderable(true); // Uses drag_source and drag_dest.

    /* DRAG AND DROP - MODEL DRAG */
    treeview2->enable_model_drag_source();  // Sets reorderable false.
    treeview2->enable_model_drag_dest();

    /* DRAG AND DROP - CONTROLLERS */
    static bool BEFORE = false;
    static bool AFTER = true;
    treeview3->set_reorderable(true); // Uses drag_source and drag_dest.
    auto dragsource = Gtk::DragSource::create();
    dragsource->set_actions(Gdk::DragAction::MOVE);
    dragsource->signal_prepare(    ).connect([](auto, auto) -> Glib::RefPtr<Gdk::ContentProvider> {
        Glib::Value<Glib::ustring> value;
        value.init(G_TYPE_STRING);
        value.set("NotUsed");
        auto provider = Gdk::ContentProvider::create(value);
        return provider;
      }, BEFORE);
    dragsource->signal_drag_begin( ).connect([](auto)       { std::cout << "Drag begin"   << std::endl; });
    dragsource->signal_drag_end(   ).connect([](auto, auto) { std::cout << "Drag end"     << std::endl; });
    dragsource->signal_drag_cancel().connect([](auto, auto) -> bool { std::cout << "Drag cancel"  << std::endl; return false; }, BEFORE);
    treeview3->add_controller(dragsource);

    auto droptarget = Gtk::DropTarget::create(Glib::Value<Glib::ustring>::value_type(), Gdk::DragAction::MOVE);
    droptarget->signal_drop().connect([](auto, auto, auto) -> bool { std::cout << "Drop" << std::endl; return false; }, BEFORE);
    treeview3->add_controller(droptarget);

    /* DRAG AND DROP - REORDERABLE - CUSTOM TREEMODEL */
    treeview4->enable_model_drag_source();  // Sets reorderable false.
    treeview4->enable_model_drag_dest();

    /* FINISH UP */

    auto box = Gtk::manage(new Gtk::Box);
    box->append(*treeview1);
    box->append(*treeview2);
    box->append(*treeview3);
    box->append(*treeview4);
    set_child(*box);
  }
};


int main(int argc, char *argv[]) {

  auto application = Gtk::Application::create("org.tavmjong.template");
  return application->make_window_and_run<Window>(argc, argv);

  return 0;
}

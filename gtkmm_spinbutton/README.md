---
title: Custom SpinButton
---

A comparison of GTK3 and GTK4 with three types of SpinButtons:

1. Default GTK SpinButton.
2. SpinButton derived from Gtk::SpinButton.
3. SpinButton created from scratch.

Goal is to implement a custom spinbutton per https://gitlab.com/inkscape/ux/-/issues/25

To do:
1. Split into separate .h/.cpp files.
2. Tune presentation via CSS.

# Default GTK SpinButton #

## GTK3 ##

* No vertical mode.
* +/- buttons: Default behavior:
** Left click: single step.
** Middle click: page step.
** Right click: to max/min value.

## GTK4 ##

* Same as GTK3 except vetical mode exists ('+' button on top, '-' button on bottom).
* Composed of '+' Gtk::Button, '-' Gtk::Button, and entry widget Gtk::Text.

# SpinButton derived from Gtk::SpinButton

## GTK3 ##

Essentially same as Gtk::SpinButton.

* No vertical mode.
* Cannot rearrange +/- buttons.
* Cannot hide/show +/- buttons.
* Cannot add dragging to change value (would interfere with editing value).
* No attempt made to modify behaviour from Gtk::SpinButton.

## GTK4 ##

* Vertical mode. (But themes are less developed for vertical mode.)
* Widget is composed of three child widgets that can be rearranged and shown/hidden individually.
* The Gtk::Text widget can be hidden and a Gtk::Label widget added and shown in its place. This allows separating text entry (Gtk::Text widget) and dragging to change value (Gtk::Label). See comment below about "controllers".

# SpinButton created from Scratch

## GTK3 ##

* SpinButton created by wrapping three Gtk::Buttons and a Gtk::Entry inside a Gtk::Box inside a Gtk::EventBox in the order:
*# '-' Button
*# 'Value' Button inside a Gtk::EventBox (to receive signals for changing cursor).
*# 'Entry' (Gtk::Entry)
*# '+' Button.
* The widgets are shown in one of three configurations:
*# 'Value' only. (When the mouse cursor is outside of the SpinButton.)
*# '-' 'Value' '+' (When cursor is inside of SpinButton and Entry not activated, allows for dragging.)
*# 'Entry' only. (When Entry mode is activated, allowing text to be entered and editted.)
* The event box is needed as the Gtk::Box does not support signals. A signal is used to hide/show the '+'/'-' buttons witht the cursor enters/leaves the SpinButton.
* Dragging to change value cannot use modifier keys to modify step size (due to deficiency in GTK3 controller implementation).

## GTK4 ##

* Same widget arrangement as for GTK3 except there is no Gtk::EventBox as "controllers" can be added directly to the Gtk::Box.

# Question on dragging #

How important is being able to drag to set values? This adds quite a bit of complexity inorder to prevent dragging and text editing from interfering with each other. Do we really need four different ways to change the SpinButton's value?

# Note on Gtk::Controllers #

During the lifetime of GTK3, "controllers" were introduced. Controllers basically encapsulate various signal mechanisms which can then be added to any widget. Controller support in GTK3 is limited to "Gestures". In GTK4 most GUI interaction signals have been replaced by controllers. There are some significant differences in API between the GTK3 controllers and those in GTK4. It seems the GTK3 effort was more of a trial implementation. GTK3 controllers are missing an API to monitor modifier keys.

Any controller can be added to any widget but some widgets have built in controllers. There is not an easy way to disable or removed built in controllers (I managed to remove four of the eight built in controllers in the Gtk::Text widget). This means that it is not practical to use the Gtk::Text widget inside the GTK4 Gtk::SpinButton widget. Also, in GTK3, the Gtk::Button uses a controller to handle first mouse button presses. This cannot be turned off and results in the signal_pressed signal not being triggered for the first mouse button. One can work around this by adding a controller to the button.

# Note on Gtk4 #

GTK4 offers several advantages over GTK3:

* Easy to access "child" widgets in composite widgets.
* By default all widgets are shown, thus it is easier to control visibility of children of composite widgets at construction time.
* Easy to add controllers to any widget. No need for EventBox. Can add on "Capture" or "Bubble" phase. "Capture" phase is very practical to capture signals that should apply to the whole widget when a widget is composed of child widgets.

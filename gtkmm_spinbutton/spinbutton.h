
#include <gtkmm.h>

namespace Ink {

#if GTK_CHECK_VERSION(4, 0, 0)
  // Work-around do to missing signal_activate() in GTK4.
  void on_activate_c(GtkEntry* entry, gpointer user_data);
#endif

  class SpinButton : public Gtk::SpinButton {

    using parent_type = Gtk::SpinButton;

  public:
#if GTK_CHECK_VERSION(4, 0, 0)
    SpinButton(Gtk::Orientation orientation = Gtk::Orientation::HORIZONTAL);
#else
    SpinButton(); // GTK3 only has horizontal spinbuttons.
#endif
    virtual ~SpinButton() {};

  private:

#if GTK_CHECK_VERSION(4, 0, 0)
    Gtk::Widget* m_minus = nullptr;
    Gtk::Widget* m_plus  = nullptr;
    Gtk::Text*   m_text  = nullptr;
    Gtk::Button* m_value = nullptr;

    Glib::RefPtr<Gdk::Cursor> m_old_cursor;
#endif

#if GTK_CHECK_VERSION(4, 0, 0)

    // ------------- CONTROLLERS -------------
    // Only Gestures are available in GTK3 (and not GestureClick).
    // We'll rely on signals for GTK3.

    Glib::RefPtr<Gtk::EventControllerMotion> m_motion;
    void on_motion_enter(double x, double y);
    void on_motion_leave();

    Glib::RefPtr<Gtk::EventControllerMotion> m_motion_value;
    void on_motion_enter_value(double x, double y);
    void on_motion_leave_value();

    Glib::RefPtr<Gtk::GestureDrag> m_drag_value;
    void on_drag_begin_value(Gdk::EventSequence *sequence);
    void on_drag_update_value(Gdk::EventSequence *sequence);
    void on_drag_end_value(Gdk::EventSequence *sequence);

    Glib::RefPtr<Gtk::EventControllerScroll> m_scroll;
    void on_scroll_begin(); // Not used with mouse wheel.
    bool on_scroll(double dx, double dy);
    void on_scroll_end(); // Not used with mouse wheel.

    // -------------  CALLBACKS  -------------

    void on_changed(); // This does NOT override default handler in GTK4 (even if declared to)!

  public:
    void on_activate(); // Needs to be public for GTK4 work-around.
  private:
    void on_value_clicked();

    Glib::RefPtr<Gio::ListModel> controllers_text;

#else
#endif

    // ---------------- DATA ------------------
    double m_initial_value = 0.0; // Value of adjustment at start of drag.
    bool m_dragged = false; // Hack to avoid enabling text widget after drag.
  };

#if GTK_CHECK_VERSION(4, 0, 0)
  SpinButton::SpinButton(Gtk::Orientation orientation)
#else
  SpinButton::SpinButton()
#endif
    : Gtk::SpinButton()
  {
    set_name("Ink::SpinButton");

#if GTK_CHECK_VERSION(4, 0, 0)
    set_orientation(orientation);

    m_value = Gtk::manage(new Gtk::Button("TEMP"));
    m_value->set_name("Ink::SpinButton::Value");
    m_value->set_vexpand(true);
    m_value->set_hexpand(true);

    if (get_orientation() == Gtk::Orientation::HORIZONTAL) {

      // Swap 'text' and '-' button.
      m_text = dynamic_cast<Gtk::Text*>(get_first_child());
      m_minus = m_text->get_next_sibling();
      m_plus = m_minus->get_next_sibling();
      m_minus->reference();
      m_minus->unparent();
      m_minus->insert_at_start(*this);
      m_minus->unreference();

      // Must also swap CSS "border" values.
      // auto css_minus = m_minus->get_style_context();
      // auto css_text  = m_text->get_style_context();
      // auto border_minus = css_minus->get_border();
      // auto border_text  = css_text->get_border();
      // border_minus.swap(border_text);

      m_text->set_halign(Gtk::Align::CENTER);
      m_value->set_halign(Gtk::Align::FILL);
      m_value->set_hexpand(true);

    } else {

      // Vertical is already in good postion for GTK4.
      m_plus = get_first_child();
      m_text = dynamic_cast<Gtk::Text*>(m_plus->get_next_sibling());
      m_minus = m_text->get_next_sibling();

      m_text->set_halign(Gtk::Align::FILL);
      m_value->set_halign(Gtk::Align::FILL);
    }

    m_value->insert_after(*this, *m_text);

    m_text->set_margin(0);
    m_value->set_margin(0);

    static Glib::RefPtr<Gtk::CssProvider> provider;
    if (!provider) {
      provider = Gtk::CssProvider::create();
      Glib::ustring css = R"=====(
          button { border: none; border-radius: 0px; }
      )=====";
      provider->load_from_data(css);

      Glib::RefPtr<Gtk::StyleContext> style_value = m_value->get_style_context();
      Glib::RefPtr<Gtk::StyleContext> style_text = m_text->get_style_context();

      style_value->add_provider(provider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
      style_text->add_provider(provider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    }

    //                  GTK4
    // ------------- CONTROLLERS -------------

    // Test to see if we can remove controllers.
    // auto controllers = observe_controllers();
    // std::cout << "Number of box controllers: " << controllers->get_n_items() << std::endl;

    // controllers_text = m_text->observe_controllers();
    // std::cout << "Number of text controllers: before: " << controllers_text->get_n_items() << std::endl;
    // for (uint i = 0; i < controllers_text->get_n_items(); ++i) {
    //   auto name = std::dynamic_pointer_cast<Gtk::EventController>(controllers_text->get_object(i))->get_name();
    //   std::cout << name << std::endl;
    //   //m_text->remove_controller(std::dynamic_pointer_cast<Gtk::EventController>(controllers_text->get_object(i)));
    // }
    // // This doesn't remove GtkEventControllerFocus, GtkEventControllerMotion, GtkGestureDrag, ShortcutController.
    // std::cout << "Number of text controllers:  after: " << controllers_text->get_n_items() << std::endl;

    // This is mouse movement. Shows/hides +/- buttons.
    m_motion = Gtk::EventControllerMotion::create();
    m_motion->signal_enter().connect(sigc::mem_fun(*this, &SpinButton::on_motion_enter));
    m_motion->signal_leave().connect(sigc::mem_fun(*this, &SpinButton::on_motion_leave));
    m_motion->set_propagation_phase(Gtk::PropagationPhase::CAPTURE);
    add_controller(m_motion);

    // This is mouse movement. Sets cursor.
    m_motion_value = Gtk::EventControllerMotion::create();
    m_motion_value->signal_enter().connect(sigc::mem_fun(*this, &SpinButton::on_motion_enter_value));
    m_motion_value->signal_leave().connect(sigc::mem_fun(*this, &SpinButton::on_motion_leave_value));
    m_motion_value->set_propagation_phase(Gtk::PropagationPhase::CAPTURE);
    m_value->add_controller(m_motion_value);

    // This is mouse drag movement. Changes value.
    m_drag_value = Gtk::GestureDrag::create();
    m_drag_value->signal_begin().connect(sigc::mem_fun(*this, &SpinButton::on_drag_begin_value));
    m_drag_value->signal_update().connect(sigc::mem_fun(*this, &SpinButton::on_drag_update_value));
    m_drag_value->signal_end().connect(sigc::mem_fun(*this, &SpinButton::on_drag_end_value));
    // Set on SpinButton + if CAPTURE, prevents +/- buttons working.
    m_drag_value->set_propagation_phase(Gtk::PropagationPhase::CAPTURE); 
    // m_text->add_controller(m_drag_value);
    add_controller(m_drag_value);

   // Changes value.
    m_scroll = Gtk::EventControllerScroll::create();
    m_scroll->signal_scroll_begin().connect(sigc::mem_fun(*this, &SpinButton::on_scroll_begin));
    m_scroll->signal_scroll().connect(      sigc::mem_fun(*this, &SpinButton::on_scroll      ), false);
    m_scroll->signal_scroll_end().connect(  sigc::mem_fun(*this, &SpinButton::on_scroll_end  ));
    m_scroll->set_flags(Gtk::EventControllerScroll::Flags::BOTH_AXES); // Mouse wheel is on y.
    add_controller(m_scroll);

    m_plus->hide();
    m_minus->hide();
    m_text->hide();

    m_value->set_label(m_text->get_text());

    //                  GTK4
    // -------------   SIGNALS   -------------

    // GTKMM4 missing signal_activate()!
    g_signal_connect(G_OBJECT(m_text->gobj()), "activate", G_CALLBACK(on_activate_c), this);
    // signal_activate().connect(sigc::mem_fun(*this, &SpinButton::on_activate));

    signal_changed().connect(sigc::mem_fun(*this, &SpinButton::on_changed));

    // Value (button)
    m_value->signal_clicked().connect(sigc::mem_fun(*this, &SpinButton::on_value_clicked));
                                              
#else
    // GTK3 NOT IMPLEMENTED
#endif

  }

#if GTK_CHECK_VERSION(4, 0, 0)

  // ---------------- CONTROLLERS -----------------

  // ------------------  MOTION  ------------------

  void SpinButton::on_motion_enter(double x, double y)
  {
    m_minus->show();
    m_plus->show();
  }

  void SpinButton::on_motion_leave()
  {
    m_minus->hide();
    m_plus->hide();

    if (m_text->is_visible()) {
      // We left spinbutton, save value and update.
      on_activate();
    }
  }

  // ---------------  MOTION VALUE  ---------------

  void SpinButton::on_motion_enter_value(double x, double y)
  {
    m_old_cursor = get_cursor();
    auto new_cursor = Gdk::Cursor::create("ew-resize");
    set_cursor(new_cursor);
  }

  void SpinButton::on_motion_leave_value()
  {
    set_cursor(m_old_cursor);
  }

  // ---------------   DRAG VALUE  ----------------

  void SpinButton::on_drag_begin_value(Gdk::EventSequence *sequence)
  {
    grab_focus();
    m_initial_value = get_value();
  }

  void SpinButton::on_drag_update_value(Gdk::EventSequence *sequence)
  {
    double dx = 0.0;
    double dy = 0.0;
    m_drag_value->get_offset(dx, dy);

    dx = std::round(dx/10.0);

    // If we don't move, then it probably was a button click.
    if (dx != 0.0) {
      double scale = 1.0;
      auto state = m_drag_value->get_current_event_state();
      if ((state & Gdk::ModifierType::CONTROL_MASK) == Gdk::ModifierType::CONTROL_MASK) {
        scale = 0.1;
      } else if ((state & Gdk::ModifierType::SHIFT_MASK) == Gdk::ModifierType::SHIFT_MASK) {
        scale = 10.0;
      }
      set_value (m_initial_value + scale * dx);

      m_dragged = true;
    }
  }

  void SpinButton::on_drag_end_value(Gdk::EventSequence *sequence)
  {
    // NOT USED (Button click handled by on_value_clicked().)
  }

  // ------------------  SCROLL  ------------------

  void SpinButton::on_scroll_begin()
  {
    // NOT USED
  }

  bool SpinButton::on_scroll(double dx, double dy)
  {
    double scale = 1.0;
    auto state = m_scroll->get_current_event_state();
    if ((state & Gdk::ModifierType::CONTROL_MASK) == Gdk::ModifierType::CONTROL_MASK) {
      scale = 0.1;
    } else if ((state & Gdk::ModifierType::SHIFT_MASK) == Gdk::ModifierType::SHIFT_MASK) {
      scale = 10.0;
    }
    get_adjustment()->set_value(get_adjustment()->get_value() + get_adjustment()->get_step_increment() * scale * dy);
    
    return true;
  }

  void SpinButton::on_scroll_end()
  {
    // NOT USED
  }

  //  -----------------  SIGNALS ------------------

  void SpinButton::on_changed()
  {
    m_value->set_label(m_text->get_text());
  }

  void SpinButton::on_activate()
  {
    m_value->set_label(m_text->get_text());

    m_text->hide();
    m_minus->show();
    m_value->show();
    m_plus->show();
    //grab_focus();
  }

  void SpinButton::on_value_clicked()
  {
    if (!m_dragged) {
      m_minus->hide();
      m_value->hide();
      m_plus->hide();
      m_text->show();
      m_text->select_region(0, m_text->get_text_length());
      m_text->grab_focus();
    }
    m_dragged = false;
    //  set_cursor(m_old_cursor);
  }

  // GTKMM4 bindings are missing signal_activate()!!
  void on_activate_c(GtkEntry* entry, gpointer user_data)
  {
    auto spinbutton = reinterpret_cast<Ink::SpinButton *>(user_data);
    spinbutton->on_activate();
  }

#endif // ========================================

} // Namespace

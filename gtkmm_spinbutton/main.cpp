
#include <iostream>
#include <gtkmm.h>

#include "spinbutton.h"
#include "spinbutton2.h"

class MyWindow : public Gtk::Window
{

public:
#if GTK_CHECK_VERSION(4, 0, 0)
  MyWindow() { //const std::shared_ptr<Gtk::Application>& app)
    set_title("Spinbutton Test 4");
#else
  MyWindow(const Glib::RefPtr<Gtk::Application>& app) {
    set_title("Spinbutton Test 3");
#endif    

    auto adjustment1 = Gtk::Adjustment::create(3.0, 0.0, 100.0, 1.0, 5.0, 0.0);
    auto adjustment2 = Gtk::Adjustment::create(0.0, 0.0, 100.0, 1.0, 5.0, 0.0);

    auto gtk_label  = Gtk::manage(new Gtk::Label("Gtk::SpinButton"));
    auto ink_label  = Gtk::manage(new Gtk::Label("Ink::SpinButton"));
    auto ink2_label = Gtk::manage(new Gtk::Label("Ink2::SpinButton"));

    // ---------- Gtk::SpinButton ----------
    m_gtk_spinbutton1 = Gtk::manage(new Gtk::SpinButton(adjustment1));
    m_gtk_spinbutton1->set_digits(1);

    m_gtk_spinbutton2 = Gtk::manage(new Gtk::SpinButton(adjustment2));
    m_gtk_spinbutton2->set_digits(2);
#if GTK_CHECK_VERSION(4, 0, 0)
    m_gtk_spinbutton2->set_orientation(Gtk::Orientation::VERTICAL);
#endif

    // ---------- Ink::SpinButton ----------
    m_ink_spinbutton1 = Gtk::manage(new Ink::SpinButton());
    m_ink_spinbutton1->set_adjustment(adjustment1);
    m_ink_spinbutton1->set_digits(1);

#if GTK_CHECK_VERSION(4, 0, 0)
    m_ink_spinbutton2 = Gtk::manage(new Ink::SpinButton(Gtk::Orientation::VERTICAL));
#else
    m_ink_spinbutton2 = Gtk::manage(new Ink::SpinButton());
#endif
    m_ink_spinbutton2->set_adjustment(adjustment2);
    m_ink_spinbutton2->set_digits(2);

    // ---------- Ink2::SpinButton ----------
    m_ink2_spinbutton1 = Gtk::manage(new Ink2::SpinButton());
    m_ink2_spinbutton1->set_adjustment(adjustment1);
    m_ink2_spinbutton1->set_digits(1);

#if GTK_CHECK_VERSION(4, 0, 0)
    m_ink2_spinbutton2 = Gtk::manage(new Ink2::SpinButton(Gtk::Orientation::VERTICAL));
#else
    m_ink2_spinbutton2 = Gtk::manage(new Ink2::SpinButton(Gtk::ORIENTATION_VERTICAL));
#endif
    m_ink2_spinbutton2->set_adjustment(adjustment2);
    m_ink2_spinbutton2->set_digits(2);


    // -------------- Layout ---------------
    m_grid = Gtk::manage(new Gtk::Grid);

    m_grid->attach(*gtk_label, 0, 0);
    m_grid->attach(*m_gtk_spinbutton1, 0, 1);
    m_grid->attach(*m_gtk_spinbutton2, 0, 2);

    m_grid->attach(*ink_label, 1, 0);
    m_grid->attach(*m_ink_spinbutton1, 1, 1);
    m_grid->attach(*m_ink_spinbutton2, 1, 2);

    m_grid->attach(*ink2_label, 2, 0);
    m_grid->attach(*m_ink2_spinbutton1, 2, 1);
    m_grid->attach(*m_ink2_spinbutton2, 2, 2);

#if GTK_CHECK_VERSION(4, 0, 0)
    set_child(*m_grid);
#else
    // A bit complicated in GTK3 to get everything shown correctly!
    add(*m_grid);
    m_grid->show();
    gtk_label->show();
    ink_label->show();
    ink2_label->show();
    m_gtk_spinbutton1->show_all();
    m_gtk_spinbutton2->show_all();
    m_ink_spinbutton1->show();
    m_ink_spinbutton2->show();
    m_ink2_spinbutton1->show();
    m_ink2_spinbutton2->show();
    show();
#endif
  }

private:
  Gtk::Grid*       m_grid;
  Gtk::SpinButton* m_gtk_spinbutton1;
  Gtk::SpinButton* m_gtk_spinbutton2;
  Ink::SpinButton* m_ink_spinbutton1;
  Ink::SpinButton* m_ink_spinbutton2;
  Ink2::SpinButton* m_ink2_spinbutton1;
  Ink2::SpinButton* m_ink2_spinbutton2;
};

int main(int argc, char *argv[])
{

#if GTK_CHECK_VERSION(4, 0, 0)
  auto application = Gtk::Application::create("org.tavmjong.spinbutton4");
  return application->make_window_and_run<MyWindow>(argc, argv);
#else
  auto application = Gtk::Application::create(argc, argv, "org.tavmjong.spinbutton3");
  MyWindow window(application);
  return application->run(window);
#endif
  
}

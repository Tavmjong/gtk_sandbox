#ifndef MY_APPLICATION_H
#define MY_APPLICATION_H

/*
 * The main application.
 *
 * Copyright (C) 2020 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include <gtkmm.h>

class MyApplication : public Gtk::Application
{
 protected:
  MyApplication();

 public:
  static Glib::RefPtr<MyApplication> create();

 protected:
  void on_startup() override;
  void on_activate() override;

};

#endif // MY_APPLICATION_H

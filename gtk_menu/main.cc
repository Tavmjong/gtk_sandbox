
#include <iostream>
#include <ostream>
#include <string>
#include <unordered_map>

#include <giomm.h>
#include <glibmm.h>
#include <gtkmm.h>

void my_set_visible(Gtk::Widget* widget) {
  static int count = 0;
  ++count;
  for (int i = 0; i < count; i++) { std::cout << "  "; }
  std::cout << widget->get_name() << std::endl;

  if (auto image = dynamic_cast<Gtk::Image*>(widget)) {
    std::cout << "   set visible: " << image->get_icon_name() << std::endl;
    image->set_visible(true);
  }
  for (auto child = widget->get_first_child(); child != nullptr; child = child->get_next_sibling()) {
    my_set_visible(child);
  }

  --count;
}

class MyMenuItem : public Gtk::Button {
public:
  MyMenuItem(Glib::ustring const &action,
             Glib::ustring const &label_string,
             Glib::ustring const &shortcut_string,
             Glib::ustring const &image_string = {})
  {
    std::cout << "MyMenuItem: " << action
              << "  label_string: " << label_string
              << "  shortcut_string: " << shortcut_string
              << "  image_string: " << image_string
              << std::endl;
    set_name("MyMenuItem");
    add_css_class("my-menu-item");
    set_action_name(action);

    auto image = Gtk::make_managed<Gtk::Image>();
    if (!image_string.empty()) {
      image->set_from_icon_name(image_string);
    }

    auto label = Gtk::make_managed<Gtk::Label>(label_string);
    auto separator = Gtk::make_managed<Gtk::Separator>();
    separator->set_hexpand(true);
    separator->set_opacity(0);
    auto shortcut = Gtk::make_managed<Gtk::Label>(shortcut_string);
    shortcut->add_css_class("my-menu-item-shortcut");

    auto box = Gtk::make_managed<Gtk::Box>();
    box->append(*image);
    box->append(*label);
    box->append(*separator);
    box->append(*shortcut);
    set_child(*box);

    signal_clicked().connect(sigc::bind(sigc::mem_fun(*this, &MyMenuItem::on_clicked), this));
  }

  void on_clicked(Gtk::Widget *widget) {
    auto parent = widget->get_parent();
    if (parent) {
      if (auto popovermenu = dynamic_cast<Gtk::PopoverMenu *>(parent)) {
	popovermenu->popdown();
	return;
      }
      on_clicked(parent);
    }
  }

};

// Functions for action testing.
void test1() { std::cout << "test1" << std::endl; }
void test2() { std::cout << "test2" << std::endl; }
void test3() { std::cout << "test3" << std::endl; }
void radio(int i) { std::cout << "radio: " << i << std::endl; }
void toggle() { std::cout << "toggled" << std::endl; }

class Window: public Gtk::ApplicationWindow
{

public:
  Window()
  {
    set_title("Window");
    set_show_menubar(true);

    add_action( "test1", sigc::ptr_fun(test1));
    add_action( "test2", sigc::ptr_fun(test2));
    add_action( "test3", sigc::ptr_fun(test3));
    add_action_radio_integer("radio", sigc::ptr_fun(radio), 1);
    add_action_bool("toggle", sigc::ptr_fun(toggle), true);

    liststore = Gio::ListStore<Gtk::Shortcut>::create();
    liststore->append(Gtk::Shortcut::create(Gtk::KeyvalTrigger::create(GDK_KEY_g, Gdk::ModifierType::CONTROL_MASK), Gtk::NamedAction::create("win.test1")));
    liststore->append(Gtk::Shortcut::create(Gtk::KeyvalTrigger::create(GDK_KEY_h, Gdk::ModifierType::CONTROL_MASK), Gtk::NamedAction::create("win.test2")));
    liststore->append(Gtk::Shortcut::create(Gtk::KeyvalTrigger::create(GDK_KEY_i, {}                             ), Gtk::NamedAction::create("win.test3")));
    liststore->append(Gtk::Shortcut::create(Gtk::KeyvalTrigger::create(GDK_KEY_x, {}                             ), Gtk::NamedAction::create("win.radio::0")));
    liststore->append(Gtk::Shortcut::create(Gtk::KeyvalTrigger::create(GDK_KEY_y, {}                             ), Gtk::NamedAction::create("win.radio(1)")));
    liststore->append(Gtk::Shortcut::create(Gtk::KeyvalTrigger::create(GDK_KEY_z, Gdk::ModifierType::CONTROL_MASK), Gtk::NamedAction::create("win.radio('2')")));
    liststore->append(Gtk::Shortcut::create(Gtk::KeyvalTrigger::create(GDK_KEY_w, {}                             ), Gtk::NamedAction::create("win.toggle")));
    std::cout << "NamedAction: " << Gtk::NamedAction::create("win.radio(0)")->get_action_name() << std::endl;

    auto shortcut_controller1 = Gtk::ShortcutController::create();
    auto shortcut_controller2 = Gtk::ShortcutController::create();
    auto shortcut_controller3 = Gtk::ShortcutController::create(liststore);
    auto shortcut_controller4 = Gtk::ShortcutController::create(liststore);
    shortcut_controller1->set_name("MyShortcutController1");
    shortcut_controller2->set_name("MyShortcutController2");
    shortcut_controller3->set_name("MyShortcutController3");
    shortcut_controller4->set_name("MyShortcutController4");
    // add_controller(shortcut_controller1);
    add_controller(shortcut_controller2);
    add_controller(shortcut_controller3);
    add_controller(shortcut_controller4);

    // shortcut_controller1->add_shortcut(Gtk::Shortcut::create(Gtk::KeyvalTrigger::create(GDK_KEY_a, Gdk::ModifierType::CONTROL_MASK),
    //                                                          Gtk::NamedAction::create("win.test1")));
    // shortcut_controller1->add_shortcut(Gtk::Shortcut::create(Gtk::KeyvalTrigger::create(GDK_KEY_b, Gdk::ModifierType::CONTROL_MASK),
    //                                                          Gtk::NamedAction::create("win.test2")));
    // shortcut_controller1->add_shortcut(Gtk::Shortcut::create(Gtk::KeyvalTrigger::create(GDK_KEY_c, Gdk::ModifierType(0)),
    //                                                          Gtk::NamedAction::create("win.test3")));
    // shortcut_controller2->add_shortcut(Gtk::Shortcut::create(Gtk::KeyvalTrigger::create(GDK_KEY_d, Gdk::ModifierType::CONTROL_MASK),
    //                                                          Gtk::NamedAction::create("win.test1")));
    // shortcut_controller2->add_shortcut(Gtk::Shortcut::create(Gtk::KeyvalTrigger::create(GDK_KEY_e, Gdk::ModifierType::CONTROL_MASK),
    //                                                          Gtk::NamedAction::create("win.test2")));
    // shortcut_controller2->add_shortcut(Gtk::Shortcut::create(Gtk::KeyvalTrigger::create(GDK_KEY_f, Gdk::ModifierType(0)),
    //                                                          Gtk::NamedAction::create("win.test3")));
    shortcut_controller2->add_shortcut(Gtk::Shortcut::create(Gtk::KeyvalTrigger::create(GDK_KEY_f, {}),
                                                             Gtk::NamedAction::create("win.radio(0)")));

    shortcut_controller1->set_propagation_phase(Gtk::PropagationPhase::BUBBLE);
    shortcut_controller2->set_propagation_phase(Gtk::PropagationPhase::CAPTURE);
    shortcut_controller3->set_propagation_phase(Gtk::PropagationPhase::BUBBLE);
    shortcut_controller4->set_propagation_phase(Gtk::PropagationPhase::CAPTURE);

    auto builder = Gtk::Builder::create_from_file("menu.ui");
    auto box = builder->get_widget<Gtk::Box>("MyBox");
    set_child(*box);

    // ========  MENU BUTTON ======== //

    auto menu = Gio::Menu::create();
    auto menuitem1 = Gio::MenuItem::create("Test1", "win.test1");
    auto menuitem2 = Gio::MenuItem::create("Test2", "win.test2");
    auto menuitem3 = Gio::MenuItem::create("Test3", "win.test3");
    auto icon1 = Gio::ThemedIcon::create("document-new", true);
    auto icon2 = Gio::ThemedIcon::create("document-open", true);
    auto icon3 = Gio::ThemedIcon::create("document-save", true);
    menuitem1->set_icon(icon1);
    menuitem2->set_icon(icon2);
    menuitem3->set_icon(icon3);
    menu->append_item(menuitem1);
    menu->append_item(menuitem2);
    menu->append_item(menuitem3);

    auto menubutton = builder->get_widget<Gtk::MenuButton>("MyMenuButton");
    menubutton->set_menu_model(menu);
    menubutton->set_label("File");

    auto popover = menubutton->get_popover();
    if (popover) {
      popover->signal_show().connect(sigc::bind(sigc::ptr_fun(my_set_visible), popover));
    } else {
      std::cout << "NO MENUBUTTON POPOVER!" << std::endl;
    }

    // The menu model from builder.
    auto menu_model = builder->get_object<Gio::MenuModel>("MyMenu");

    auto menubutton2 = builder->get_widget<Gtk::MenuButton>("MyMenuButton2");
    menubutton2->set_menu_model(menu_model);
    menubutton2->set_label("File");

    auto popover2 = menubutton2->get_popover();
    if (popover2) {
      popover2->signal_show().connect(sigc::bind(sigc::ptr_fun(my_set_visible), popover2));
    } else {
      std::cout << "NO MENUBUTTON POPOVER!" << std::endl;
    }

    // ========= MENU BAR AS FOUND IN BUILDER FILE ========= //

    auto menubar = builder->get_widget<Gtk::PopoverMenuBar>("MyMenuBar");

    // Add custom widget.
    auto menuitem = Gtk::make_managed<MyMenuItem>("win.test3", "Test3", "I", "document-open");
    menubar->add_child(*menuitem, "my-id");

    // ========= "MENU ITEM" BY ITSELF ========= //
    auto menuitemA = Gtk::make_managed<MyMenuItem>("win.test1", "Test3", "I", "document-open");
    box->append(*menuitemA);

    // ========= "MENU BAR AFTER PROCESSING ========= //
    auto menubar_new = Gtk::make_managed<Gtk::PopoverMenuBar>();
    auto menu_copy = Gio::Menu::create();
    std::vector<std::pair<MyMenuItem*, Glib::ustring>> widgets;
    rebuild_menubar(menubar_new, menu_model, menu_copy, widgets);

    menubar_new->set_menu_model(menu_copy);

    // Add replacement widgets.
    for (auto widget : widgets) {
      menubar_new->add_child(*widget.first, widget.second);
    }
    box->append(*menubar_new);

    // Set all images (icons) visible, doesn't work.
    // my_set_visible(this);
  }

  // Note: a Gio::Menu is a simple implementation of a Gio::MenuModel.
  void rebuild_menubar(Gtk::PopoverMenuBar *menubar,
                       Glib::RefPtr<Gio::MenuModel> const &menu,
                       Glib::RefPtr<Gio::Menu> const &menu_copy,
                       std::vector<std::pair<MyMenuItem*, Glib::ustring>> &widgets) {

    auto const display = Gdk::Display::get_default();

    std::cout << "rebuild_menubar: number of items: " << menu->get_n_items() << std::endl;
    for (int i = 0; i < menu->get_n_items(); ++i) {
      Glib::ustring label;
      Glib::ustring action;
      Glib::ustring target;
      Glib::VariantBase icon;
      Glib::ustring icon_string;
      Glib::ustring use_icon;

      // std::string had Glib::ustring missing hash!
      std::unordered_map<std::string, Glib::VariantBase> attributes;

      auto attribute_iter = menu->iterate_item_attributes(i);
      while (attribute_iter->next()) {
        // Attributes we need to create MenuItem or set icon.
        if          (attribute_iter->get_name() == "label") {
          // Convert label while preserving unicode translations
          label = Glib::VariantBase::cast_dynamic<Glib::Variant<std::string> >(attribute_iter->get_value()).get();
        } else if   (attribute_iter->get_name() == "action") {
          action  = attribute_iter->get_value().print();
          action.erase(0, 1);
          action.erase(action.size()-1, 1);
        } else if   (attribute_iter->get_name() == "target") {
          target  = attribute_iter->get_value().print();
        } else if   (attribute_iter->get_name() == "icon") {
          icon = attribute_iter->get_value();
          icon_string     = attribute_iter->get_value().print();
          icon_string.erase(0 ,1);
          icon_string.erase(icon_string.size()-1, 1);
        } else if (attribute_iter->get_name() == "use-icon") {
          use_icon =  attribute_iter->get_value().print();
        } else {
          // All the remaining attributes.
          attributes[attribute_iter->get_name()] = attribute_iter->get_value();
        }
      }
      Glib::ustring detailed_action = action;
      if (target.size() > 0) {
        detailed_action += "(" + target + ")";
      }

      // Create new menu item.
      auto menu_item = Gio::MenuItem::create(label, detailed_action);
      if (!detailed_action.empty() && !icon_string.empty()) {
#if 1
        menu_item->set_attribute_value("custom", Glib::Variant<Glib::ustring>::create(label));
#else
	std::cout << "Setting icon! " << icon_string << std::endl;
	auto gio_icon = Gio::ThemedIcon::create(icon_string, true);
	menu_item->set_icon(gio_icon);
	// menu_item->set_attribute_value("icon", icon);
#endif
      }

      // Add remaining attributes
      for (auto const& [key, value] : attributes) {
        menu_item->set_attribute_value(key, value);
      }

      // Add submenus
      auto link_iter = menu->iterate_item_links(i);
      while (link_iter->next()) {
        auto submenu = Gio::Menu::create();
        if (link_iter->get_name() == "submenu") {
          menu_item->set_submenu(submenu);
        } else if (link_iter->get_name() == "section") {
          menu_item->set_section(submenu);
        } else {
          std::cerr << "rebuild_menu: Unknown link type: " << link_iter->get_name().raw() << std::endl;
        }
        rebuild_menubar (menubar, link_iter->get_value(), submenu, widgets);
      }

      menu_copy->append_item(menu_item);

      // Create new set of widgets.
      if (!detailed_action.empty()) {
        // Find shortcut
        Glib::ustring shortcut_string;
        auto count = liststore->get_n_items();
        for (int i = 0; i < count; ++i) {
          auto shortcut = liststore->get_item(i);
          auto trigger = shortcut->get_trigger();
          auto action = shortcut->get_action();
          auto named_action = std::dynamic_pointer_cast<Gtk::NamedAction>(action);
          if (named_action) {
            if (detailed_action == named_action->get_action_name()) {
              shortcut_string = trigger->to_label(display);
            }
          }
        }
        std::cout << "adding MyMenuItem: " << detailed_action
                  << "  label: " << label
                  << "  icon: " << icon_string << std::endl;
        auto menu_item2 = Gtk::make_managed<MyMenuItem>(detailed_action, label, shortcut_string, icon_string);

        // menubar->add_child(*menu_item2, label);
        widgets.push_back(std::pair(menu_item2, label));
      }
    }

    auto provider = Gtk::CssProvider::create();
    Gtk::StyleContext::add_provider_for_display(display, provider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    Glib::ustring css_str = R"a(
       .my-menu-item { border: none; }
       .my-menu-item separator { margin-left: 10px; }
       .my-menu-item image { margin-left: 2px; margin-right: 5px; }
       .my-menu-item-shortcut { opacity: 0.55; })a";
    provider->load_from_data(css_str);
  }

  Glib::RefPtr<Gio::ListStore<Gtk::Shortcut>> liststore;
};


int main(int argc, char *argv[]) {

  auto application = Gtk::Application::create("org.tavmjong.spinbutton_mm4");
  return application->make_window_and_run<Window>(argc, argv);
}



#include <iostream>
#include <iomanip>
#include <gtkmm.h>

class Window: public Gtk::Window
{
public:
#if GTK_CHECK_VERSION(4, 0, 0)
  Window()
#else
  Window(const Glib::RefPtr<Gtk::Application>& app)
#endif
  {
    set_title("Device Test");
    set_default_size(200, 200);

    signal_realize().connect(sigc::mem_fun(*this, &Window::on_my_realize));
    signal_map().connect(    sigc::mem_fun(*this, &Window::on_my_map    ));

    auto display = get_display(); // gdk display
    display->beep();

    // Display name will be of the form :0 for X11 and wayland-0 for Wayland.
    std::cout << "Display name: " << display->get_name() << std::endl;

    // A button that does nothing... could refresh output.
    auto button = Gtk::manage(new Gtk::Button("Update"));
    button->signal_clicked().connect(sigc::mem_fun(*this, &Window::on_my_clicked));

#if GTK_CHECK_VERSION(4, 0, 0)
    set_child(*button);
    show();
#else
    add(*button);
    show_all();
#endif

    std::cout << std::left << std::boolalpha; // Global 

    auto seats = display->list_seats();
    std::cout << "\nNumber of seats: " << seats.size() << std::endl;
    for (auto seat : seats) {

#if GTK_CHECK_VERSION(4, 0, 0)

      std::cout << "  Capabilities: ";
      auto capabilities = seat->get_capabilities();
      if (bool(capabilities &  Gdk::Seat::Capabilities::NONE))          std::cout << " None";
      if (bool(capabilities &  Gdk::Seat::Capabilities::POINTER))       std::cout << " Pointer";
      if (bool(capabilities &  Gdk::Seat::Capabilities::TOUCH))         std::cout << " Touch";
      if (bool(capabilities &  Gdk::Seat::Capabilities::TABLET_STYLUS)) std::cout << " Tablet_Stylus";
      if (bool(capabilities &  Gdk::Seat::Capabilities::KEYBOARD))      std::cout << " Keyboard";
      if (bool(capabilities &  Gdk::Seat::Capabilities::TABLET_PAD))    std::cout << " Tablet_Pad";
      if (bool(capabilities == Gdk::Seat::Capabilities::ALL_POINTING))  std::cout << " All_Pointing";
      if (bool(capabilities == Gdk::Seat::Capabilities::ALL))           std::cout << " All";
      std::cout << std::endl;

      auto pointer = seat->get_pointer();
      if (pointer) {
        std::cout << "  Pointer: " << std::endl;
        dump_device(pointer);
      } else {
        std::cout << "  No pointer device" << std::endl;
      }
      
      auto keyboard = seat->get_keyboard();
      if (keyboard) {
        std::cout << "  Keyboard: " << std::endl;
        dump_device(keyboard);
      } else {
        std::cout << "  No keyboard device" << std::endl;
      }
      
      std::cout << "  Seat devices: (all)" << std::endl;
      auto devices = seat->get_devices(Gdk::Seat::Capabilities::ALL);
      for (auto device : devices) {
        dump_device(device);
      }

      std::cout << "  Seat devices (pointers):" << std::endl;
      auto pointer_devices = seat->get_devices(Gdk::Seat::Capabilities::POINTER);
      for (auto device : pointer_devices) {
        dump_device(device);
      }

      std::cout << "  Seat devices (keyboard):" << std::endl;
      auto keyboard_devices = seat->get_devices(Gdk::Seat::Capabilities::KEYBOARD);
      for (auto device : keyboard_devices) {
        dump_device(device);
      }

      auto tools = seat->get_tools();
      std::cout << "  Seat tools: " << tools.size() << std::endl;
      for (auto tool : tools) {
        dump_device_tool(tool);
      }

#else

      // No TABLET_PAD
      std::cout << "  Capabilities: ";
      auto capabilities = seat->get_capabilities();
      if (bool(capabilities &  Gdk::SEAT_CAPABILITY_NONE))          std::cout << " None";
      if (bool(capabilities &  Gdk::SEAT_CAPABILITY_POINTER))       std::cout << " Pointer";
      if (bool(capabilities &  Gdk::SEAT_CAPABILITY_TOUCH))         std::cout << " Touch";
      if (bool(capabilities &  Gdk::SEAT_CAPABILITY_TABLET_STYLUS)) std::cout << " Tablet_Stylus";
      if (bool(capabilities &  Gdk::SEAT_CAPABILITY_KEYBOARD))      std::cout << " Keyboard";
      if (bool(capabilities == Gdk::SEAT_CAPABILITY_ALL_POINTING))  std::cout << " All_Pointing";
      if (bool(capabilities == Gdk::SEAT_CAPABILITY_ALL))           std::cout << " All";
      std::cout << std::endl;

      auto pointer = seat->get_pointer();
      if (pointer) {
        std::cout << "  Pointer: " << std::endl;
        dump_device(pointer);
      } else {
        std::cout << "  No pointer device" << std::endl;
      }

      auto keyboard = seat->get_keyboard();
      if (keyboard) {
        std::cout << "  Keyboard: " << std::endl;
        dump_device(keyboard);
      } else {
        std::cout << "  No keyboard device" << std::endl;
      }
      
#endif
    }

  }

  void on_my_realize() {
    std::cout << "on_my_realize()" << std::endl;
  }

  void on_my_map() {
    std::cout << "on_my_map()" << std::endl;
  }

  void on_my_clicked() {
    std::cout << "on_my_clicked: Clicked" << std::endl;
  }

  void dump_device(Glib::RefPtr<Gdk::Device> device) {
    std::cout << "    Device: "    << std::setw(50) << device->get_name()
#if GTK_CHECK_VERSION(4, 0, 0)
              << "    Vendor: "    << std::setw( 5) << device->get_vendor_id()   // Must be master to work in Gtk3
              << "    Product: "   << std::setw( 5) << device->get_product_id()  // Must be master to work in Gtk3
              << "  No. Touches: " << device->get_num_touches()
#else
              << "  No. Keys: "    << std::setw( 5) << device->get_n_keys()
#endif
              << "  Has cursor: "  << std::setw( 7) << device->get_has_cursor();
    if (device->get_source() != Gdk::SOURCE_KEYBOARD) {
      std::cout << "  No. Axis: "    << device->get_n_axes();
      auto axis_labels = device->list_axes();
      for (auto axis_label : axis_labels) {
        std::cout << ",  " << axis_label;
      }
    }
    std::cout << std::endl;
#if GTK_CHECK_VERSION(4, 0, 0)
    auto device_tool = device->get_device_tool();
    if (device_tool) {
      dump_device_tool(device_tool);
    } else {
      std::cout << "      No device tool!" << std::endl;
    }
#else
    if (device->get_device_type() == Gdk::DEVICE_TYPE_MASTER) {
      auto slaves = device->list_slave_devices();
      std::cout << "   Slaves: " << std::endl;
      for (auto slave : slaves) {
        dump_device(slave);
      }
      std::cout << std::endl;
    }
#endif
  }

#if GTK_CHECK_VERSION(4, 0, 0)
  void dump_device_tool(Glib::RefPtr<Gdk::DeviceTool> device_tool) {
    auto tool_type = device_tool->get_tool_type();
    switch (tool_type) {
    case Gdk::DeviceTool::Type::UNKNOWN:  std::cout << "      Unknown"  << std::endl; break;
    case Gdk::DeviceTool::Type::PEN:      std::cout << "      Pen"      << std::endl; break;
    case Gdk::DeviceTool::Type::ERASER:   std::cout << "      Eraser"   << std::endl; break;
    case Gdk::DeviceTool::Type::BRUSH:    std::cout << "      Brush"    << std::endl; break;
    case Gdk::DeviceTool::Type::PENCIL:   std::cout << "      Pencil"   << std::endl; break;
    case Gdk::DeviceTool::Type::AIRBRUSH: std::cout << "      Airbrush" << std::endl; break;
    case Gdk::DeviceTool::Type::MOUSE:    std::cout << "      Mouse"    << std::endl; break;
    case Gdk::DeviceTool::Type::LENS:     std::cout << "      Lens"     << std::endl; break;
    default: std::cout << "Unknown" << std::endl;
    }
  }
#endif

};


int main(int argc, char **argv)
{

#if GTK_CHECK_VERSION(4, 0, 0)
  auto application = Gtk::Application::create("org.tavmjong.monitors_mm");
  return application->make_window_and_run<Window>(argc, argv);
#else
  auto application = Gtk::Application::create(argc, argv, "org.tavmjong.montiors_mm");
  Window window(application);
  return application->run(window);
#endif

}




#include <iostream>
#include <vector>

#include <gtkmm.h>

namespace {
  class MyColor : public Glib::Object  {

  public:
    static Glib::RefPtr<MyColor> create(const Glib::ustring& color)
    {
      return Glib::make_refptr_for_instance<MyColor>(new MyColor(color));
    }

    Glib::ustring get_color() const { return m_color; }

  protected:
    MyColor(const Glib::ustring& color)
      : Glib::ObjectBase(typeid(MyColor))
      , m_color(color)
    {
    }

  private:
    Glib::ustring m_color;
  };
} // anonymouse namespace

// Recursively hunt for a widget with name 'name'.
Gtk::Widget* find_widget(Gtk::Widget* widget, Glib::ustring name) {
  if (widget) {
    auto child = widget->get_first_child();
    if (child) {
      std::cout << "find_widget: " << widget->get_name() << std::endl;
      if (child->get_name() == name) {
        // We found it!
        return child;
      } else {
        auto child2 = find_widget(child, name);
        if (child2) {
          // Pass it back up the tree!
          return child2;
        } else {
          return find_widget(widget->get_next_sibling(), name);
        }
      }
    } else {
      return nullptr;
    }
  }
  return nullptr;
}

class Window: public Gtk::ApplicationWindow
{
public:
  Window()  {
    set_title("DrowDown Test");
    set_default_size(200, 200);

    // Use Gio::ListStore
    auto list_store = Gio::ListStore<MyColor>::create();
    list_store->append(MyColor::create("Red"));
    list_store->append(MyColor::create("Blue"));
    list_store->append(MyColor::create("Green"));
    list_store->append(MyColor::create("Purple"));
    list_store->append(MyColor::create("Orange"));
    list_store->append(MyColor::create("Aqua"));
    list_store->append(MyColor::create("Violet"));

    std::cout << "Before creating Gio::ListStore" << std::endl;
    const int STEP = 4; // Controls size of ListStore arrays, larger step -> smaller array.

    static int count = 0;
    for (int i = 0; i < 0xFF; i+=STEP) {
      for (int j = 0; j < 0xFF; j+=STEP) {
        for (int k = 0; k < 0xFF; k+=STEP) {
          char hex[8];
          int color = (i << 16) + (j << 8) + k;
          snprintf(hex, sizeof hex, "#%06x", color);
          list_store->append(MyColor::create(hex));
          count++;
        }
      }
    }
    std::cout << "After creating Gio::ListStorelist: number of items: " << count << "." << std::endl;

    auto factory = Gtk::SignalListItemFactory::create();
    factory->signal_setup().connect(sigc::mem_fun(*this, &Window::setup_listitem_cb));
    factory->signal_bind().connect(sigc::mem_fun(*this, &Window::bind_listitem_cb));

    auto list = Gtk::make_managed<Gtk::ListView>(Gtk::SingleSelection::create(list_store), factory);
    list->set_single_click_activate(true);
    list->signal_activate().connect(sigc::bind<0>(sigc::mem_fun(*this, &Window::activate_list_view_cb), list));

    auto dropdown1 = Gtk::make_managed<Gtk::DropDown>(Gtk::SingleSelection::create(list_store));
    auto dropdown2 = Gtk::make_managed<Gtk::DropDown>(Gtk::SingleSelection::create(list_store));
    dropdown1->set_factory(factory);
    dropdown2->set_factory(factory);
    //dropdown1->set_show_arrow(true);
    //dropdown2->set_show_arrow(true);

    // Replace Gtk::ListView by Gtk::GridView in Gtk::DrowDown
    auto list_view = dynamic_cast<Gtk::ListView*>(find_widget(dropdown2, "GtkListView"));
    if (list_view) {
      std::cout << "  Found ListView!!" << std::endl;
      auto model = list_view->get_model();
      auto factory = list_view->get_factory();
      auto grid_view = Gtk::make_managed<Gtk::GridView>(model, factory);
      grid_view->set_single_click_activate();
      grid_view->set_min_columns(2);
      grid_view->set_max_columns(2);
      grid_view->signal_activate().connect(sigc::bind<0>(sigc::mem_fun(*this, &Window::activate_grid_view_cb), grid_view));
      auto scrolled_window = dynamic_cast<Gtk::ScrolledWindow*>(list_view->get_parent());
      if (scrolled_window) {
        scrolled_window->set_child(*grid_view);
      } else {
        std::cerr << "Did not find drop down scrolled window!" << std::endl;
      }
    }

    // A popover with a Gtk::GridView as a replacement for a Gtk::DropDown
    auto grid_view2 = Gtk::make_managed<Gtk::GridView>(Gtk::SingleSelection::create(list_store), factory);
    grid_view2->set_single_click_activate();
    grid_view2->set_min_columns(2);
    grid_view2->set_max_columns(2);

    auto scrolled_window2 = Gtk::make_managed<Gtk::ScrolledWindow>();
    scrolled_window2->set_propagate_natural_height(true);
    scrolled_window2->set_max_content_height(400);
    scrolled_window2->set_policy(Gtk::PolicyType::NEVER, Gtk::PolicyType::AUTOMATIC);
    scrolled_window2->set_child(*grid_view2);

    auto popover = Gtk::make_managed<Gtk::Popover>();
    popover->set_child(*scrolled_window2);

    auto menu_button = Gtk::make_managed<Gtk::MenuButton>();
    menu_button->set_popover(*popover);
    menu_button->set_always_show_arrow(true);

    Glib::ustring initial_color = list_store->get_item(0)->get_color();
    auto item_box = compose_item_box(initial_color);
    menu_button->set_child(*item_box);

    grid_view2->signal_activate().connect(sigc::bind(sigc::mem_fun(*this, &Window::activate_grid_view_cb2), grid_view2, menu_button));

    // Add everything to a box for display!
    // auto box = Gtk::make_managed<Gtk::Box>();
    // box->set_homogeneous();
    // box->append(*dropdown1);
    // box->append(*dropdown2);
    // box->append(*menu_button);

    auto grid = Gtk::make_managed<Gtk::Grid>();
    grid->set_column_homogeneous(true);

    grid->attach(*(Gtk::make_managed<Gtk::Label>("ListView")),   0, 0, 1, 1);
    grid->attach(*(Gtk::make_managed<Gtk::Label>("GridView")),   1, 0, 1, 1);
    grid->attach(*(Gtk::make_managed<Gtk::Label>("MenuButton")), 2, 0, 1, 1);
    grid->attach(*dropdown1,   0, 1, 1, 1);
    grid->attach(*dropdown2,   1, 1, 1, 1);
    grid->attach(*menu_button, 2, 1, 1, 1);
    set_child(*grid);
  }

  void activate_list_view_cb(Gtk::ListView* list, guint position) {
    if (auto model = std::dynamic_pointer_cast<Gio::ListModel>(list->get_model())) {
      auto item = model->get_object(position);
      if (auto column = std::dynamic_pointer_cast<MyColor>(item)) {
        std::cout << "activate_list_view_cb: " << column->get_color() << std::endl;
      } else {
        std::cout << "actibate_list_view_:cb: wrong type!" << std::endl;
      }
    }
  }

  void activate_grid_view_cb(Gtk::GridView* grid_view, guint position) {
    Glib::ustring color;
    if (auto model = std::dynamic_pointer_cast<Gio::ListModel>(grid_view->get_model())) {
      auto item = model->get_object(position);
      if (auto column = std::dynamic_pointer_cast<MyColor>(item)) {
        std::cout << "activate_grid_view_cb: " << column->get_color() << std::endl;
        color = column->get_color();
      } else {
        std::cout << "activate_grid_view_cb: wrong type!" << std::endl;
      }
    }

    // Need to recreate DropDown closed state content and manually close popover.
    auto popover = dynamic_cast<Gtk::Popover*>(grid_view->get_parent()->get_parent()->get_parent()->get_parent());
    if (popover) {
      auto drop_down = dynamic_cast<Gtk::DropDown*>(popover->get_parent());
      if (drop_down) {
        auto toggle_button = dynamic_cast<Gtk::ToggleButton*>(drop_down->get_first_child());
        if (toggle_button) {

          auto label = Gtk::make_managed<Gtk::Label>(color);
          label->set_halign(Gtk::Align::START);
          label->set_hexpand(true);

          auto drawing_area = Gtk::make_managed<Gtk::DrawingArea>();
          drawing_area->set_content_width(10);
          drawing_area->set_content_height(10);
          drawing_area->set_draw_func(sigc::bind(sigc::ptr_fun(on_drawing_area_draw), color));

          auto icon = Gtk::make_managed<Gtk::Image>();
          icon->set_from_icon_name("pan-down");

          auto box = Gtk::make_managed<Gtk::Box>();
          box->append(*label);
          box->append(*drawing_area);
          box->append(*icon);

          toggle_button->set_child(*box);
        } else {
          std::cerr << "Failed to get toggle button!" << std::endl;
        }
      } else {
        std::cerr << "Failed to get drop down!" << std::endl;
      }
      popover->popdown();
    } else {
      std::cerr << "acitvate_grid_view_cb: didn't find popover!" << std::endl;
    }
  }

  void activate_grid_view_cb2(guint position, Gtk::GridView* grid_view, Gtk::MenuButton* menu_button) {
    Glib::ustring color;
    if (auto model = std::dynamic_pointer_cast<Gio::ListModel>(grid_view->get_model())) {
      auto item = model->get_object(position);
      if (auto column = std::dynamic_pointer_cast<MyColor>(item)) {
        std::cout << "activate_grid_view_cb2: " << column->get_color() << std::endl;
        color = column->get_color();
      } else {
        std::cout << "activate_grid_view_cb2: wrong type!" << std::endl;
      }
    }

    if (menu_button) {
      auto box = compose_item_box(color);
      menu_button->set_child(*box);
    } else {
      std::cerr << "Failed to get toggle button!" << std::endl;
    }

    auto popover = menu_button->get_popover();
    if (popover) {
      popover->popdown();
    } else {
      std::cerr << "activate_grid_view_cb2: missing popover!" << std::endl;
    }
  }

  void setup_listitem_cb(const Glib::RefPtr<Gtk::ListItem>& list_item) {
    // std::cout << "setup_listitem_cb" << std::endl;
    auto box   = Gtk::make_managed<Gtk::Box>(Gtk::Orientation::HORIZONTAL, 12);

    auto label = Gtk::make_managed<Gtk::Label>();
    label->set_halign(Gtk::Align::START);
    label->set_hexpand(true);
    box->append(*label);

    auto drawing_area = Gtk::make_managed<Gtk::DrawingArea>();
    drawing_area->set_content_width(10);
    drawing_area->set_content_height(10);
    box->append(*drawing_area);

    list_item->set_child(*box);
  }

  static  void on_drawing_area_draw(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height, Glib::ustring const &color) {
    std::cout << "on_drawing_area_draw: " << color << std::endl;
    Gdk::Cairo::set_source_rgba(cr, Gdk::RGBA(color));
    cr->paint();
  }

  void bind_listitem_cb(const Glib::RefPtr<Gtk::ListItem>& list_item) {
    auto item = list_item->get_item();
    if (auto column = std::dynamic_pointer_cast<MyColor>(item)) {

      if (auto label = dynamic_cast<Gtk::Label*>(list_item->get_child()->get_first_child())) {
        label->set_text(column->get_color());

        if (auto drawing_area = dynamic_cast<Gtk::DrawingArea*>(label->get_next_sibling())) {
          drawing_area->set_draw_func(sigc::bind(sigc::ptr_fun(on_drawing_area_draw), column->get_color()));
          // std::cout << "bind_listitem_cb: " << column->get_color() << std::endl;
        } else {
          std::cerr << "bind_listitem_cb: second child not DrawingArea!" << std::endl;
        }
      } else {
        std::cerr << "bind_listitem_cb: first child not Label!" << std::endl;
      }
    } else {
      std::cerr << "bind_listitem_cb: wrong type!" << std::endl;
    }
  }


  // Convenience function to fill Gtk::MenuButton.
  Gtk::Box* compose_item_box(const Glib::ustring& color) {
    auto label = Gtk::make_managed<Gtk::Label>(color);
    label->set_halign(Gtk::Align::START);
    label->set_hexpand(true);

    auto drawing_area = Gtk::make_managed<Gtk::DrawingArea>();
    drawing_area->set_content_width(10);
    drawing_area->set_content_height(10);
    drawing_area->set_draw_func(sigc::bind(sigc::ptr_fun(on_drawing_area_draw), color));

    auto box = Gtk::make_managed<Gtk::Box>();
    box->set_halign(Gtk::Align::FILL);
    box->append(*label);
    box->append(*drawing_area);

    return box;
  }

};


int main(int argc, char **argv)
{
  auto application = Gtk::Application::create("org.tavmjong.dropdown");
  return application->make_window_and_run<Window>(argc, argv);
}


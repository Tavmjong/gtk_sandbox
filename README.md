A repository of GTK 3 and GTK4 tests.

Each sub-directory contains a test with a README.

This list is not up-to-date with all tests.

##gtk_event

An experiment in using event controllers for Gtk3, Gtk3, Gtkmm3, and Gtkmm4.

##gtkmm_action

An experiment in using GActions.

Four actions are defined:

1. An action that triggers an event.
2. An action that toggles a value.
3. An action that selects an integer.
4. An action that selects a string.

A GUI is built from an XML .ui file.

Each action is exercised in a variety of way (triggering console output):

1. Internally via code (before GUI is constructed).
2. In a Gio::Menu menu. Actions 3 and 4 are presented two different ways, once as a set of radial buttons and once as a drop-down menu.
3. In a Gtk::Menubar.
4. In a toolbar (ToolButton, ToggleToolButton, RadioToolButton RadioToolButton).
5. As a normal GTK items (Button, ToggleButton, RadioButton, RadioButton).

Labels and separators are also used. 

Adding a SpinButton works if added via code. Fails if added via XML. The SpinButton is not useful in "menu" mode (when falling of the end of toolbar).

Commented out as we are almost finished with removing GtkActions from our code:
The bottom part of the window contains an attempt to mix GtkActions and GActions which fails as test-application.ui must be read to create the ActionGroup before the action can be added.


##gtkmm_paned

An experiment for a new way to handle dialogs in Inkscape based on a "multi-paned" widget and using Notebooks (inspired by Gimp's interface). This method of handling dialogs was favored by a group of designers at the Seville LGM meeting. This has been implemented in Inkscape.


##gtkmm_spinbutton

An experiment in implementing a custom spin button per https://gitlab.com/inkscape/ux/-/issues/25
both in GTK3 and GTK4.

##gtkmm_ui

An experiment in creating the same user interface in Gtkmm3 and Gtkmm4 (using actions).


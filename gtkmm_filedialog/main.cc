
// FileChooserNative duplicates much of the API of FileChooserDialog
// but it does not inherit it (except from FileChooser). Thus all the
// dialog related code must be duplicated. Yuck!
// See: https://gnome.pages.gitlab.gnome.org/gtkmm/classGtk_1_1NativeDialog.html#details

// FileChooserNative and FileChooserDialog are deprecated in Gtk4. Use FileDialog instead.

#include <iomanip>
#include <iostream>
#include <vector>

#include <giomm.h>
#include <gtkmm.h>

#ifdef _WIN32
#include <windows.h>
#endif

#if GTK_CHECK_VERSION(4, 0, 0)
std::vector<std::pair<std::string, std::string>> filter_data =
  {
    { "Any",    "*"   },
    { "SVG",    "svg" },
    { "PDF",    "pdf" },
    { "PNG",    "png" },
    { "WMF",    "wmf" }
  };
#else
std::vector<std::pair<std::string, std::string>> filter_data =
  {
    { "Any",    "*"     },
    { "SVG",    "*.svg" },
    { "PDF",    "*.pdf" },
    { "PNG",    "*.png" },
    { "WMF",    "*.wmf" }
  };
#endif

std::vector<Glib::ustring> options =
  {"0", "2", "4", "8"};

std::vector<Glib::ustring> options_labels =
  {"Zero", "Two", "Four", "Eight"};

#if GTK_CHECK_VERSION(4, 0, 0)
Gtk::Widget* find_by_name(Gtk::Widget* widget, Glib::ustring name) {
  // std::cout << "Checking: " << widget->get_name() << std::endl;
  if (widget->get_name() == name) {
    return widget;
  }
  for (auto child = widget->get_first_child(); child; child = child->get_next_sibling()) {
    if (auto found = find_by_name(child, name)) {
      return found;
    }
  }
  return nullptr;
}

// NOT USED: Replacing model in FileFilter dropdown leads to errors.
// Inkscape 1.3 has file filter entries that are disabled if the
// dependencies of an import/export option are not satisfied.
class ModelColumns : public Glib::Object {
public:
  static Glib::RefPtr<ModelColumns> create(const Glib::ustring& label,
                                           const Glib::ustring& extension,
                                           const bool enable) {
    return Glib::make_refptr_for_instance<ModelColumns>(new ModelColumns(label, extension, enable));
  }
  Glib::ustring label;
  Glib::ustring extension;
  bool enable = true;
protected:
  ModelColumns(const Glib::ustring& label,
               const Glib::ustring& extension,
               const bool enable)
    : Glib::ObjectBase(typeid(ModelColumns))
    , label(label)
    , extension(extension)
    , enable(enable)
  {
    // Could add signal here.
  }
};
#endif

void filter_changed(Gtk::FileChooser *filechooser) {
    auto filefilter = filechooser->property_filter();
    std::cout << "Selected: " << filefilter.get_value()->get_name() << std::endl;
}

// Common setup for Gtk::FileChooserDialog and Gtk::FileChooserNative.
void setup_filechooser(Gtk::FileChooser &filechooser) {
  std::cout << "setup_filechooser" << std::endl;

  // Add filters.
  for (auto data : filter_data) {
    auto filefilter = Gtk::FileFilter::create();
    filefilter->set_name(data.first);
#if GTK_CHECK_VERSION(4, 0, 0)
    if (data.second == "*") {
      filefilter->add_pattern(data.second);
    } else {
      filefilter->add_suffix(data.second); // Case independent matching.
    }
#else
    filefilter->add_pattern(data.second);
#endif
    filechooser.add_filter(filefilter);
  }

  auto filefilter = Gtk::FileFilter::create();
  filefilter->set_name("Bitmaps"); // All pixbuf supported formats (including SVG!).
  filefilter->add_pixbuf_formats();
  filechooser.add_filter(filefilter);

  auto mimefilter = Gtk::FileFilter::create();
  mimefilter->set_name("JPEG (via mimetype)");
  mimefilter->add_mime_type("image/jpeg");
  filechooser.add_filter(mimefilter);

  // Add "Choices". Will not work with Gtk::FileChooserNative on MacOS.
  filechooser.add_choice("bool", "Save as SVG 1.1");
  filechooser.add_choice("id", "Multiplier", options, options_labels);

  // Does not work for Gtk::FileChooserNative!
  std::cout << "Adding property_filter().signal_changed() callback!" << std::endl;
  filechooser.property_filter().signal_changed().connect(sigc::bind(sigc::ptr_fun(filter_changed), &filechooser));

  // Cannot access dropdown here... Gtk::FileChooser is an interface, not a widget!
}

#if GTK_CHECK_VERSION(4, 0, 0)
// Common handler for Gtk::FileChooserDialog and Gtk::FileChooserNative.
void handle_response(int response_id, Gtk::FileChooser *filechooser) {

  switch (response_id) {
  case Gtk::ResponseType::OK:
  case Gtk::ResponseType::ACCEPT: // Native dialog (could fix).
    {
      std::cout << " Single file" << std::endl;
      auto filename = filechooser->get_file()->get_path();
      auto uri      = filechooser->get_file()->get_uri();
      auto display  = filechooser->get_file()->get_parse_name();
      std::cout << "  filename:   " << filename << std::endl;
      std::cout << "  uri: " << uri << std::endl;
      std::cout << "  display:    " << display << std::endl;

      auto recentmanager = Gtk::RecentManager::get_default();
      // recentmanager->add_item(uri); // Added below.

      std::cout << " Multiple files" << std::endl;
      auto files = filechooser->get_files2();
      for (auto const &file : files) {
        auto filename = file->get_path();
        auto uri      = file->get_uri();
        auto display  = file->get_parse_name();
        std::cout << "  filename:   " << filename << std::endl;
        std::cout << "  uri: " << uri << std::endl;
        std::cout << "  display:    " << display << std::endl;
        recentmanager->add_item(uri);
      }

      // Get filefilter.
      auto filefilter = filechooser->get_filter();
      std::cout << "Got FileFilter: " << filefilter->get_name() << std::endl;

      // Get choices.
      std::cout << "Choice: bool: " << filechooser->get_choice("bool") << std::endl;
      std::cout << "Choice: id:   " << filechooser->get_choice("id") << std::endl;

      break;
    }
  case Gtk::ResponseType::CANCEL:
    {
      std::cout << "Cancel clicked" << std::endl;
      break;
    }
  default:
    {
      std::cout << "Unexpected button clicked: " << response_id << std::endl;
    }
  }
} // handle_response()
#endif

class MyFileChooserDialog : public Gtk::FileChooserDialog
{
public:
#if GTK_CHECK_VERSION(4, 0, 0)
  MyFileChooserDialog(Glib::ustring const &string, Gtk::FileChooser::Action action)
#else
  MyFileChooserDialog(Glib::ustring const &string, Gtk::FileChooserAction action)
#endif
    : Gtk::FileChooserDialog(string, action)
  {
    std::cout << "MyFileChooserDialog::MyFileChooserDialog" << std::endl;

#if GTK_CHECK_VERSION(4, 0, 0)
    if (action == Gtk::FileChooser::Action::OPEN) {
#else
    if (action == Gtk::FILE_CHOOSER_ACTION_OPEN) { // Can't set for saves!
#endif
      set_select_multiple(true);
    }

    setup_filechooser(*this);

    add_button("Help", GTK_RESPONSE_HELP); // Cannot use with Native or new Gtk4 file dialogs.

#if GTK_CHECK_VERSION(4, 0, 0)
    // Find dropdown and see what it contains.
    if (dropdown = dynamic_cast<Gtk::DropDown*>(find_by_name(this, "GtkDropDown"))) {
      std::cout << "Found dropdown" << std::endl;
      auto model = dropdown->get_model();
      std::cout << "  Size of model: " << model->get_n_items() << std::endl;
      GType type = model->get_item_type();
      std::cout << "  Type: " << (g_type_name(type) ? g_type_name(type) : "null")<< std::endl;

      // Create new model.
      // THIS DOES NOT WORK "gtk_file_filter_get_name: assertion 'GTK_IF_FILE_FILTER (filter)' failed"
      // Might be able to derive from Gtk::FileFilter? Maybe not necessary.
      // std::cout << "Creating new list store" << std::endl;
      auto list_store = Gio::ListStore<ModelColumns>::create();
      list_store->append(ModelColumns::create("Any", "*",     true));
      list_store->append(ModelColumns::create("SVG", "*.svg", true));
      list_store->append(ModelColumns::create("PNG", "*.png", false));
      // dropdown->set_model(list_store);
    } else {
      std::cout << "Did not find dropdown" << std::endl;
    }

    signal_response().connect(sigc::mem_fun(*this, &MyFileChooserDialog::on_response));
#endif

  }

#if GTK_CHECK_VERSION(4, 0, 0)
  void on_response(int response_id) {
    std::cout << "on_response: " << response_id << std::endl;
    handle_response(response_id, this);

    // Get filefilter from dropdown. filefilter can be gottne directly, see above.
    auto object = dropdown->get_selected_item();
    if (object) {
      std::cout << "Got object" << std::endl;
      auto filefilter = std::dynamic_pointer_cast<Gtk::FileFilter>(object);
      if (filefilter) {
        std::cout << "Got FileFilter: " << filefilter->get_name() << std::endl;
      }
    }

    delete this;
  } // on_response()

  private:
  Gtk::DropDown* dropdown = nullptr;
#endif
}; // MyFileChooserDialog

// -------------------------------------------- //

class MyFileChooserNative : public Gtk::FileChooserNative
{
public:
#if GTK_CHECK_VERSION(4, 0, 0)
  MyFileChooserNative(Glib::ustring const &string, Gtk::FileChooser::Action action)
#else
  MyFileChooserNative(Glib::ustring const &string, Gtk::FileChooserAction action)
#endif
    : Gtk::FileChooserNative(string, action, "Accept", "Cancel")
  {
    std::cout << "MyFileChooserNative::MyFileChooserNative" << std::endl;

#if GTK_CHECK_VERSION(4, 0, 0)
    if (action == Gtk::FileChooser::Action::OPEN) {
#else
    if (action == Gtk::FILE_CHOOSER_ACTION_OPEN) { // Can't set for saves!
#endif
      set_select_multiple(true);
    }

    setup_filechooser(*this);

#if GTK_CHECK_VERSION(4, 0, 0)
    // Cannot find dropdown as Gtk::FileChooserNative is not a widget!

    signal_response().connect(sigc::mem_fun(*this, &MyFileChooserNative::on_response));
#endif
  } // MyFileChooserNative

#if GTK_CHECK_VERSION(4, 0, 0)
  void on_response(int response_id) {
    std::cout << "on_response: " << response_id << std::endl;
    handle_response(response_id, this);
    delete this;
  } // on_response()
#endif
}; // MyFileChooserNative

// -------------------------------------------- //

#if GTK_CHECK_VERSION(4, 0, 0)
class MyFileDialog : public Gtk::FileDialog
{
protected:
  MyFileDialog()
    : Gtk::FileDialog()
  {
    std::cout << "MyFileDialog::MyFileDialog" << std::endl;
    auto filefilters = Gio::ListStore<Gtk::FileFilter>::create();
    for (auto data : filter_data) {
      auto filefilter = Gtk::FileFilter::create();
      filefilter->set_name(data.first);
      if (data.second == "*") {
	filefilter->add_pattern(data.second);
      } else {
	filefilter->add_suffix(data.second);
      }
      filefilters->append(filefilter);
    }

    auto filefilter = Gtk::FileFilter::create();
    filefilter->set_name("Bitmaps"); // All pixbuf supported formats (including SVG!).
    filefilter->add_pixbuf_formats();
    filefilters->append(filefilter);

    auto mimefilter = Gtk::FileFilter::create();
    mimefilter->set_name("JPEG (via mimetype)");
    mimefilter->add_mime_type("image/jpg");
    filefilters->append(mimefilter);

    set_filters(filefilters);

    // add_choice("bool", "Save as SVG 1.1");
    // add_choice("id", "Multiplier", options, options_labels);
  }

public:
  static Glib::RefPtr<MyFileDialog> create() {
    return Glib::RefPtr<MyFileDialog>(new MyFileDialog());
  }
}; // MyFileDialog
#endif

// -------------------------------------------- //

class Window: public Gtk::ApplicationWindow
{
public:
#if GTK_CHECK_VERSION(4, 0, 0)
  Window()
#else
  Window(const Glib::RefPtr<Gtk::Application>& app)
#endif
  {
    set_title("File Dialog Test");

    auto button0 = Gtk::make_managed<Gtk::Button>("Open-Dialog");
    button0->signal_clicked().connect(sigc::bind(sigc::mem_fun(*this, &Window::on_click), 0));

    auto button1 = Gtk::make_managed<Gtk::Button>("Open-Native");
    button1->signal_clicked().connect(sigc::bind(sigc::mem_fun(*this, &Window::on_click), 1));

#if GTK_CHECK_VERSION(4, 0, 0)
    auto button2 = Gtk::make_managed<Gtk::Button>("Open-Gtk4");
    button2->signal_clicked().connect(sigc::bind(sigc::mem_fun(*this, &Window::on_click), 2));
#endif

    auto button3 = Gtk::make_managed<Gtk::Button>("Save-Dialog");
    button3->signal_clicked().connect(sigc::bind(sigc::mem_fun(*this, &Window::on_click), 3));

    auto button4 = Gtk::make_managed<Gtk::Button>("Save-Native");
    button4->signal_clicked().connect(sigc::bind(sigc::mem_fun(*this, &Window::on_click), 4));

#if GTK_CHECK_VERSION(4, 0, 0)
    auto button5 = Gtk::make_managed<Gtk::Button>("Save-Gtk4");
    button5->signal_clicked().connect(sigc::bind(sigc::mem_fun(*this, &Window::on_click), 5));
#endif

    auto grid = Gtk::make_managed<Gtk::Grid>();

    grid->attach(*button0, 0, 0);
    grid->attach(*button1, 1, 0);
    grid->attach(*button3, 0, 1);
    grid->attach(*button4, 1, 1);
#if GTK_CHECK_VERSION(4, 0, 0)
    grid->attach(*button2, 2, 0);
    grid->attach(*button5, 2, 1);
    set_child(*grid);
#else
    add(*grid);
    show_all();
#endif
  }

  void on_click(int type) {
    std::cout << "on_click" << std::endl;

#if GTK_CHECK_VERSION(4, 0, 0)
    Gtk::FileChooser::Action action = Gtk::FileChooser::Action::OPEN;
    if (type == 3 || type == 4) {
      action = Gtk::FileChooser::Action::SAVE;
    }
    if (type == 0 || type == 3) {
      auto dialog = new MyFileChooserDialog("Please choose a file", action);
      dialog->add_button("_Cancel",                     Gtk::ResponseType::CANCEL);
      dialog->add_button(type==0 ? "_Open" : "_Save",   Gtk::ResponseType::OK);
      dialog->set_transient_for(*this);
      dialog->set_modal(true);

      auto box = dialog->get_content_area();
      box->set_name("ContentArea");
      dialog->show();
    } else if (type == 1 || type == 4) {
      auto dialog = new MyFileChooserNative("Please choose a file", action);
      dialog->set_transient_for(*this);
      dialog->set_modal(true);
      dialog->show();
    } else {
      auto dialog = MyFileDialog::create();
      if (type == 2) {
        dialog->open_multiple(*this, sigc::bind(sigc::mem_fun(*this, &Window::on_file_dialog_open_multiple), dialog));
      } else {
        dialog->save(*this, sigc::bind(sigc::mem_fun(*this, &Window::on_file_dialog_save), dialog));
      }
      dialog->set_modal(true);
    }
#else
    // Gtk3
    // Can't share code between MyFileChooserDialog and MyFileChooserNative as they don't have a common dialog ancestor.
    auto action = Gtk::FILE_CHOOSER_ACTION_OPEN;
    if (type == 3 || type == 4) {
      action = Gtk::FILE_CHOOSER_ACTION_SAVE;
    }
    if (type == 0 || type == 3) {
      auto dialog = new MyFileChooserDialog("Please choose a file", action);
      dialog->add_button("_Cancel", Gtk::RESPONSE_CANCEL);
      dialog->add_button(type == 0 ? "_Open" : "_Save",   Gtk::RESPONSE_OK);
      dialog->set_transient_for(*this);

      int result = dialog->run();

      switch (result) {
      case (Gtk::RESPONSE_OK):
        {
          std::cout << "Select clicked." << std::endl;
          std::cout << "  File selected: " << dialog->get_filename() << std::endl;
          std::cout << "  URI: " << dialog->get_uri() << std::endl;
          std::cout << "  Basename: " << Glib::path_get_basename(dialog->get_filename()) << std::endl;
          std::cout << "  Basename: " << Glib::path_get_basename(dialog->get_uri()) << std::endl;
          std::cout << " Multiple files" << std::endl;
          for (auto filename : dialog->get_filenames()) {
            std::cout << "  filename:   " << filename << std::endl;
          }
          break;
        }
      case (Gtk::RESPONSE_CANCEL):
        {
          std::cout << "Cancel clicked." << std::endl;
          break;
        }
      default:
        {
          std::cout << "Unexpected button clicked! (Dialog)" << std::endl;
        }
      }
      delete dialog;
    } else {
      auto dialog = new MyFileChooserNative("Please choose a file", action);
      int result = dialog->run();
      std::cout << "MyFileChooserNative: " << result << std::endl;
      switch (result) {
      case (Gtk::RESPONSE_ACCEPT):
        {
          std::cout << "Accept clicked." << std::endl;
          std::cout << "  File selected: " << dialog->get_filename() << std::endl;
          std::cout << "  URI: " << dialog->get_uri() << std::endl;
          std::cout << " Multiple files" << std::endl;
          for (auto filename : dialog->get_filenames()) {
            std::cout << "  filename:   " << filename << std::endl;
          }
          break;
        }
      case (Gtk::RESPONSE_CANCEL):
        {
          std::cout << "Cancel clicked." << std::endl;
          break;
        }
      default:
        {
          std::cout << "Unexpected button clicked (Native)!" << std::endl;
        }
      }
      delete dialog;
    }
#endif
  } // on_click()

#if GTK_CHECK_VERSION(4, 0, 0)
  void on_file_dialog_save(Glib::RefPtr<Gio::AsyncResult>& result, Glib::RefPtr<Gtk::FileDialog> dialog) {
    try {
      auto file = dialog->save_finish(result);
      if (file) {
        std::cout << "  filename:   " << file->get_path() << std::endl;
        std::cout << "  uri: "        << file->get_uri()  << std::endl;
        std::cout << "  display:    " << file->get_parse_name() << std::endl;
	// auto file_info = file->query_info();
	// auto file_info = file->query_info(G_FILE_ATTRIBUTE_ACCESS_CAN_WRITE);
	auto file_info = file->query_info("access::can-write,standard::content-type,standard::fast-content-type");
	std::cout << "  content_type: " << file_info->get_content_type() << std::endl;
	auto attributes = file_info->list_attributes("");
	for (auto attribute : attributes) {
	  std::cout << "  FileInfo: attribute: " << std::setw(24) << std::left << attribute
		    << "  value: " << file_info->get_attribute_as_string(attribute)
		    << std::endl;
	}
      } else {
        std::cout << "  Cancelled" << std::endl;
      }
    }
    catch (const Glib::Error& err) {
      std::cout << "Exception: " << err.what() << std::endl;
    }
  }

  void on_file_dialog_open_multiple(Glib::RefPtr<Gio::AsyncResult>& result, Glib::RefPtr<Gtk::FileDialog> dialog) {
    try {
      std::cout << "on_file_dialog_open_multiple: Entrance" << std::endl;
      auto files = dialog->open_multiple_finish(result);
      std::cout << "  number of files: " << files.size() << std::endl;
      for (auto file : files) {
        //      auto file = std::dynamic_pointer_cast<Gio::File>(list_model->get_object(0));
        std::cout << "  filename:   " << file->get_path() << std::endl;
        std::cout << "  uri: "        << file->get_uri()  << std::endl;
        std::cout << "  display:    " << file->get_parse_name() << std::endl;
      }
    }
    catch (const Glib::Error& err) {
      std::cout << "Exception: " << err.what() << std::endl;
    }
  }
#endif

}; // Window class

int main(int argc, char **argv) {

  std::cout << "Home directory:      " << Glib::get_home_dir() << std::endl;
  std::cout << "Temp directory:      " << Glib::get_tmp_dir() << std::endl;
  std::cout << "Data directory:      " << Glib::get_user_data_dir() << std::endl;
  std::cout << "Config directory:    " << Glib::get_user_config_dir() << std::endl;
  std::cout << "Document directory:  " << Glib::get_user_special_dir(Glib::UserDirectory::DOCUMENTS) << std::endl;
  std::cout << "Current directory:   " << Glib::get_current_dir() << std::endl;

#ifdef _WIN32
  // The path to the My Documents folder is read from the
  // value "HKEY_CURRENT_USER\Software\Windows\CurrentVersion\Explorer\Shell Folders\Personal"
  std::string start_path;

  HKEY key = NULL;
  if (RegOpenKeyExA(HKEY_CURRENT_USER,
		    "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders",
		    0, KEY_QUERY_VALUE, &key) == ERROR_SUCCESS) {
    WCHAR utf16path[_MAX_PATH];
    DWORD value_type;
    DWORD data_size = sizeof(utf16path);
    if(RegQueryValueExW(key, L"Personal", NULL, &value_type,
			(BYTE*)utf16path, &data_size) == ERROR_SUCCESS) {
      g_assert(value_type == REG_SZ);
      gchar *utf8path = g_utf16_to_utf8((const gunichar2*)utf16path, -1, NULL, NULL, NULL);
      if (utf8path) {
	start_path = Glib::ustring(utf8path); // Glib::ustring -> std::string
	g_free(utf8path);
      }
    }
    std::cout << "Windows \"My Document\" folder: " << start_path << std::endl;
  }

#endif

  // Look at content-type for a given directory (given as argument).
  if (argc > 1) {
    std::cout << std::endl;
    std::cout << "WARNING: Listing directory will prevent application from running properly!" << std::endl;

    Gio::init(); // Must init!!
    auto directory = Gio::File::create_for_path(argv[1]);
    if (directory) {
      auto children = directory->enumerate_children("standard::display-name,standard::content-type,standard::fast-content-type");

      while (auto fileinfo = children->next_file()) {
	std::cout << std::setw(24) << std::left << fileinfo->get_attribute_as_string("standard::display-name") << " "
		  << std::setw(24) << std::left << fileinfo->get_attribute_as_string("standard::content-type") << " "
		  << std::setw(24) << std::left << fileinfo->get_attribute_as_string("standard::fast-content-type")
		  << std::endl;
      }
    } else {
      std::cerr << "Failed to find directory: " << argv[1] << std::endl;
    }
    return 0;
  }

#if GTK_CHECK_VERSION(4, 0, 0)
  auto application = Gtk::Application::create("org.tavmjong.custom_mm4");
  return application->make_window_and_run<Window>(argc, argv);
#else
  auto application = Gtk::Application::create(argc, argv, "org.tavmjong.custom_mm3");
  Window window(application);
  return application->run(window);
#endif

}

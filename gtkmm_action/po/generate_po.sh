
# Usage: ./generate_po.sh fr

# Generate localizable file.
msginit --locale=${1} --input=test-application.pot

# Next translate .po file.


#include "test-application.h"

int main(int argc, char *argv[])
{

  auto application = TestApplication::create();

  const int status = application->run(argc, argv);
  return status;
}
  

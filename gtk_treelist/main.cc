
#include <cassert>
#include <iostream>
#include <ostream>
#include <ranges>
#include <string>

#include <gtkmm.h>

/*

  Some Explanation:

  class Gtk::TreeListModel:
    A data class for a tree where each non-leaf node contains a Gio::ListStore
    corresponding to the node's children.

    This class treats the tree as a unified object with each node
    having a unique index number.

  class Gtk::ListView:
    A widget class for displaying lists. It can display a
    Gtk::TreeListModel, with expanders to show/hide branches.

  class Element:
    A simple class that contains a Glib::ustring "name" plus a vector
    of pointers to child Elements. It is used to build a tree of
    Elements, like one would find in an XML document.

  class ModelColumns : public Glib::Object:
    A class that reflects an Element. The Gtk::TreeListModel contains
    Gio::ListModel's that store items of this class.
    It provides access to data needed by Gtk::ListView to create a row as
    well as pointers to children (if not a leaf node).

  create_model():
    A function for building the a Gtk::TreeListModel from the Element
    tree.  It recursively takes a ModelColumns and returns a
    Gio::ListStore of ModelColumns' children (or a nullptr if
    none). It is passed to the 'create' function for the
    Gtk::TreeListModel. The root Gio::ListModel is created if no
    argument is passed in.

  To keep the changes to the Element tree (adding, removing, or moving
  a node) insync with the Gtk::TreeListModel either:

  1. The node containing the change can be regenerated via a call to
     create_model(). This can be done by closing and opening the
     expander corresponding to the node.

  2. The Gio::ListModel that contains the node can be altered directly.
     But this results in a lot of extra code.

  If autoexpand is true:
    Tree is open at start.
    create_model is called for every node.
    Opening/closing expander calls create_model for expander's Gio::ListStore.
    Scrolling to show/hide expander does nothing.

  If autoexpand is false:
    Tree is closed at start.
    create_model is called for root (creates ModelColumns for each child).
    Opening/closing expander calls create_model for expander's Gio::ListStore.
    Scrolling to show expander may call create_model for expander's
      Gio::ListStore if expander is closed and one scrolls far enough
      away from expander.
*/

class Element
{
public:
  Element() = default;
  Element(Glib::ustring name, bool group = true) : name(name), group(group) {}; // Root is always group!
  Element(Glib::ustring name, Element *parent, bool group) : name(name), parent(parent), group(group) {};
  Element(           const Element&  element) = default;
  Element(                 Element&& element) = default;
  Element& operator=(const Element&  element) = default;
  Element& operator=(      Element&  element) = default;
  ~Element() {
    for (auto child : children) {
      delete child;
    }
    children.clear();
  }

  Element* add(Glib::ustring name, bool group = false) {
    Element* child = new Element(name, this, group);
    children.push_back(child);
    return child;
  }

  Glib::ustring name;
  std::vector<Element*> children;
  Element* parent = nullptr;
  bool group = false;
}; // Element

class Window: public Gtk::ApplicationWindow
{

public:
  Window()
  {
    set_title("TreeList Example");
    set_default_size(300, 500);

    /* STYLING */

    auto provider = Gtk::CssProvider::create();
    try {
      provider->load_from_path("treelist.css");
    }
    catch (...) {
    }
    auto const display = Gdk::Display::get_default();
    Gtk::StyleContext::add_provider_for_display(display, provider, GTK_STYLE_PROVIDER_PRIORITY_USER);

    /* LIST */

    add_items();
    root_list_model = create_model();

    tree_list_model = Gtk::TreeListModel::create(root_list_model,
                                                 sigc::mem_fun(*this, &Window::create_model),
                                                 false,  // passthrough
                                                 true);  // autoexpand
    auto selection = Gtk::MultiSelection::create(tree_list_model);

    auto factory = Gtk::SignalListItemFactory::create();
    factory->signal_setup().connect(sigc::mem_fun(*this, &Window::setup_listitem_cb));
    factory->signal_bind().connect(sigc::mem_fun(*this, &Window::bind_listitem_cb));

    listview = Gtk::make_managed<Gtk::ListView>(selection, factory);
    listview->signal_activate().connect(sigc::bind<0>(sigc::mem_fun(*this, &Window::activate_cb), listview));

    // Drop target for moving item to end of tree.
    static bool BEFORE = false;
    auto droptarget = Gtk::DropTarget::create(Glib::Value<std::shared_ptr<Gtk::TreeListRow>>::value_type(), Gdk::DragAction::MOVE);
    droptarget->signal_drop().connect([this](auto value, auto, auto) -> bool {

      auto src = static_cast<Glib::Value<std::shared_ptr<Gtk::TreeListRow>> const &>(value).get();

      // Should the drop be in the last group or at the Svg level? Or maybe not implement this at all?
      auto end_number = tree_list_model->property_n_items().get_value();
      auto dst = tree_list_model->get_row(end_number-1);

      move_row (src, dst, DROP_AFTER);

      return true;
    },
      BEFORE);
    listview->add_controller(droptarget);

    // It is expected that a ListView is inside a ScrolledWindow!
    auto scrolled_window = Gtk::make_managed<Gtk::ScrolledWindow>();
    scrolled_window->set_policy(Gtk::PolicyType::NEVER, Gtk::PolicyType::AUTOMATIC);
    scrolled_window->set_child(*listview);

    auto box = Gtk::make_managed<Gtk::Box>();
    box->append(*scrolled_window);

    auto button_up    = Gtk::make_managed<Gtk::Button>("Up");
    auto button_down  = Gtk::make_managed<Gtk::Button>("Down");
    auto button_in    = Gtk::make_managed<Gtk::Button>("In");
    auto button_out   = Gtk::make_managed<Gtk::Button>("Out");
    auto button_del   = Gtk::make_managed<Gtk::Button>("Delete");
    auto button_add   = Gtk::make_managed<Gtk::Button>("Add");
    auto button_ext   = Gtk::make_managed<Gtk::Button>("External");
    auto button_dump  = Gtk::make_managed<Gtk::Button>("Print\nElement\nTree");
    auto button_dump2 = Gtk::make_managed<Gtk::Button>("Print\nModel\nTree");
    auto button_verify= Gtk::make_managed<Gtk::Button>("Verify\nTrees");

    button_up->signal_clicked().connect([this]() {
      std::cout << "Clicked up" << std::endl;
      move_row (MoveDirection::UP);
      verify_trees();
    } );

    button_down->signal_clicked().connect([this]() {
      std::cout << "Clicked down" << std::endl;
      move_row (MoveDirection::DOWN);
      verify_trees();
    } );

    button_in->signal_clicked().connect([this]() {
      std::cout << "Clicked in" << std::endl;
      move_row_in();
      verify_trees();
    });

    button_out->signal_clicked().connect([this]() {
      std::cout << "Clicked out" << std::endl;
      move_row_out();
      verify_trees();
    });

    button_del->signal_clicked().connect([this]() {
      std::cout << "Clicked delete" << std::endl;
      delete_row();
      verify_trees();
    });

    button_add->signal_clicked().connect([this]() {
      std::cout << "Clicked add" << std::endl;
      add_row();
      verify_trees();
    });

    button_ext->signal_clicked().connect([this]() {
      std::cout << "Clicked ext" << std::endl;
      external_test("Square", "Circle");
      verify_trees();
    });

    button_dump->signal_clicked().connect([this]() {
      std::cout << "Clicked Element Tree" << std::endl;
      print_element_tree(&root_element);
    });

    button_dump2->signal_clicked().connect([this]() {
      std::cout << "Clicked Model Tree" << std::endl;
      print_model_tree();
    });

    button_verify->signal_clicked().connect([this]() {
      std::cout << "Clicked Verify Trees" << std::endl;
      verify_trees();
    });

    auto button_box = Gtk::make_managed<Gtk::Box>();
    button_box->append(*button_up);
    button_box->append(*button_down);
    button_box->append(*button_in);
    button_box->append(*button_out);
    button_box->append(*button_del);
    button_box->append(*button_add);
    button_box->append(*button_ext);
    button_box->append(*button_dump);
    button_box->append(*button_dump2);
    button_box->append(*button_verify);

    box->append(*button_box);

    set_child(*box);

    present();
  }

protected:

  class ModelColumns : public Glib::Object
  {
  public:
    Element* element;
    std::vector<Element*> children;

    static Glib::RefPtr<ModelColumns> create(Element* element)
    {
      return Glib::make_refptr_for_instance<ModelColumns>(new ModelColumns(element));
    }

  protected:
    ModelColumns(Element* element)
      : element(element)
      , children(element->children)
    { /* std::cout << "ModelColumns::ModelColumns: " << element->name << std::endl; */ }
  }; // ModelColumns

  Glib::RefPtr<Gio::ListStore<ModelColumns>> create_model(const Glib::RefPtr<Glib::ObjectBase>& base = {})
  {
    auto col = std::dynamic_pointer_cast<ModelColumns>(base);
    // std::cout << "create_model: " << (col ? col->element->name : "null") << std::endl;

    if (col && col->children.empty() && !col->element->group) {
      // An item without children, i.e. a leaf in a the tree.
      return {};
    }

    auto result = Gio::ListStore<ModelColumns>::create();
    const std::vector<Element*>& children = col ? col->element->children : root_element.children;
    for (const auto& child : children) {
      result->append(ModelColumns::create(child));
    }
    return result;
  };

  void add_items() {
    auto svg = root_element.add("Svg", true);

    auto group = svg->add("Group1", true);
    group->add("Circle");
    group->add("Square");

    auto group2 = group->add("Group2", true);
    group2->add("Path1");
    group2->add("Path2");
    group2->add("Path3");
    group2->add("Path4");

    group->add("Triangle");

    svg->add("Rect");
    svg->add("Text");
    svg->add("Path");
    svg->add("GroupX", true);

    // Add an arbitrary number of objects to test scrolling long lists.
    for (int i = 0; i < 2; ++i) {
      std::string name = "Object" + std::to_string(i);
      svg->add(name);
    }

    auto group3 = svg->add("Group3", true);
    group3->add("Text1");
    group3->add("Text2");
    group3->add("Text3");
    group3->add("Text4");
  }

  // Quite awkward!!
  void select(Element* element) {

    auto model = std::dynamic_pointer_cast<Gtk::MultiSelection>(listview->get_model());
    assert (model);

    for (auto i = 0; i < tree_list_model->property_n_items(); ++i) {
      auto col = std::dynamic_pointer_cast<ModelColumns>(tree_list_model->get_row(i)->get_item());
      if (col->element == element) {
        model->select_item(i, true);
        break;
      }
    }
  }

  // Find element from name. Used for testing.
  Element*
  find_element(Glib::ustring name, Element* parent) {
    auto& element_siblings = parent->children;

    Element* element = nullptr;
    if (parent->name == name) {
      element = parent;
      std::cout << "find_element: found: " << name << std::endl;
    } else {
      for (auto child : parent->children) {
        element = find_element(name, child);
        if (element != nullptr) {
          break;
        }
      }
    }
    return element;
  }

  std::pair<std::vector<Element*>&, std::vector<Element*>::iterator>
  find_element2(Glib::ustring name, Element* parent) {
    std::vector<Element*>& vector = parent->children;

    auto is_match = [name](Element* element) { return element->name == name; };

    auto iterator = std::find_if(vector.begin(), vector.end(), is_match);
    if (iterator != vector.end()) {
      return std::make_pair(std::ref(vector), iterator);
    } else {
      for (auto child : parent->children) {
        auto pair = find_element2(name, child);
        if (pair.second != pair.first.end()) {
          return (pair);
        }
      }
    }

    // Couldn't find match, iterator point to end().
    return std::make_pair(std::ref(vector), iterator);
  }

  // Find row corresponding to Element. This can be used to determine the parent inorder to update Gio::ListStore.
  Glib::RefPtr<Gtk::TreeListRow> find_row(Element *element) {

    auto model = std::dynamic_pointer_cast<Gtk::MultiSelection>(listview->get_model());
    assert (model);

    for (auto i = 0; i < tree_list_model->property_n_items(); ++i) {
      auto row = tree_list_model->get_row(i);
      auto col = std::dynamic_pointer_cast<ModelColumns>(row->get_item());
      if (col->element == element) {
        return row;
        break;
      }
    }
    std::cerr << "get_row(): Element not found!!" << std::endl;
    return Glib::RefPtr<Gtk::TreeListRow> {};
  }

  void delete_row() {

    auto model = std::dynamic_pointer_cast<Gtk::MultiSelection>(listview->get_model());
    assert (model);

    auto selection = model->get_selection();
    assert (selection);

    // Only delete a single row.
    auto it = selection->begin();
    auto object = model->get_object(*it);
    if (!object) {
      std::cerr << "No item selected!" << std::endl;
      return;
    }

    auto row = std::dynamic_pointer_cast<Gtk::TreeListRow>(object);
    assert (row);

    auto row_position = row->get_position();
    if (row_position == 0) {
      std::cerr << "Can't delete root element!" << std::endl;
      return;
    }

    // Get row_parent for later use in toggling expaners (row will be invalid then).
    auto row_parent = row->get_parent();
    assert (row_parent);

    auto [element_siblings, element_it] = find_element(row);

    auto element = *element_it;
    delete (element);
    element = nullptr;

    element_siblings.erase(element_it);

    // Toggle expanders to update
    row_parent->set_expanded(false);
    row_parent->set_expanded(true);
 }

  void add_row() {

    auto model = std::dynamic_pointer_cast<Gtk::MultiSelection>(listview->get_model());
    assert (model);

    auto selection = model->get_selection();
    assert (selection);

    // Only a single row.
    auto it = selection->begin();
    auto object = model->get_object(*it);
    if (!object) {
      std::cerr << "No item selected!" << std::endl;
      return;
    }

    auto row = std::dynamic_pointer_cast<Gtk::TreeListRow>(object);
    assert (row);

    auto [element_siblings, element_it] = find_element(row);

    if (!(*element_it)->group) {
      std::cerr << "Can only add new item to a group!" << std::endl;
      return;
    }

    Element* element = new Element("New", *element_it, false);
    (*element_it)->children.emplace_back(element);

    row->set_expanded(false);
    row->set_expanded(true);

    // Quite awkward!!
    select (element);
  }

  enum DropPlace {
    DROP_BEFORE,
    DROP_AFTER,
    DROP_INTO
  };

  /*
   * Find element in element tree.
   * Argument: row.
   * Return: std::pair containing std::vector that contains element and iterator pointing to it.
   */
  std::pair<std::vector<Element*>&, std::vector<Element*>::iterator>
  find_element(Glib::RefPtr<Gtk::TreeListRow> row)
  {
    // Find element corresponding to row.
    auto columns = std::dynamic_pointer_cast<ModelColumns>(row->get_item());
    assert (columns);

    auto element = columns->element;
    assert (element);

    auto& element_parent = element->parent;
    assert (element_parent);

    auto& element_siblings = element_parent->children;
    // assert (element_siblings);

    auto element_it = std::find(element_siblings.begin(), element_siblings.end(), element);
    if (element_it == element_siblings.end()) {
      std::cerr << "find_element: Couldn't find: " << element->name
                << " as child of " << element_parent->name << std::endl;
      for (auto child : element_siblings) {
        std::cerr << "  " << child->name << std::endl;
      }
    }
    assert (element_it != element_siblings.end());

    return std::make_pair(std::ref(element_siblings), element_it);
  }

  enum MoveDirection {
    UP,
    DOWN
  };

  /*
   * Move a row:
   *   src: row to move.
   *   dst: where to move.
   *   place: before, after, or dst.
   */
  void move_row(Glib::RefPtr<Gtk::TreeListRow> src, Glib::RefPtr<Gtk::TreeListRow> dst, DropPlace place) {

    assert (src);
    assert (dst);

    // Can't drop on to self.
    if (src == dst) {
      std::cerr << "move_row: Can't drop onto self!" << std::endl;
      return;
    }

    // Can't drop into self.
    auto parent = dst;
    while (parent) {
      if (src == parent) {
        std::cerr << "move_row: can't drop into self!" << std::endl;
        return;
      }
      // Need two steps, or segfaults:
      auto parent_new = parent->get_parent();
      parent = parent_new;
      if (!parent) {
        break;
      }
    }

    auto [element_src_siblings, element_src_it] = find_element(src);
    auto element = *element_src_it;
    element_src_siblings.erase(element_src_it);

    auto [element_dst_siblings, element_dst_it] = find_element(dst);

    // std::cout << "Moving " << element->name << " ";
    // switch (place) {
    // case DROP_INTO:   std::cout << "into ";   break;
    // case DROP_BEFORE: std::cout << "before "; break;
    // case DROP_AFTER:  std::cout << "after ";  break;
    // }
    // std::cout << (*element_dst_it)->name << std::endl;

    if (place == DROP_INTO) {
      element->parent = (*element_dst_it);
      (*element_dst_it)->children.push_back(element);

      // Toggle expanders to update
      auto parent_src = src->get_parent();
      assert (parent_src);
      parent_src->set_expanded(false);
      parent_src->set_expanded(true);

      dst->set_expanded(false);
      dst->set_expanded(true);
      print_element_tree(&root_element);
      return;
    }

    // DROP_BEFORE, DROP_AFTER
    element->parent = (*element_dst_it)->parent;
    if (place == DROP_AFTER) {
      element_dst_it++;
    }
    element_dst_siblings.insert(element_dst_it, element);

    // Toggle expanders to update
    auto parent_src = src->get_parent();
    assert (parent_src);
    parent_src->set_expanded(false);
    parent_src->set_expanded(true);

    auto parent_dst = dst->get_parent();
    if (parent_dst) {
      // dst can disappear if it is in same branch as src.
      parent_dst->set_expanded(false);
      parent_dst->set_expanded(true);
    }

    // Quite awkward!!
    select (element);
  }


  /*
   * Move a row one step up or down at the same tree level.
   */
  void move_row(MoveDirection direction)
  {
      auto model = std::dynamic_pointer_cast<Gtk::MultiSelection>(listview->get_model());
      assert (model);

      auto selection = model->get_selection();
      assert (selection);

      // Only move a single row.
      auto it = selection->begin();
      auto object = model->get_object(*it);
      if (!object) {
        std::cerr << "No item selected!" << std::endl;
        return;
      }

      auto row = std::dynamic_pointer_cast<Gtk::TreeListRow>(object);
      assert (row);

      auto row_position = row->get_position();
      if (row_position == 0) {
        std::cerr << "Can't move root element!" << std::endl;
        return;
      }

      // Get row_parent for later use in toggling expanders (row will be invalid then).
      auto row_parent = row->get_parent();
      assert (row_parent);

      auto [element_siblings, element_it] = find_element(row);

      if (direction == UP && element_it == element_siblings.begin()) {
        std::cerr << "Can't move top element up!" << std::endl;
        return;
      }


      if (direction == DOWN && element_it == std::prev(element_siblings.end())) {
        std::cerr << "Can't move bottom element down!" << std::endl;
        return;
      }

      auto insertion_it = element_it;
      insertion_it--; // Iterators before point of erasing are still valid!

      auto element = *element_it;

      element_siblings.erase(element_it);

      if (direction == DOWN) {
        insertion_it++;
        insertion_it++;
      }

      element_siblings.insert(insertion_it, element);

      // Toggle expanders to update
      row_parent->set_expanded(false);
      row_parent->set_expanded(true);

      // Quite awkward!!
      select (element);
  }

  /*
   * Move a row into group above.
   */
  void move_row_in()
  {
      auto model = std::dynamic_pointer_cast<Gtk::MultiSelection>(listview->get_model());
      assert (model);

      auto selection = model->get_selection();
      assert (selection);

      // Only move a single row.
      auto it = selection->begin();
      auto object = model->get_object(*it);
      if (!object) {
        std::cerr << "No item selected!" << std::endl;
        return;
      }

      auto row = std::dynamic_pointer_cast<Gtk::TreeListRow>(object);
      assert (row);

      auto row_position = row->get_position();
      if (row_position == 0) {
        std::cerr << "Can't move root element!" << std::endl;
        return;
      }

      // Get row_parent for later use in toggling expanders (row will be invalid then).
      auto row_parent = row->get_parent();
      assert (row_parent);

      auto [element_siblings, element_it] = find_element(row);

      if (element_siblings.begin() == element_it) {
        std::cerr << "Nothing above to move into!" << std::endl;
        return;
      }

      auto prev_it = std::prev(element_it);
      if (!(*prev_it)->group) {
        std::cerr << "Previous item is not Group!" << std::endl;
        return;
      }

      auto element = *element_it;

      element_siblings.erase(element_it);

      (*prev_it)->children.push_back(element); // prev_it still valid.
      element->parent = (*prev_it);

      // Toggle expanders to update
      row_parent->set_expanded(false);
      row_parent->set_expanded(true);

      // Quite awkward!!
      select (element);
  }

  /*
   * Move a row out of group.
   */
  void move_row_out()
  {
      auto model = std::dynamic_pointer_cast<Gtk::MultiSelection>(listview->get_model());
      assert (model);

      auto selection = model->get_selection();
      assert (selection);

      // Only move a single row.
      auto it = selection->begin();
      auto object = model->get_object(*it);
      if (!object) {
        std::cerr << "No item selected!" << std::endl;
        return;
      }

      auto row = std::dynamic_pointer_cast<Gtk::TreeListRow>(object);
      assert (row);

      auto row_position = row->get_position();
      if (row_position == 0) {
        std::cerr << "Can't move root element!" << std::endl;
        return;
      }

      auto row_parent = row->get_parent();
      assert (row_parent);

      auto row_parent_position = row_parent->get_position();
      if (row_parent_position == 0) {
        std::cerr << "Can't move to root level!" << std::endl;
        return;
      }

      // Get row_grandparent for later use in toggling expanders (row will be invalid then).
      auto row_grandparent = row_parent->get_parent();
      assert (row_grandparent);

      auto [element_siblings, element_it] = find_element(row);
      auto [element_parent_siblings, element_parent_it] = find_element(row_parent);

      auto element = *element_it;

      element_siblings.erase(element_it);

      element_parent_siblings.insert(std::next(element_parent_it), element);
      // element->parent->parent->children.push_back(element);
      element->parent = element->parent->parent;

      // Toggle expanders to update
      row_grandparent->set_expanded(false);
      row_grandparent->set_expanded(true);

      // Quite awkward!!
      select (element);
  }

  // A simple test of changing the Element tree directly, then updating rows.
  void external_test(Glib::ustring src, Glib::ustring dst) {

    auto [vector0, iterator0] = find_element2(src, &root_element);
    auto [vector1, iterator1] = find_element2(dst, &root_element);

    if (iterator0 == vector0.end()) {
      std::cerr << "Failed to find " << src << std::endl;
      return;
    }
    if (iterator1 == vector1.end()) {
      std::cerr << "Failed to find " << dst << std::endl;
      return;
    }

    auto element0 = *iterator0;
    auto element1 = *iterator1;

    vector0.erase(iterator0);

    // If dst in same branch as src, iterator1 may be invalid!
    if (vector0 == vector1) {
      auto [vector2, iterator2] = find_element2(dst, &root_element);
      vector1 = vector2;
      iterator1 = iterator2;
    }
    element0->parent = (*iterator1)->parent;
    vector1.insert(iterator1, element0);

    // Regenerate rows (but only if parent expanded).
    auto row0 = find_row(element0);
    if (row0) {
      auto parent = row0->get_parent(); // row0 will be invalid after unexpanding!
      if (parent->get_expanded()) {
        parent->set_expanded(false);
        parent->set_expanded(true);
      }
    }

    auto row1 = find_row(element1);
    if (row1) {
      auto parent = row1->get_parent(); // row1 will be invalid after unexpanding!
      if (parent->get_expanded()) {
        parent->set_expanded(false);
        parent->set_expanded(true);
      }
    }
  }

  void setup_listitem_cb(const Glib::RefPtr<Gtk::ListItem>& list_item) {
    // Note list_item->get_item() here returns a nullptr as item is not "bound" yet.

    auto expander = Gtk::make_managed<Gtk::TreeExpander>();

    auto label    = Gtk::make_managed<Gtk::Label>();
    label->set_halign(Gtk::Align::START);
    expander->set_child(*label);
    list_item->set_child(*expander);
  }

  void bind_listitem_cb(const Glib::RefPtr<Gtk::ListItem>& list_item) {
    // std::cout << "bind_listitem_cb: " << list_item->get_position() << std::endl;
    auto item = list_item->get_item();
    if (auto row = std::dynamic_pointer_cast<Gtk::TreeListRow>(item)) {
      if (auto col = std::dynamic_pointer_cast<ModelColumns>(row->get_item())) {
        if (auto expander = dynamic_cast<Gtk::TreeExpander*>(list_item->get_child())) {
          expander->set_list_row(row);
          expander->get_parent()->set_name(col->element->name); // Test: to see how GtkListItemWidget's are recycled.
          // std::cout << "  " << col->element->name << std::endl;

          static bool BEFORE = false;
          static bool AFTER = true;

          auto dragsource = Gtk::DragSource::create();
          dragsource->set_propagation_phase(Gtk::PropagationPhase::CAPTURE);
          dragsource->set_actions(Gdk::DragAction::MOVE);
          dragsource->signal_prepare(    ).connect([row, expander, dragsource](auto x, auto y) -> Glib::RefPtr<Gdk::ContentProvider> {
              // Create drag icon.
              auto paintable = Gtk::WidgetPaintable::create(*expander);
              auto height = expander->get_height();
              dragsource->set_icon(paintable, x, height/2);

              Glib::Value<std::shared_ptr<Gtk::TreeListRow>> value;
              value.init(Glib::Value<std::shared_ptr<Gtk::TreeListRow>>::value_type());
              value.set(row);
              auto provider = Gdk::ContentProvider::create(value);
              return provider;
            }, BEFORE);
          dragsource->signal_drag_begin( ).connect([](auto)       { std::cout << "Drag begin2"   << std::endl; });
          dragsource->signal_drag_end(   ).connect([](auto, auto) { std::cout << "Drag end2"     << std::endl; });
          dragsource->signal_drag_cancel().connect([](auto, auto) -> bool { std::cout << "Drag cancel2"  << std::endl; return false; }, BEFORE);
          expander->add_controller(dragsource);

          auto droptarget = Gtk::DropTarget::create(Glib::Value<std::shared_ptr<Gtk::TreeListRow>>::value_type(), Gdk::DragAction::MOVE);

          /*
          droptarget->signal_enter().connect([expander]) {
            expander->add_css_class("drop");
          }
          */

          droptarget->signal_leave().connect([expander]() -> auto {
            //  expander->remove_css_class("drop");
            expander->remove_css_class("before");
            expander->remove_css_class("after");
            expander->remove_css_class("into");
          });

          droptarget->signal_motion().connect([expander, col](auto x, auto y) -> Gdk::DragAction {
            // std::cout << "Motion2: " << x << ", " << y << " " << col->element->name;
            auto height = expander->get_height();
            auto drop_place = DROP_BEFORE;
            if (col->element->name == "Svg") {  // FIXME, Dropping into should be allowed
              std::cout << "  Drop blocked" << std::endl;
              return Gdk::DragAction{};
            } else if (col->element->group) {
              if (y < height * 0.2) {
                drop_place = DROP_BEFORE;
              } else if (y > height * 0.8) {
                  drop_place = DROP_AFTER;
              } else {
                drop_place = DROP_INTO;
              }
            } else {
              if (y < height/2.0) {
                drop_place = DROP_BEFORE;
              } else {
                drop_place = DROP_AFTER;
              }
            }

            expander->remove_css_class("before");
            expander->remove_css_class("after");
            expander->remove_css_class("into");

            switch (drop_place) {
            case DROP_BEFORE: /* std::cout << "  Drop before" << std::endl; */ expander->add_css_class("before"); break;
            case DROP_AFTER:  /* std::cout << "  Drop after"  << std::endl; */ expander->add_css_class("after");  break;
            case DROP_INTO:   /* std::cout << "  Drop into"   << std::endl; */ expander->add_css_class("into");   break;
            default:          std::cout << "  Unexpected drop position!" << std::endl;
            }

            return Gdk::DragAction::MOVE;
          }, BEFORE);

          droptarget->signal_drop().connect([this, row, expander, col](auto value, auto x, auto y) -> bool {

            auto source_row = static_cast<Glib::Value<std::shared_ptr<Gtk::TreeListRow>> const &>(value).get();
            auto source_col = std::dynamic_pointer_cast<ModelColumns>(source_row->get_item());

            std::cout << "Drop2: dropping "
                      << source_col->element->name << " (" << source_row->get_position() << ")"
                      << " on : "
                      <<        col->element->name << " (" <<        row->get_position() << ")" << std::endl;

            if (row == source_row) {
              // Avoid segfault later if we try inserting source before/after source.
              std::cout << "Dropping on self is not allowed" << std::endl;
              return true; // If we return 'false', we continue to look for a drop widget at the same point which
                           // results in a spurious drop attempt.
            }

            auto height = expander->get_height();
            auto drop_place = DROP_BEFORE;
            if (col->element->group) {
              if (y < height * 0.2) {
                drop_place = DROP_BEFORE;
              } else if (y > height * 0.8) {
                  drop_place = DROP_AFTER;
              } else {
                drop_place = DROP_INTO;
              }
            } else {
              if (y < height/2.0) {
                drop_place = DROP_BEFORE;
              } else {
                drop_place = DROP_AFTER;
              }
            }

            move_row(source_row, row, drop_place);

            // listview->scroll_to (row->get_position()); DOESN'T WORK HERE!
            return true;
          }, BEFORE);
          expander->add_controller(droptarget);

          if (auto label = dynamic_cast<Gtk::Label*>(expander->get_child())) {
            label->set_text(col->element->name);
          }
        }
      } else {
        std::cerr << "listitem Gtk::TreeListRow does not have columns!" << std::endl;
      }
    } else {
      std::cerr << "listitem item is not Gtk::TreeListRow!" << std::endl;
    }
  }

  void activate_cb(Gtk::ListView* list, unsigned int position) {
    if (auto model = std::dynamic_pointer_cast<Gtk::MultiSelection>(list->get_model())) {
      auto selection = model->get_selection();
      std::cout << "Selected: " << selection->get_size() << std::endl;
      for (auto it = selection->begin(); it != selection->end(); ++it) {
        auto object = model->get_object(*it);
        if (auto row = std::dynamic_pointer_cast<Gtk::TreeListRow>(object)) {
          if (auto col = std::dynamic_pointer_cast<ModelColumns>(row->get_item())) {
            std::cout << "  " << col->element->name << std::endl;
          }
        }
      }
    }
  }

  // For debugging.
  void print_element_tree(Element* element) {
    static int level = 0;
    for (int i = 0; i < level; ++i) {
      std::cout << "  ";
    }
    std::cout << level << " " << element->name << std::endl;
    level++;
    for (auto child : element->children) {
      print_element_tree(child);
    }
    level--;
  }

  // For debugging.
  void print_model_tree() {

    for (int r = 0; r < tree_list_model->property_n_items().get_value(); ++r) {

      auto row = tree_list_model->get_row(r);

      for (int i = 0; i < row->get_depth(); ++i) {
        std::cout << "  ";
      }

      // Find element corresponding to row.
      auto columns = std::dynamic_pointer_cast<ModelColumns>(row->get_item());
      assert (columns);

      auto element = columns->element;
      assert (element);

      std::cout << row->get_depth() << " " << element->name << std::endl;
    }
  }

  // For debugging. Checks that Element tree and Model tree match.
  //                Also checks that Element tree nodes have correct parents.
  void verify_trees() {

    std::vector<Element*> elements;
    std::stack<Element*> stack;
    stack.push(root_element.children[0]);

    while (!stack.empty()) {
      Element* top = stack.top();
      stack.pop();
      for (auto child : top->children | std::views::reverse) {
        if (child->parent != top) {
          std::cout << "verify_trees: parent of " << child->name
                    << " (" << child->parent->name << ") not "
                    << top->name << " !!!" << std::endl;
        }
        stack.push(child);
      }
      elements.push_back(top);
    }

    for (int r = 0; r < tree_list_model->property_n_items().get_value(); ++r) {

      auto row = tree_list_model->get_row(r);

      // Find element corresponding to row.
      auto columns = std::dynamic_pointer_cast<ModelColumns>(row->get_item());
      assert (columns);

      if (elements[r] != columns->element) {
        std::cerr << "verify_trees: trees don't match at " << columns->element->name
                  << "  (" << elements[r]->name  << ")" << std::endl;
        break;
      }
    }
  }

  // std::vector<Element> root;
  Element root_element = {"Root"};
  Gtk::ListView *listview = nullptr;
  Glib::RefPtr<Gio::ListStore<ModelColumns>> root_list_model;
  Glib::RefPtr<Gtk::TreeListModel> tree_list_model;

}; // Window

int main(int argc, char *argv[]) {

  auto application = Gtk::Application::create("org.tavmjong.treelistmodel");
  return application->make_window_and_run<Window>(argc, argv);

  return 0;
}

